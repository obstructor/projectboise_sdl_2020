#include "pch.h"
#include <GameController.h>
#include <iostream>
#include <filesystem>

using namespace pb;

class ControllerTests : public testing::Test
{
protected:
	pbController controller;
	bool initialized = false;

	void Send_Signal_And_Process(pbEvent eve)
	{
		controller.Receive_Signal(eve);
		controller.Process_Signals();
	};

	void SetUp() {
		if (!initialized)
		{
			controller.Initialize_Headless();
			initialized = true;
		}
	}
};

TEST_F(ControllerTests, SaveGame)
{
	controller.Get_Game_Engine()->Generate_Gallery_Map();
	ASSERT_NO_THROW(controller.Save_Game("Test1"));
	using namespace std::filesystem;

	path cur_path = current_path();
	path a = cur_path;
	path b = cur_path;
	a.append("SaveData").append("Test1");
	ASSERT_TRUE(exists(a));
	b.append("SaveData").append("Test1").append("SaveData.json");
}
