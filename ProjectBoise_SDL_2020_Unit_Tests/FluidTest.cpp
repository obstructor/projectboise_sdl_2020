#include "pch.h"
#include <GameController.h>
#include <iostream>

using namespace pb;

class SignalTests : public testing::Test
{
protected:
	GameEngineCore* game_engine;
	pbController controller;
	bool initialized = false;
	Map_Block vacuum = Map_Block();
	pbEvent eve;
	Game_Map* map;
	BlockPrototype* block1;
	BlockPrototype* block2;

	FluidPrototype* fluid1;
	FluidPrototype* fluid2;

	void Send_Signal_And_Process(pbEvent eve)
	{
		controller.Receive_Signal(eve);
		controller.Process_Signals();
		game_engine->Game_Update();
	};

	void SetUp() {
		if (!initialized)
		{
			controller.Initialize_Headless();
			initialized = true;
		}
		game_engine = controller.Get_Game_Engine();

		map = game_engine->Get_Game_Map(0);
		block1 = (BlockPrototype*)game_engine->Get_Prototype(EID(0, PROTOTYPE_BLOCK));
		block2 = (BlockPrototype*)game_engine->Get_Prototype(EID(1, PROTOTYPE_BLOCK));

		fluid1 = (FluidPrototype*)game_engine->Get_Prototype(EID(0, PROTOTYPE_FLUID));
		fluid2 = (FluidPrototype*)game_engine->Get_Prototype(EID(1, PROTOTYPE_FLUID));

		map->resize(Coordinate(63, 63));
	}
};

TEST_F(SignalTests, GameEnginePlaceBlocks)
{
	pbDesignation desig;
	desig.eid = block1->get_EID();
	desig.startCoord(WorldCoordinate(1, 1, 0));
	desig.endCoord(WorldCoordinate(4, 4, 0));
	eve.type = PB_EVENT_PLACE_BLOCK;
	eve.payload = desig;

	Send_Signal_And_Process(eve);

	ASSERT_EQ(map->At(Coordinate(1, 1)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(2, 2)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(3, 3)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(4, 4)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(1, 2)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(2, 2)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(3, 4)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(3, 4)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(1, 4)).parent_eid, block1->self_eid);
	ASSERT_EQ(map->At(Coordinate(4, 1)).parent_eid, block1->self_eid);

	eve.type = PB_EVENT_DESTROY_BLOCK;
	Send_Signal_And_Process(eve);

	ASSERT_EQ(map->At(Coordinate(1, 1)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(2, 2)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(3, 3)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(4, 4)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(1, 2)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(2, 2)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(3, 4)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(3, 4)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(1, 4)).parent_eid, vacuum.parent_eid);
	ASSERT_EQ(map->At(Coordinate(4, 1)).parent_eid, vacuum.parent_eid);
}

TEST_F(SignalTests, GameEngineVacuumMerge)
{
	pbDesignation desig;
	desig.eid = block1->get_EID();
	desig.startCoord(WorldCoordinate(1, 1, 0));
	desig.endCoord(WorldCoordinate(4, 4, 0));
	eve.type = PB_EVENT_PLACE_BLOCK;
	eve.payload = desig;

	Send_Signal_And_Process(eve);
	ASSERT_EQ(map->At(Coordinate(1, 1)).parent_eid, block1->self_eid);

	desig.eid = block1->get_EID();
	desig.startCoord(WorldCoordinate(2, 2, 0));
	desig.endCoord(WorldCoordinate(3, 3, 0));
	eve.type = PB_EVENT_DESTROY_BLOCK;
	eve.payload = desig;

	//Deletion leave Vacuum
	Send_Signal_And_Process(eve);
	ASSERT_TRUE(map->At(Coordinate(2, 2)).isVacuum());

	desig.eid = fluid1->get_EID();
	desig.startCoord(WorldCoordinate(0,0,0));
	desig.endCoord(WorldCoordinate(0, 0, 0));
	desig.setQuantity(100000);
	eve.type = PB_EVENT_SPAWN_GAS;
	eve.payload = desig;

	//Spawn gas should change it to Fluid_Block
	Send_Signal_And_Process(eve);
	ASSERT_EQ(map->At(Coordinate(0, 0)).get_type(), ENTITY_FLUID_BLOCK);
	//ASSERT_EQ(map->At(Coordinate(0, 0)).parent_eid, fluid1->self_eid); need to be able to check inside fluid body
	
	desig.eid = block1->get_EID();
	desig.startCoord(WorldCoordinate(1, 1, 0));
	desig.endCoord(WorldCoordinate(4, 4, 0));
	eve.type = PB_EVENT_DESTROY_BLOCK;
	eve.payload = desig;

	//Merge Test
	Send_Signal_And_Process(eve);

	auto neighbor_fluid = map->At(Coordinate(0, 0));
	ASSERT_TRUE(map->At(Coordinate(1, 1)).isFluid());

	ASSERT_EQ(neighbor_fluid.parent_eid, map->At(Coordinate(1, 1)).parent_eid);
	ASSERT_EQ(neighbor_fluid.parent_eid, map->At(Coordinate(1, 1)).parent_eid);
	ASSERT_EQ(neighbor_fluid.parent_eid, map->At(Coordinate(2, 2)).parent_eid);
	ASSERT_EQ(neighbor_fluid.parent_eid, map->At(Coordinate(4, 4)).parent_eid);
	ASSERT_EQ(neighbor_fluid.parent_eid, map->At(Coordinate(1, 4)).parent_eid);
	ASSERT_EQ(neighbor_fluid.parent_eid, map->At(Coordinate(4, 1)).parent_eid);
}
