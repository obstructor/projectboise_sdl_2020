#include "pch.h"
#include <GameController.h>

using namespace pb;
pbController controller;
bool initialized = false;
class PawnTests : public testing::Test
{
protected:
	Pawn test_pawn;
	GameEngineCore* game_engine;
	PawnHolder* pawn_holder;
	void SetUp() {
		if (!initialized)
		{
			controller.Initialize_Headless();
			initialized = true;
		}
		game_engine = controller.Get_Game_Engine();
		pawn_holder = game_engine->Get_PawnHolder();
	}
};

TEST_F(PawnTests, ChangeState)
{
	test_pawn.Change_State(game_engine, PAWN_STATE_NULL);
	ASSERT_EQ(test_pawn.Get_State(), PAWN_STATE_NULL);
	test_pawn.Change_State(game_engine, PAWN_STATE_FALLING);
	ASSERT_EQ(test_pawn.Get_State(), PAWN_STATE_FALLING);
}

TEST_F(PawnTests, AddPawn)
{
	test_pawn.Set_Location(Coordinate(1, 1));
	int id = pawn_holder->Add_Pawn(test_pawn);
	Pawn* added_pawn = game_engine->Get_Pawn_By_Id(id);
	ASSERT_EQ(test_pawn.Get_Location(), added_pawn->Get_Location());
}
TEST_F(PawnTests, FallingPawn)
{
	game_engine->Generate_Gallery_Map();
	test_pawn.Set_Location(Coordinate(5, 1));
	int id = pawn_holder->Add_Pawn(test_pawn);
	Pawn* added_pawn = pawn_holder->Get_Pawn_By_Id(id);
	FCoordinate first_location = added_pawn->Get_Exact_Location();
	added_pawn->Update(game_engine); // Decide is falling
	ASSERT_EQ(added_pawn->Get_State(), PAWN_STATE_FALLING);
	ASSERT_NE(first_location.y, added_pawn->Get_Exact_Location().y);

	for (int time = added_pawn->Get_Time_To_Finish_Task(); time > 0; --time)
	{
		added_pawn->Update(game_engine);
	}
	ASSERT_NE(added_pawn->Get_State(), PAWN_STATE_FALLING);
}