#include "pch.h"
#include <GameController.h>
#include <GameEngineCore.h>
#include <Entities.h>
#include <iostream>
#include <sstream>

using namespace pb;

class EntityTests : public testing::Test
{
protected:
	GameEngineCore* game_engine;
	pbController controller;
	EntityHolder* holder;
	bool initialized = false;

	std::map<EID, EID> eid_map = {
		{EID(), EID()},
		{EID(1,PROTOTYPE_BLOCK), EID(1,PROTOTYPE_BLOCK)},
		{EID(2,PROTOTYPE_BLOCK), EID(3, PROTOTYPE_BLOCK)},
		{EID(3, PROTOTYPE_BLOCK), EID(2, PROTOTYPE_BLOCK)},
		{EID(0, PROTOTYPE_CONSTRUCTOR), EID(0, PROTOTYPE_CONSTRUCTOR)},
		{EID(0, PROTOTYPE_RECIPE), EID(0, PROTOTYPE_RECIPE)},
		{EID(0, PROTOTYPE_FLUID), EID(0, PROTOTYPE_FLUID)},
		{EID(1, PROTOTYPE_FLUID), EID(2, PROTOTYPE_FLUID)},
		{EID(2, PROTOTYPE_FLUID), EID(1, PROTOTYPE_FLUID)},
	};

	void SetUp() {
		if (!initialized)
		{
			controller.Initialize_Headless();
			initialized = true;
		}
		game_engine = controller.Get_Game_Engine();
		holder = game_engine->Get_EntityHolder(0);
	}
};

TEST_F(EntityTests, SaveLoadBlock_EntityBaseEID) {
	BlockPrototype blockproto[3];
	blockproto[0].self_eid = EID(1, PROTOTYPE_BLOCK);
	blockproto[1].self_eid = EID(2, PROTOTYPE_BLOCK);
	blockproto[2].self_eid = EID(3, PROTOTYPE_BLOCK);

    Map_Block block0(&blockproto[0]);
	Map_Block block1(&blockproto[1]);
	Map_Block block2(&blockproto[2]);
	bj::value block_val0 = block0.toJSON();
	bj::value block_val1 = block1.toJSON();
	bj::value block_val2 = block2.toJSON();

	//Self equality, toJSON should be equal to itself
	ASSERT_EQ(block_val0, block0.toJSON());
	ASSERT_EQ(block_val1, block1.toJSON());
	ASSERT_EQ(block_val2, block2.toJSON());

	//Should not be equal to other values
	ASSERT_NE(block_val0, block_val1);
	ASSERT_NE(block_val1, block_val2);
	ASSERT_NE(block_val0, block_val2);

	//Block0 maps to itself in the eid map and should remain unchanged after load
	block0.Load_Block_FromJSON(block_val0, eid_map);
	ASSERT_EQ(block0.toJSON(), block_val0);

	//Block1 maps to block2 in the eid map and should be changed after load
	block1.Load_Block_FromJSON(block_val1, eid_map);
	ASSERT_NE(block1.toJSON(), block_val1);
	ASSERT_EQ(block1.toJSON(), block_val2);
	
	//Block2 maps to block1 in the eid map and should be changed after load
	block2.Load_Block_FromJSON(block_val2, eid_map);
	ASSERT_NE(block2.toJSON(), block_val2);
	ASSERT_EQ(block2.toJSON(), block_val1);
}

TEST_F(EntityTests, JSON_Block) {
	//Basic block json
	bj::value json_value = bj::parse("{\"peid\":{\"id\":1, \"world_id\":0, \"entity_type\":2}, \"hp\":100}");
	Map_Block parsed_block(json_value, eid_map);
	ASSERT_EQ(parsed_block.get_type(), ENTITY_BLOCK);
	ASSERT_EQ(parsed_block.block_internals.hp, 100);
	ASSERT_EQ(parsed_block.parent_eid, EID(1, PROTOTYPE_BLOCK));
	ASSERT_EQ(parsed_block.toJSON(), json_value);

	//Basic fluid json, parent is fluid body, feid is plant
	json_value = bj::parse("{\"peid\":{\"id\":2, \"world_id\":0, \"entity_type\":24}, \"feid\":{\"id\":1, \"world_id\":0, \"entity_type\":23}}");
	ASSERT_NO_THROW(parsed_block.Load_Block_FromJSON(json_value, eid_map));
	ASSERT_EQ(parsed_block.get_type(), ENTITY_FLUID_BLOCK);
	ASSERT_EQ(parsed_block.fluid_internals.placed_entity, EID(1, ENTITY_PLANT));
	ASSERT_EQ(parsed_block.parent_eid, EID(2, ENTITY_FLUID_BODY));
	ASSERT_EQ(parsed_block.toJSON(), json_value);
}


TEST_F(EntityTests, JSON_Constructor) {
	//Basic block json
	bj::value json_value = bj::parse("{\"peid\":{\"id\":0, \"world_id\":0, \"entity_type\":5},\"location\":{\"x\":100, \"y\":150}, \"progress\":100, \"working\":true, \"recipe\":{\"id\":0, \"world_id\":0, \"entity_type\":8}, \"input_buffer\":\"AQAAAAIAAAADAAAABAAAAAUAAAAGAAAABwAAAAgAAAA=\", \"output_buffer\":\"AQAAAAIAAAADAAAABAAAAAUAAAAGAAAABwAAAAgAAAA=\"}");
	Constructor parsed_con(json_value, eid_map, holder);

	//All values populated
	ASSERT_EQ(parsed_con.get_type(), ENTITY_CONSTRUCTOR);
	ASSERT_EQ(parsed_con.parent_eid.type, PROTOTYPE_CONSTRUCTOR);
	ASSERT_EQ(parsed_con.input_buffer[0], 1);
	ASSERT_EQ(parsed_con.input_buffer[1], 2);
	ASSERT_EQ(parsed_con.input_buffer[2], 3);
	ASSERT_EQ(parsed_con.input_buffer[3], 4);
	ASSERT_EQ(parsed_con.input_buffer[4], 5);
	ASSERT_EQ(parsed_con.input_buffer[5], 6);
	ASSERT_EQ(parsed_con.input_buffer[6], 7);
	ASSERT_EQ(parsed_con.input_buffer[7], 8);
	ASSERT_EQ(parsed_con.output_buffer[0], 1);
	ASSERT_EQ(parsed_con.output_buffer[1], 2);
	ASSERT_EQ(parsed_con.output_buffer[2], 3);
	ASSERT_EQ(parsed_con.output_buffer[3], 4);
	ASSERT_EQ(parsed_con.output_buffer[4], 5);
	ASSERT_EQ(parsed_con.output_buffer[5], 6);
	ASSERT_EQ(parsed_con.output_buffer[6], 7);
	ASSERT_EQ(parsed_con.output_buffer[7], 8);
	ASSERT_EQ(parsed_con.isWaiting(), false);
	ASSERT_EQ(parsed_con.isFinished(), false);
	ASSERT_EQ(parsed_con.inProgress(), true);
	ASSERT_EQ(parsed_con.toJSON(), json_value);

	//Should have attached to Potato recipe
	ASSERT_NE(parsed_con.recipe, nullptr);
	ASSERT_NE(parsed_con.recipe, (RecipePrototype*)NULL);
	ASSERT_EQ(parsed_con.recipe->self_eid, EID(0, PROTOTYPE_RECIPE));
}


TEST_F(EntityTests, JSON_FluidBody) {
	bj::value json_value = bj::parse("{\"total\":200,\"eid\":{\"id\":0, \"world_id\":0, \"entity_type\":24},\"fluids\":[{\"quantity\":50, \"peid\":{\"id\":0, \"world_id\":0, \"entity_type\":1}},{\"quantity\":150, \"peid\":{\"id\":1, \"world_id\":0, \"entity_type\":1}}]}");
	bj::value eq_json_value = bj::parse("{\"total\":200,\"eid\":{\"id\":0, \"world_id\":0, \"entity_type\":24},\"fluids\":[{\"quantity\":50, \"peid\":{\"id\":0, \"world_id\":0, \"entity_type\":1}}]}");
	FluidBody parsed_fb(json_value, eid_map);

	ASSERT_EQ(parsed_fb.total, 200);
	ASSERT_EQ(parsed_fb.self_eid, EID(0, ENTITY_FLUID_BODY, 0));
	// json_value changed with load to match engine mapping
	ASSERT_NE(bj::value(parsed_fb.toJSON()), json_value);
	// eq_json_value did not change and tests to/from equivalency
	ASSERT_EQ(eq_json_value, FluidBody(eq_json_value, eid_map).toJSON());

	// fluid_a should be unchanged from json, fluid_b should have converted to 2
	Fluid fluid_a = parsed_fb.Get_Fluid(EID(0, PROTOTYPE_FLUID));
	Fluid fluid_b = parsed_fb.Get_Fluid(EID(2, PROTOTYPE_FLUID));

	//Fluids should not be null fluid
	ASSERT_NE(fluid_a, Fluid(EID()));
	ASSERT_NE(fluid_b, Fluid(EID()));

	ASSERT_EQ(fluid_a.get_type(), ENTITY_FLUID);
	ASSERT_EQ(fluid_b.get_type(), ENTITY_FLUID);

	ASSERT_EQ(fluid_a.quantity, 50);
	ASSERT_EQ(fluid_b.quantity, 150);
}