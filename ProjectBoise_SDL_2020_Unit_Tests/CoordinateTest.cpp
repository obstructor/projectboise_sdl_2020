#include "pch.h"
#include "Coordinate.h"
#include <map>
using namespace pb;

TEST(CoordinateTest, toString)
{
    Coordinate x;
    x.x = 111;
    x.y = 222;
    std::string message = x.toString();

    ASSERT_TRUE(message.find("111") != std::string::npos);
    ASSERT_TRUE(message.find("222") != std::string::npos);
    EXPECT_TRUE(message.find("X") != std::string::npos);
    EXPECT_TRUE(message.find("Y") != std::string::npos);
}

TEST(CoordinateTest, booleanOperators)
{
    Coordinate coord1 = Coordinate(5, 5);
    Coordinate coord2 = Coordinate(5, 5);
    Coordinate coord3 = Coordinate(6, 5);
    Coordinate coord4 = Coordinate(5, 6);
    Coordinate bigcoord(99, 99);

    ASSERT_TRUE(coord1 == coord2);
    ASSERT_FALSE(coord1 == bigcoord);

    ASSERT_FALSE(coord1 != coord2);
    ASSERT_TRUE(coord1 != bigcoord);
    ASSERT_TRUE(coord1 != coord3);
    ASSERT_TRUE(coord1 != coord4);
    ASSERT_TRUE(coord3 != coord4);
    ASSERT_TRUE(coord4 != coord3);
    ASSERT_TRUE(coord3 != coord1);
    ASSERT_TRUE(coord4 != coord1);

    ASSERT_FALSE(coord1 < coord2);
    ASSERT_FALSE(coord1 > coord2);
    ASSERT_FALSE(coord2 < coord1);
    ASSERT_FALSE(coord2 > coord1);
    ASSERT_TRUE(coord1 < bigcoord);
    ASSERT_TRUE(bigcoord > coord1);
    ASSERT_FALSE(bigcoord < coord1);
    ASSERT_FALSE(coord1 > bigcoord);
}

TEST(CoordinateTest, mathOperators)
{
    Coordinate coord1;
    Coordinate coord2;
    Coordinate bigcoord(99, 99);
    ASSERT_EQ(coord1.x, coord2.x);
    ASSERT_EQ(coord1.y, coord2.y);

    coord1.x = coord1.y = 1;
    ASSERT_EQ((coord1 * 5).x, 5);
    ASSERT_EQ((coord1 * 5).y, 5);

    coord1.y = coord1.x = 6;
    ASSERT_EQ((coord1 / 6).y, 1);
    ASSERT_EQ((coord1 / 6).x, 1);

    coord1.x = coord1.y = 1;
    coord2 = coord1;
    ASSERT_EQ((coord1 + coord2).y, 2);
    ASSERT_EQ((coord1 - coord2).y, 0);
    ASSERT_EQ((coord1 + coord2).x, 2);
    ASSERT_EQ((coord1 - coord2).x, 0);
}

TEST(CoordinateTest, distance)
{
    Coordinate coord1(100, 100);
    Coordinate coord2(200, 100);
    Coordinate coord3(100, 200);
    Coordinate coord4(200, 200);

    ASSERT_FLOAT_EQ(coord1.Exact_Distance(coord2), 100);
    ASSERT_LT(abs(coord1.Exact_Distance(coord4) - 141.42), 1); // (100, 100), (200,200) => sqrt(100^2 + 100^2) ~= 142.42
    ASSERT_LT(abs(coord4.Exact_Distance(coord1) - 141.42), 1); // (100, 100), (200,200) => sqrt(100^2 + 100^2) ~= 142.42
    ASSERT_LT(abs(coord2.Exact_Distance(coord1) - 100), 1);
    ASSERT_LT(abs(coord1.Exact_Distance(coord2) - 100), 1);

    coord1 = Coordinate(50, 100);
    ASSERT_LT(abs(coord1.Exact_Distance(coord4) - 180.2776), 1);
    ASSERT_LT(abs(coord4.Exact_Distance(coord1) - 180.2776), 1);
    ASSERT_LT(abs(coord2.Exact_Distance(coord1) - 150), 1);
    ASSERT_LT(abs(coord1.Exact_Distance(coord2) - 150), 1);

    ASSERT_LT(abs(coord3.Exact_Distance(coord1) - 111.8), 1);
    ASSERT_LT(abs(coord1.Exact_Distance(coord3) - 111.8), 1);

    ASSERT_LT(abs(coord2.Exact_Distance(coord3) - 141.42), 1);
    ASSERT_LT(abs(coord3.Exact_Distance(coord2) - 141.42), 1);
}

TEST(CoordinateTest, IDistance)
{
    Coordinate coord1(100, 100);
    Coordinate coord2(200, 100);
    Coordinate coord3(100, 200);
    Coordinate coord4(200, 200);

    ASSERT_EQ(coord1.IDistance(coord2), 100);
    ASSERT_EQ(coord2.IDistance(coord1), 100);
    ASSERT_EQ(coord1.IDistance(coord3), 100);
    ASSERT_EQ(coord3.IDistance(coord1), 100);

    ASSERT_EQ(coord3.IDistance(coord2), 200);
    ASSERT_EQ(coord2.IDistance(coord3), 200);
}

TEST(CoordinateTest, Random_Coordinate_Between)
{
    Coordinate coord1(100, 100);
    Coordinate coord2(200, 100);
    Coordinate coord3(100, 200);
    Coordinate coord4(200, 200);


    Coordinate rloc;
    for (int i = 0; i < 1000; ++i)
    {
        ASSERT_EQ(coord1.Get_Random_Coordinate_Between(coord1, coord1), coord1);
        ASSERT_EQ(coord2.Get_Random_Coordinate_Between(coord2, coord2), coord2);
        ASSERT_EQ(coord3.Get_Random_Coordinate_Between(coord3, coord3), coord3);
        ASSERT_EQ(coord4.Get_Random_Coordinate_Between(coord4, coord4), coord4);

        rloc = coord1.Get_Random_Coordinate_Between(coord1, coord4);
        ASSERT_TRUE(Coordinate::Is_Between(coord1, rloc, coord4));
        rloc = coord4.Get_Random_Coordinate_Between(coord4, coord1);
        ASSERT_TRUE(Coordinate::Is_Between(coord1, rloc, coord4));
    }
}

TEST(CoordinateTest, Random_Coordinate_Between_Spread)
{
    Coordinate coord1(0, 0);
    Coordinate coord2(0, 100);
    Coordinate temp;
    std::vector<int> count(101, 0);

    for (int i = 0; i < 1000; ++i)
    {
        temp = Coordinate::Get_Random_Coordinate_Between(coord1, coord2);
        count[temp.y] += 1;
    }

    for (int i = 0; i < 101; ++i)
    {
        EXPECT_LT(count[i], 50);
    }
}


TEST(CoordinateTest, Is_Between_X)
{
    Coordinate left(100, 100), middle(100, 200), right(100, 300);

    ASSERT_TRUE(Coordinate::Is_Between(left, middle, right));
    ASSERT_TRUE(Coordinate::Is_Between(right, middle, left));

    ASSERT_TRUE(Coordinate::Is_Between(left, left, left));
    ASSERT_TRUE(Coordinate::Is_Between(right, right, right));

    ASSERT_TRUE(Coordinate::Is_Between(left, right, right));
    ASSERT_TRUE(Coordinate::Is_Between(left, left, right));
    ASSERT_TRUE(Coordinate::Is_Between(right, left, left));
    ASSERT_TRUE(Coordinate::Is_Between(right, right, left));

    ASSERT_FALSE(Coordinate::Is_Between(middle, right, left));
    ASSERT_FALSE(Coordinate::Is_Between(middle, left, right));

    ASSERT_FALSE(Coordinate::Is_Between(middle, left, middle));
    ASSERT_FALSE(Coordinate::Is_Between(middle, right, middle));

    ASSERT_FALSE(Coordinate::Is_Between(right, left, middle));
    ASSERT_FALSE(Coordinate::Is_Between(left, right, middle));

}

TEST(CoordinateTest, Is_Between_Y)
{
    Coordinate left(100, 100), middle(200, 100), right(300, 100);

    ASSERT_TRUE(Coordinate::Is_Between(left, middle, right));
    ASSERT_TRUE(Coordinate::Is_Between(right, middle, left));

    ASSERT_TRUE(Coordinate::Is_Between(left, left, left));
    ASSERT_TRUE(Coordinate::Is_Between(right, right, right));

    ASSERT_TRUE(Coordinate::Is_Between(left, right, right));
    ASSERT_TRUE(Coordinate::Is_Between(left, left, right));
    ASSERT_TRUE(Coordinate::Is_Between(right, left, left));
    ASSERT_TRUE(Coordinate::Is_Between(right, right, left));

    ASSERT_FALSE(Coordinate::Is_Between(middle, right, left));
    ASSERT_FALSE(Coordinate::Is_Between(middle, left, right));

    ASSERT_FALSE(Coordinate::Is_Between(middle, left, middle));
    ASSERT_FALSE(Coordinate::Is_Between(middle, right, middle));

    ASSERT_FALSE(Coordinate::Is_Between(right, left, middle));
    ASSERT_FALSE(Coordinate::Is_Between(left, right, middle));

}

TEST(CoordinateTest, Is_Between_XY)
{
    Coordinate left(100, 100), middle(200, 200), right(300, 300);

    ASSERT_TRUE(Coordinate::Is_Between(left, middle, right));
    ASSERT_TRUE(Coordinate::Is_Between(right, middle, left));

    ASSERT_TRUE(Coordinate::Is_Between(left, left, left));
    ASSERT_TRUE(Coordinate::Is_Between(right, right, right));

    ASSERT_TRUE(Coordinate::Is_Between(left, right, right));
    ASSERT_TRUE(Coordinate::Is_Between(left, left, right));
    ASSERT_TRUE(Coordinate::Is_Between(right, left, left));
    ASSERT_TRUE(Coordinate::Is_Between(right, right, left));

    ASSERT_FALSE(Coordinate::Is_Between(middle, right, left));
    ASSERT_FALSE(Coordinate::Is_Between(middle, left, right));

    ASSERT_FALSE(Coordinate::Is_Between(middle, left, middle));
    ASSERT_FALSE(Coordinate::Is_Between(middle, right, middle));

    ASSERT_FALSE(Coordinate::Is_Between(right, left, middle));
    ASSERT_FALSE(Coordinate::Is_Between(left, right, middle));

}

TEST(RectangleTest, Corners)
{
    Rectangle a(Coordinate(0, 0), Coordinate(10, 10));
    Rectangle b(Coordinate(10, 10), Coordinate(0, 0));

    Rectangle br(Coordinate(10, 10), Coordinate(10, 10));
    Rectangle tl(Coordinate(0, 0), Coordinate(0, 0));

    Rectangle bl(Coordinate(0, 10), Coordinate(0, 10));
    Rectangle tr(Coordinate(10, 0), Coordinate(10, 0));

    ASSERT_EQ(a.Top_Left(), a.Top_Left());
    ASSERT_EQ(a.Top_Left(), b.Top_Left());
    ASSERT_EQ(a.Top_Left(), tl.Top_Left());
    ASSERT_NE(a.Top_Left(), tr.Top_Left());
    ASSERT_NE(a.Top_Left(), bl.Top_Left());
    ASSERT_NE(a.Top_Left(), br.Top_Left());

    ASSERT_EQ(a.Top_Right(), a.Top_Right());
    ASSERT_EQ(a.Top_Right(), b.Top_Right());
    ASSERT_NE(a.Top_Right(), tl.Top_Right());
    ASSERT_EQ(a.Top_Right(), tr.Top_Right());
    ASSERT_NE(a.Top_Right(), bl.Top_Right());
    ASSERT_NE(a.Top_Right(), br.Top_Right());

    ASSERT_EQ(a.Bot_Left(), a.Bot_Left());
    ASSERT_EQ(a.Bot_Left(), b.Bot_Left());
    ASSERT_NE(a.Bot_Left(), tl.Bot_Left());
    ASSERT_NE(a.Bot_Left(), tr.Bot_Left());
    ASSERT_EQ(a.Bot_Left(), bl.Bot_Left());
    ASSERT_NE(a.Bot_Left(), br.Bot_Left());

    ASSERT_EQ(a.Bot_Right(), a.Bot_Right());
    ASSERT_EQ(a.Bot_Right(), b.Bot_Right());
    ASSERT_NE(a.Bot_Right(), tl.Bot_Right());
    ASSERT_NE(a.Bot_Right(), tr.Bot_Right());
    ASSERT_NE(a.Bot_Right(), bl.Bot_Right());
    ASSERT_EQ(a.Bot_Right(), br.Bot_Right());
}

TEST(RectangleTest, CornersNonZero)
{
    Rectangle a(Coordinate(10, 10), Coordinate(110, 110));
    Rectangle b(Coordinate(110, 110), Coordinate(10, 10));

    Rectangle br(Coordinate(110, 110), Coordinate(110, 110));
    Rectangle tl(Coordinate(10, 10), Coordinate(10, 10));

    Rectangle bl(Coordinate(10, 110), Coordinate(10, 110));
    Rectangle tr(Coordinate(110, 10), Coordinate(110, 10));

    ASSERT_EQ(a.Top_Left(), a.Top_Left());
    ASSERT_EQ(a.Top_Left(), b.Top_Left());
    ASSERT_EQ(a.Top_Left(), tl.Top_Left());
    ASSERT_NE(a.Top_Left(), tr.Top_Left());
    ASSERT_NE(a.Top_Left(), bl.Top_Left());
    ASSERT_NE(a.Top_Left(), br.Top_Left());

    ASSERT_EQ(a.Top_Right(), a.Top_Right());
    ASSERT_EQ(a.Top_Right(), b.Top_Right());
    ASSERT_NE(a.Top_Right(), tl.Top_Right());
    ASSERT_EQ(a.Top_Right(), tr.Top_Right());
    ASSERT_NE(a.Top_Right(), bl.Top_Right());
    ASSERT_NE(a.Top_Right(), br.Top_Right());

    ASSERT_EQ(a.Bot_Left(), a.Bot_Left());
    ASSERT_EQ(a.Bot_Left(), b.Bot_Left());
    ASSERT_NE(a.Bot_Left(), tl.Bot_Left());
    ASSERT_NE(a.Bot_Left(), tr.Bot_Left());
    ASSERT_EQ(a.Bot_Left(), bl.Bot_Left());
    ASSERT_NE(a.Bot_Left(), br.Bot_Left());

    ASSERT_EQ(a.Bot_Right(), a.Bot_Right());
    ASSERT_EQ(a.Bot_Right(), b.Bot_Right());
    ASSERT_NE(a.Bot_Right(), tl.Bot_Right());
    ASSERT_NE(a.Bot_Right(), tr.Bot_Right());
    ASSERT_NE(a.Bot_Right(), bl.Bot_Right());
    ASSERT_EQ(a.Bot_Right(), br.Bot_Right());
}

TEST(RectangleTest, Center)
{
    Rectangle a(Coordinate(0, 0), Coordinate(100, 100));
    ASSERT_EQ(a.Center(), Coordinate(50, 50));
    ASSERT_EQ(a.Top_Left(), Coordinate(0, 0));
    ASSERT_EQ(a.Top_Right(), Coordinate(100, 0));
    ASSERT_EQ(a.Bot_Left(), Coordinate(0, 100));
    ASSERT_EQ(a.Bot_Right(), Coordinate(100, 100));

    a.Move_Center(Coordinate(100, 100)); // Step down right
    ASSERT_EQ(a.Center(), Coordinate(100, 100));

    a.Move_Center(Coordinate(100, 100)); // Doesn't shift
    ASSERT_EQ(a.Center(), Coordinate(100, 100));

    a.Move_Center(Coordinate(50, 100)); // Step left
    ASSERT_EQ(a.Center(), Coordinate(50, 100));

    a.Move_Center(Coordinate(50, 50)); // Step up
    ASSERT_EQ(a.Center(), Coordinate(50, 50));

    a.Move_Center(Coordinate(0, 0)); // Step up left
    ASSERT_EQ(a.Center(), Coordinate(0, 0));

    a.Move_Center(Coordinate(0, 100)); // Step down
    ASSERT_EQ(a.Center(), Coordinate(0, 100));

    a.Move_Center(Coordinate(-100, 0)); // Step down Left
    ASSERT_EQ(a.Center(), Coordinate(-100, 0));

    a.Move_Center(Coordinate(0, -100)); // Step up right
    ASSERT_EQ(a.Center(), Coordinate(0, -100));

    a.Move_Center(Coordinate(100, -100)); // Step right
    ASSERT_EQ(a.Center(), Coordinate(100, -100));

    ASSERT_EQ(a.Top_Left(), Coordinate(50, -150));
    ASSERT_EQ(a.Top_Right(), Coordinate(150, -150));
    ASSERT_EQ(a.Bot_Left(), Coordinate(50, -50));
    ASSERT_EQ(a.Bot_Right(), Coordinate(150, -50));
}
TEST(RectangleTest, MoveTopLeft)
{
    Rectangle a(Coordinate(0, 0), Coordinate(100, 100));

    a.Move_TopLeft(Coordinate(100, 100)); // Step down right
    ASSERT_EQ(a.Top_Left(), Coordinate(100, 100));

    a.Move_TopLeft(Coordinate(100, 100)); // Doesn't shift
    ASSERT_EQ(a.Top_Left(), Coordinate(100, 100));

    a.Move_TopLeft(Coordinate(50, 100)); // Step left
    ASSERT_EQ(a.Top_Left(), Coordinate(50, 100));

    a.Move_TopLeft(Coordinate(50, 50)); // Step up
    ASSERT_EQ(a.Top_Left(), Coordinate(50, 50));

    a.Move_TopLeft(Coordinate(0, 0)); // Step up left
    ASSERT_EQ(a.Top_Left(), Coordinate(0, 0));

    a.Move_TopLeft(Coordinate(0, 100)); // Step down
    ASSERT_EQ(a.Top_Left(), Coordinate(0, 100));

    a.Move_TopLeft(Coordinate(-100, 0)); // Step down Left
    ASSERT_EQ(a.Top_Left(), Coordinate(-100, 0));

    a.Move_TopLeft(Coordinate(0, -100)); // Step up right
    ASSERT_EQ(a.Top_Left(), Coordinate(0, -100));

    a.Move_TopLeft(Coordinate(100, -100)); // Step right
    ASSERT_EQ(a.Top_Left(), Coordinate(100, -100));

    ASSERT_EQ(a.Center(), Coordinate(150, -50));
    ASSERT_EQ(a.Top_Right(), Coordinate(200, -100));
    ASSERT_EQ(a.Bot_Left(), Coordinate(100, 0));
    ASSERT_EQ(a.Bot_Right(), Coordinate(200, 0));
}