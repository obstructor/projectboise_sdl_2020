#include <sdlheaders.h>
#include "RenderCore.h"
#include "GameController.h"
#include "EventHandler.h"

int main(int argc, char* argv[])
{
	pb::pbController game_controller;
	game_controller.Initalize_Everything();
	game_controller.Game_Loop();
	return 0;
}
