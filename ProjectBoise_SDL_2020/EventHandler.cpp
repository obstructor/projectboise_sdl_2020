#include "EventHandler.h"
#include "GameController.h"
#include "common.h"
#include "sdlheaders.h"

SDL_Event sdl_event;
pb::pbController* game_controller;
pb::MenuBase* menu_base;
void pbsdl::Initialize_Event_Handler(pb::pbController* inGameController, pb::MenuBase * inMenuBase)
{
	game_controller = inGameController;
	menu_base = inMenuBase;
}

void pbsdl::Gather_Input()
{
	while (SDL_PollEvent(&sdl_event))
	{
		switch (sdl_event.type)
		{
		case SDL_QUIT:
			game_controller->Receive_Input();
			break;
		case SDL_MOUSEMOTION:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEWHEEL:
		case SDL_WINDOWEVENT:
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			menu_base->Receive_SDL_Event(sdl_event);
			break;
		}
	}
}
