#pragma once
#include "sdlheaders.h"
#include "GameEngineCore.h"
#include "MenuBase.h"
#include "common.h"

struct RenderData
{
	SDL_Color color;
};

class RenderCore
{
private:
	SDL_Window* window = NULL;
	SDL_Surface* screen_surface = NULL;
	SDL_Renderer* game_renderer = NULL;
	pb::Game_Map* game_map;
	pb::GameEngineCore* game_engine;
	pb::PawnHolder* pawns;
	pb::UserInterfaceCore* ui;
	SDL_Texture* background;
	SDL_Texture* desig_texture;
	pb::pbEvent designation;
	pb::MenuBase* menu_base;
	TTF_Font* font;

	pb::pbController* parent;
	pb::CircularBuffer<pb::pbEvent> event_queue;

	void Pull_Designation();

	void Render_Square(const int& type, const SDL_Rect& renderer);
	void Render_Color_Square(SDL_Rect dest, SDL_Color color);
	void Render_Background();
	void Render_Gui();
	void Clear_Screen();
	void Update_Screen();
	void Draw_Map();
	void Draw_Pawns();
	void Save_Texture(const char* file_name, SDL_Renderer* renderer, SDL_Texture* texture);

public:
	enum RenderCommandType
	{
		RENDER_COMMAND_NULL,
		RENDER_COMMAND_MOVE_VIEWPORT,
		RENDER_COMMAND_END
	};

	struct RenderCommand
	{
		RenderCommandType type;
		int val1, val2;
	};

	void Save_Screen_Texture(const char* file_name);
	void Process_Signals();
	void Receive_Signal(pb::pbEvent);

	void Initialize_Render_Core(pb::GameEngineCore*, pb::UserInterfaceCore*, pb::pbController*);
	pb::MenuBase* Get_Menu_Base();
	void Receive_Render_Commands();
	void Render();
};