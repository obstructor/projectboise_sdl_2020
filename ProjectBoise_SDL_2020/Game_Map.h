#pragma once
#include "common.h"
#include "Constructor.h"
#include "Entities.h"

namespace pb
{
	class Pawn;
	class Fluid;
	class Game_Map;
	class GameEngineCore;
	struct FluidBody;
	const int CHUNK_SIZE = 64;

	struct Map_Chunk
	{
		Map_Block chunk[CHUNK_SIZE * CHUNK_SIZE];
	};

	class EntityHolder
	{
	private:
		//TODO: create stack to reuse removed EIDs
		Game_Map* parent;
		static FluidBody vacuum;
	public:
		EntityHolder(Game_Map* in_parent);
		FluidBody* Get_Fluid(Coordinate);
		void Merge_Fluids(const std::set<EID>&, EID);
		Prototype* Get_Prototype(EID e);

		std::vector<Plant> plants;
		std::vector<Constructor> constructors;
		std::vector<FluidBody> fluid_bodies;
		std::vector<Item> items;

		bj::object toJSON();
		void Load_Entities_JSON(bj::value val, std::map<EID, EID> eid_map);
	};

	class Game_Map
	{
		std::vector<Map_Chunk> internal_map_storage;
		GameEngineCore* parent;
		EntityHolder entities;
		int map_chunk_xwidth = 0;
		int map_chunk_ywidth = 0;
		bool Fill_Vacuum(Coordinate, Fluid);
		std::set<EID> Find_Adjoining_Fluids(Coordinate);
		void Merge_Fluids(std::set<EID>, Coordinate);

	public:
		void Place_Blocks(Rectangle, EID);
		void Remove_Blocks(Rectangle);

		Prototype* Get_Prototype(EID);
		EntityHolder* Get_Entities();

		unsigned int size();
		void resize(const Coordinate&);

		bool Bound_Check(const Coordinate&) const;
		int Coord_to_Chunk(const Coordinate&) const;
		int Coord_to_Block(const Coordinate&) const;
		Coordinate Get_Ground(Coordinate);
		std::tuple<Coordinate, Coordinate> Get_Floor_Extents(Coordinate);
		bool CreateFluid(const FluidPrototype*, int quantity, Coordinate);
		void Delete_FluidBody(Coordinate);
		FluidBody& CreateFluidBody();
		bool CreateBlock(const BlockPrototype*, Coordinate);
		bool DeleteBlock(Coordinate);
		bool RemoveBlock(Coordinate);
		bool CreateEntity(const Prototype*, int quantity, Coordinate);
		bool DeleteEntity(EID);
		EntityBase* GetEntity(EID);

		Map_Block& operator[](const Coordinate&);
		Map_Block& At(const Coordinate&);
		Map_Block Get_Map_Block(const Coordinate&) const;

		int MaxX();
		int MaxY();

		void Update_Entities();

		bj::object toJSON();
		void Load_Game_Map_JSON(bj::value val, std::map<EID, EID> eid_map);

		//void Generate();
		void Generate(const std::vector<BlockPrototype>& blocks, const std::vector<FluidPrototype>& fluids, const std::vector<PlantPrototype>& plants);

		Game_Map(GameEngineCore*, bj::value, std::map<EID, EID> eid_map);
		Game_Map(GameEngineCore*);
		Game_Map();
	};
}