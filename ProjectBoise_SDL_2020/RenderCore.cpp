#include "RenderCore.h"
#include "GameEngineCore.h"
#include "GameController.h"
#include <filesystem>
#include <iostream>
#include "common.h"

using namespace pb;


void RenderCore::Save_Screen_Texture(const char* file_name)
{
	Save_Texture(file_name, game_renderer, SDL_GetRenderTarget(game_renderer));
}

void RenderCore::Process_Signals()
{
	while (event_queue.size() > 0)
	{
		auto eve = event_queue.pop_front();
		switch (eve.type)
		{
		case PB_EVENT_CURSOR_DESIGNATION_CHANGED:
			Pull_Designation();
			break;
		}
	}
}

void RenderCore::Receive_Signal(pb::pbEvent inEvent)
{
	event_queue.push_back(inEvent);
}

void RenderCore::Initialize_Render_Core(pb::GameEngineCore * in_game_engine, pb::UserInterfaceCore* in_ui, pbController* in_parent)
{
	game_engine = in_game_engine;
	ui = in_ui;
	parent = in_parent;

	window = SDL_CreateWindow("Hello", 100, 100, 800, 600, SDL_WINDOW_RESIZABLE);
	game_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (window == NULL || game_renderer == NULL || IMG_Init(IMG_INIT_PNG) <= 0)
	{
		throw std::runtime_error(SDL_GetError());
	}
	//HACK: magic number for game map, should be migrated to some sort of viewport logic
	game_map = game_engine->Get_Game_Map(0);
	background = IMG_LoadTexture(game_renderer,"background.png");
	if (background == NULL)
	{
		throw std::runtime_error(SDL_GetError());
	}

	int w, h;
	SDL_GetWindowSize(window, &w, &h);
	menu_base = new pb::MenuBase(window, w, h);

	if (menu_base == NULL)
	{
		throw std::runtime_error("Failed to initialize MenuBase");
	}
	if(TTF_Init() < 0)
		throw std::runtime_error("Failed to initialize SDL_TTF");

	font = TTF_OpenFont("Resources/OpenSans-Regular.ttf", 12);
	if (font == nullptr)
	{
		throw std::runtime_error("failed to load font: " + std::string(SDL_GetError()));
	}


	event_flag bitmask;
	bitmask.set(PB_EVENT_CURSOR_DESIGNATION_CHANGED);

	parent->Register_Listener_Bitmask(bitmask, PB_SUBSYSTEM_SDLRENDER_ENGINE);
}

pb::MenuBase* RenderCore::Get_Menu_Base()
{
	return menu_base;
}

void RenderCore::Receive_Render_Commands()
{

}

void RenderCore::Pull_Designation()
{
	if(desig_texture != nullptr)
		SDL_DestroyTexture(desig_texture);
	desig_texture = nullptr;
	auto ui_desig = ui->Get_Designation().first;
	auto desig = std::get<pbDesignation>(ui_desig.payload);
	textInformation eid_text = ui->Get_EID_Text(desig.eid);
	std::string text = pb::enum_text_map.at(ui_desig.type) + ": " + eid_text.name;

	SDL_Color fg, bg;
	fg.a = 255; bg.a = 0;
	fg.r = 255; bg.r = 0;
	fg.g = 255; bg.g = 0;
	fg.b = 255; bg.b = 0;
	auto surface = TTF_RenderText_Solid(font, text.c_str(), fg);
	desig_texture = SDL_CreateTextureFromSurface(game_renderer, surface);
	SDL_FreeSurface(surface);
}

void RenderCore::Render_Square(const int& type, const SDL_Rect& renderer)
{
	//SDL_RenderCopy(game_renderer, blocks.sheet, &blocks.Get_Sprite_Rect(type), &renderer);
}

void RenderCore::Render_Color_Square(SDL_Rect dest, SDL_Color color)
{
	SDL_SetRenderDrawColor(game_renderer, color.a, color.g, color.b, color.a);
	SDL_RenderFillRect(game_renderer, &dest);
	SDL_SetRenderDrawColor(game_renderer, 0, 0, 0, 0);
}

void RenderCore::Render_Background()
{
	//SDL_Rect x;
	SDL_RenderCopy(game_renderer,background,NULL,NULL);
}

void RenderCore::Render_Gui()
{
	menu_base->Draw_All();
	if (desig_texture != nullptr)
	{
		int x, y;
		int w, h;
		SDL_GetMouseState(&x, &y);
		SDL_QueryTexture(desig_texture, NULL, NULL, &w, &h);
		SDL_Rect rect;
		rect.x = x + 10;
		rect.y = y + 10;
		rect.w = w;
		rect.h = h;
		SDL_RenderCopy(game_renderer, desig_texture, NULL, &rect);
	}
}

void RenderCore::Clear_Screen()
{
	SDL_RenderClear(game_renderer);
}

void RenderCore::Update_Screen()
{
	SDL_RenderPresent(game_renderer);
}

void RenderCore::Draw_Map()
{
	SDL_Rect position;
	SDL_Color plant;
	plant.r = 0; plant.g = 255; plant.b = 0; plant.a = 255;
	SDL_Color block;
	block.r = block.g = block.b = 125;
	block.a = 255;
	SDL_Color clear;
	clear.r = clear.b = clear.g = 0;
	clear.a = 0;
	for (int x = 0; x < game_map->MaxX(); ++x)
	{
		for (int y = 0; y < game_map->MaxY(); ++y)
		{
			position.x = x * 8;
			position.y = y * 8;
			position.w = 8;
			position.h = 8;
			auto map_block = (*game_map)[pb::Coordinate(x, y)];
			if (map_block.isFluid() && map_block.fluid_internals.placed_entity.type == pb::ENTITY_PLANT)
				Render_Color_Square(position, plant);
			else if (map_block.isFluid())
				continue;
			else
				Render_Color_Square(position, block);
		}

	}
}

void RenderCore::Draw_Pawns()
{
	SDL_Rect position;
	SDL_Color pawn_color;
	pawn_color.r = pawn_color.g = 0;
	pawn_color.b = 255;
	pawn_color.a = 255;
	SDL_Color clear;
	clear.r = clear.b = clear.g = 50;
	clear.a = 255;

	pb::PawnHolder * pawns = game_engine->Get_PawnHolder();
	pb::Pawn* pawn;
	for (int i = 0; i < pawns->Get_Pawn_Count(); ++i)
	{
		pawn = pawns->Get_Pawn_By_Id(i);
		position.x = pawn->Get_Exact_Location().x * 8;
		position.y = pawn->Get_Exact_Location().y * 8;
		position.w = 8;
		position.h = 8;
		Render_Color_Square(position, pawn_color);
	}
}

void RenderCore::Save_Texture(const char* file_name, SDL_Renderer* renderer, SDL_Texture* texture) {
	SDL_Texture* target = SDL_GetRenderTarget(renderer);
	SDL_SetRenderTarget(renderer, texture);
	int width, height;
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	SDL_Surface* surface = SDL_CreateRGBSurface(0, width, height, 32, 0, 0, 0, 0);
	SDL_RenderReadPixels(renderer, NULL, surface->format->format, surface->pixels, surface->pitch);
	IMG_SavePNG(surface, file_name);
	SDL_FreeSurface(surface);
	SDL_SetRenderTarget(renderer, target);
}

void RenderCore::Render()
{
	Process_Signals();
	Clear_Screen();
	Render_Background();
	Draw_Map();
	Draw_Pawns();
	Render_Gui();
	Update_Screen();
}