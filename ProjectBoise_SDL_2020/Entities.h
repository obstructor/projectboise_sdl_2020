#pragma once
#include "common.h"
#include <vector>
#include "Prototypes.h"

namespace pb
{
	class EntityHolder;
	class EntityBase
	{
	protected:
		EntityBase() {};
	public:
		EID parent_eid;

		EntityType get_type();
		EntityBase(const Prototype*);
		EntityBase(EID);
		EntityBase(bj::value val, const std::map<EID, EID>& eid_map);

		bj::object toJSON() const;
		void Load_EntityBase_FromJSON(bj::value val, const std::map<EID, EID>& eid_map);
	};

	class PhysicalEntity : public EntityBase
	{
	protected:
		PhysicalEntity() {};
	public:
		Coordinate location;
		PhysicalEntity(const Prototype*, Coordinate);
		PhysicalEntity(bj::value val, const std::map<EID, EID>& eid_map);

		bj::object toJSON() const;
		void Load_PhysicalEntity_FromJSON(bj::value val, const std::map<EID, EID>& eid_map);
	};

	class Item : public PhysicalEntity
	{
	protected:
		Item() {};
	public:
		Item(const Prototype*, Coordinate);
		Item(bj::value val, const std::map<EID, EID>& eid_map);
		bj::object toJSON() const;
		void Load_Item_FromJSON(bj::value val, const std::map<EID, EID>& eid_map);
	};

	class Constructor : public PhysicalEntity
	{
	protected:
		Constructor() {};
		int progress = 0;
		enum class ConstructorState
		{
			IN_PROGRESS,
			FINISHED,
			INPUT
		};

		ConstructorState getState();
		void ProgressState();
	public:
		bool working = false;
		RecipePrototype* recipe;
		int input_buffer[RECIPE_BUFFER_SIZE] = { 0 };
		int output_buffer[RECIPE_BUFFER_SIZE] = { 0 };
		Constructor(const ConstructorPrototype*, Coordinate, RecipePrototype*);
		Constructor(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent);
		bool Create_Output();
		bool Consume_Input();

		bool isFinished();
		bool isInputBlocked();
		bool isWaiting();
		bool inProgress();

		void Update();

		bj::object toJSON() const;
		void Load_Constructor_FromJSON(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent);
	};

	class Plant : protected Constructor
	{
		void Breath(EntityHolder*);
	public:
		Plant(const PlantPrototype*, Coordinate, RecipePrototype*);
		Plant(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent);
		void Update(EntityHolder*);

		bj::object toJSON() const;
		void Load_Plant_FromJSON(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent);
	};
	
	class Fluid : public EntityBase
	{
	public:
		mutable int quantity;
		Fluid(const FluidPrototype*);
		Fluid(const EID&);
		Fluid(bj::value val, const std::map<EID, EID>& eid_map);
		bool operator==(const Fluid& other) const;
		bool operator!=(const Fluid& other) const;
		bool operator<(const Fluid& other) const { return other.parent_eid < parent_eid; };

		bj::object toJSON() const;
		void Load_Fluid_FromJSON(bj::value val, const std::map<EID, EID>& eid_map);
	};

	struct FluidBody
	{
		//Potentially more variables like pressure or a prototype pointer
		EID self_eid;
		std::set<Fluid> constituent_fluids;
		int total;

		void clear();
		void Add_Fluid(Fluid);
		Fluid Get_Fluid(EID);
		std::vector<Fluid> Get_Fluids();
		int Remove_Fluid_Quantity(EID fluid_id, int amount);
		FluidBody operator+(const FluidBody& other) const;

		FluidBody() { clear(); };
		FluidBody(bj::value val, const std::map<EID, EID>& eid_map);

		bj::object toJSON() const;
		void Load_FluidBody_FromJSON(bj::value val, const std::map<EID, EID>& eid_map);
	};

	class Map_Block : public EntityBase
	{
	private:
	public:
		union {
			struct {
				EID placed_entity;
			} fluid_internals;

			struct {
				int hp;
			} block_internals;
		};
		Map_Block(const BlockPrototype*);
		Map_Block(const FluidBody*);
		Map_Block(bj::value val, const std::map<EID, EID>& eid_map);
		Map_Block();

		bool isVacuum();
		bool isFluid();

		bj::object toJSON();
		void Load_Block_FromJSON(bj::value val, const std::map<EID, EID>& eid_map);
	};
}
