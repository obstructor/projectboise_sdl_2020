#pragma once
#include "Pawn.h"
namespace pb
{
	class PawnHolder : public std::vector<Pawn>
	{
	private:
		GameEngineCore* game_engine;
	public:
		PawnHolder(GameEngineCore*);

		Pawn* Get_Pawn_By_Id(unsigned int id);
		unsigned int Get_Pawn_Count();
		int Add_Pawn(Coordinate);
		int Add_Pawn(Pawn);
		void Update_All_Pawns();

		bj::array toJSON();
		void fromJSON(bj::value val);
	};
}