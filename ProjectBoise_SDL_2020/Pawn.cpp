#include "Pawn.h"
#include "GameEngineCore.h"
#include "common.h"
using namespace pb;

pb::Pawn::Pawn()
{
	state = PAWN_STATE_NULL;
	hunger = 1000;
	tired = 1000;
	time_to_finish_task = 0;
}

pb::Pawn::Pawn(bj::value val)
{
	fromJSON(val);
}

void Pawn::Set_Location(WorldCoordinate x)
{
	location = x;
	// Probably need to wipe out task if its related to falling, etc.
}

WorldCoordinate Pawn::Get_Location() const
{
	if (state == PAWN_STATE_NULL || start_time == 0)
		return location;
	return location + (destination - location) * (start_time - time_to_finish_task) / start_time;
}

FWorldCoordinate Pawn::Get_Exact_Location() const
{
	if (state == PAWN_STATE_NULL || start_time == 0)
		return FWorldCoordinate(location);
	return FWorldCoordinate(location) + FWorldCoordinate(destination - location) * (start_time - time_to_finish_task) / start_time;
}

PawnState pb::Pawn::Get_State() const
{
	return state;
}

int pb::Pawn::Get_Time_To_Finish_Task() const
{
	return time_to_finish_task;
}


void pb::Pawn::Update(GameEngineCore* game_engine)
{
	Check_Environment(game_engine);
	Think(game_engine);
	Move(game_engine);
}

void pb::Pawn::Check_Environment(GameEngineCore* game_engine)
{
	//In theory later on we don't need to check the environment we would just interrupt the pawn on environment changes
	Game_Map* a = game_engine->Get_Game_Map(Get_Location());
	bool on_ground = Get_Location() == a->Get_Ground(Get_Location());
	if (a->Bound_Check(Get_Location()))
	{
		if(!on_ground && state != PAWN_STATE_FALLING)
		{
			location = Get_Location();
			Change_State(game_engine,PAWN_STATE_FALLING);
		}
		else if (on_ground && state == PAWN_STATE_FALLING)
		{
			Change_State(game_engine, PAWN_STATE_NULL);
		}
	}
	else
	{
		throw std::out_of_range("Pawn out of bounds");
	}
}

void pb::Pawn::Change_State(GameEngineCore* game_engine,PawnState new_state)
{
	Game_Map* a = game_engine->Get_Game_Map(Get_Location());
	if (new_state == state)
		return;
	if(new_state == PAWN_STATE_FALLING)
	{
		destination = a->Get_Ground(Get_Location());
		start_time = time_to_finish_task = destination.IDistance(Get_Location()) * FALL_SPEED;
	}
	else if (new_state == PAWN_STATE_IDLE)
	{
		auto extents = a->Get_Floor_Extents(Get_Location());
		destination = WorldCoordinate::Get_Random_Coordinate_Between(std::get<0>(extents), std::get<1>(extents));
		start_time = time_to_finish_task = destination.IDistance(location) * WALK_SPEED;
	}
	state = new_state;
	//cleanup task if interrupted.
	//validate state change is appropriate
}

void Pawn::Move(GameEngineCore* game_engine)
{
	if (time_to_finish_task <= 1)
	{
		location = destination;
		Change_State(game_engine,PAWN_STATE_NULL);
	}
	else
	{
		time_to_finish_task -= 1;
	}
}

void Pawn::Think(GameEngineCore* game_engine)
{
	if (state == PAWN_STATE_NULL)
	{
		Change_State(game_engine,PAWN_STATE_IDLE);
	}
}

bj::object pb::Pawn::toJSON()
{
	bj::object obj;
	obj.insert_or_assign("state", state);
	obj.insert_or_assign("location", location.toJSON());
	obj.insert_or_assign("destination", destination.toJSON());
	obj.insert_or_assign("hunger", hunger);
	obj.insert_or_assign("tired", tired);
	obj.insert_or_assign("start_time", start_time);
	obj.insert_or_assign("time_to_finish_task", time_to_finish_task);
	return obj;
}

void Pawn::fromJSON(bj::value val)
{
	state = (PawnState)val.at("state").as_int64();
	location.fromJSON(val.at("location"));
	destination.fromJSON(val.at("destination"));
	hunger = val.at("hunger").as_int64();
	tired = val.at("tired").as_int64();
	start_time = val.at("start_time").as_int64();
	time_to_finish_task = val.at("time_to_finish_task").as_int64();
}
