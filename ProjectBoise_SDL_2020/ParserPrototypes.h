#pragma once
#include "Prototypes.h"
#include "common.h"

namespace pb
{
	class Parser;

	//PYTHON START HERE PARSER_PROTOTYPES_CLASSES
	class ParserFluidPrototype : public FluidPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserBlockPrototype : public BlockPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserItemPrototype : public ItemPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserParticlePrototype : public ParticlePrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserConstructorPrototype : public ConstructorPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserFurniturePrototype : public FurniturePrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserConstructionPrototype : public ConstructionPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserRecipePrototype : public RecipePrototype
	{
	public:
		std::string string_ingredient_id[RECIPE_BUFFER_SIZE];
		std::string string_output_id[RECIPE_BUFFER_SIZE];
		void Convert(Parser* parser);
	};

	class ParserMapgenPrototype : public MapgenPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserPawnPrototype : public PawnPrototype
	{
	public:
		void Convert(Parser* parser);
	};

	class ParserPlantPrototype : public PlantPrototype
	{
	public:
		std::string string_recipe;
		void Convert(Parser* parser);
	};

	//PYTHON STOP HERE PARSER_PROTOTYPES_CLASSES	

	//Used for parsing, need every single prototype
	class AllPrototypeUnion
	{
	public:
		union {
			Prototype base;
			//PYTHON START HERE DECLARE_UNION
			ParserFluidPrototype fluid;
			FluidPrototype final_fluid;
			ParserBlockPrototype block;
			BlockPrototype final_block;
			ParserItemPrototype item;
			ItemPrototype final_item;
			ParserParticlePrototype particle;
			ParticlePrototype final_particle;
			ParserConstructorPrototype constructor;
			ConstructorPrototype final_constructor;
			ParserFurniturePrototype furniture;
			FurniturePrototype final_furniture;
			ParserConstructionPrototype construction;
			ConstructionPrototype final_construction;
			ParserRecipePrototype recipe;
			RecipePrototype final_recipe;
			ParserMapgenPrototype mapgen;
			MapgenPrototype final_mapgen;
			ParserPawnPrototype pawn;
			PawnPrototype final_pawn;
			ParserPlantPrototype plant;
			PlantPrototype final_plant;
			//PYTHON STOP HERE DECLARE_UNION
		};
		//Update destructor, PopParse, etc when adding variables
		textInformation text;

		bool is_final = false;

		int sprite_id;
		int proto_id;

		EntityType type;

		operator bool() const { return type != ENTITY_TYPE_NULL; };

		void ClearData();
		void ClearProtoData();

		AllPrototypeUnion() { type = ENTITY_TYPE_NULL; base = Prototype(); };
		AllPrototypeUnion(EntityType t);
		AllPrototypeUnion(Prototype p) { base = p; type = ENTITY_TYPE_NULL; };

		AllPrototypeUnion(const AllPrototypeUnion& in_all_type);
		AllPrototypeUnion& operator=(const AllPrototypeUnion& in_all_type);
		~AllPrototypeUnion();

		void Finalize(Parser * parser);

		operator Prototype() { return base; };
		//PYTHON START HERE TEMPLATE_FINAL_ENTITY_CASTS
		operator FluidPrototype() const { return final_fluid; };
		operator BlockPrototype() const { return final_block; };
		operator ItemPrototype() const { return final_item; };
		operator ParticlePrototype() const { return final_particle; };
		operator ConstructorPrototype() const { return final_constructor; };
		operator FurniturePrototype() const { return final_furniture; };
		operator ConstructionPrototype() const { return final_construction; };
		operator RecipePrototype() const { return final_recipe; };
		operator MapgenPrototype() const { return final_mapgen; };
		operator PawnPrototype() const { return final_pawn; };
		operator PlantPrototype() const { return final_plant; };
		//PYTHON STOP HERE TEMPLATE_FINAL_ENTITY_CASTS
	};
}
