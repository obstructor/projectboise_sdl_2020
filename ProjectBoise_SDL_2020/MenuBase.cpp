#include "MenuBase.h"
#include "GameController.h"
#include <iostream>

using namespace pb;
using namespace sdlgui;

void MenuBase::Generate_Default_Event_Map()
{
	button_event_map = {
		{1, pbEvent(PB_EVENT_NULL)},
		{2, pbEvent(PB_EVENT_NULL)},
		{SDLK_e, pbEvent(PB_EVENT_NEXT_CURSOR_DESIGNATION)},
	};
}

void MenuBase::Execute_Event(int id)
{
	event_handled = true;
	parent->Receive_Signal(button_event_map.at(id));
}

auto MenuBase::Generate_Event_Lambda()
{
	id_counter++;
	return [=] {Execute_Event(id_counter); };
}

auto MenuBase::Generate_Event_Lambda(int id)
{
	return [=] {Execute_Event(id); };
}

auto MenuBase::Generate_Event_Lambda(int id, pbEvent eve)
{
	button_event_map.insert({ id, eve });
	return [=] {Execute_Event(id); };
}

auto MenuBase::Generate_Event_Lambda(pbEvent eve)
{
	int id = id_counter;
	id_counter++;
	button_event_map.insert({ id, eve });
	return [=] {Execute_Event(id); };
}

MenuBase::MenuBase(SDL_Window* pwindow, int rwidth, int rheight) : Screen(pwindow, Vector2i(rwidth, rheight), "SDL_gui Test")
{
	ui = NULL;
	id_counter = 0;
}

void MenuBase::Draw_All()
{
	drawAll();
}

void MenuBase::Receive_SDL_Event(SDL_Event& eve)
{
	pbEvent result_event(PB_EVENT_NULL);
	event_handled = false;
	//onEvent(eve); Replaced with switch to catch the bool
	switch (eve.type)
	{
	case SDL_MOUSEWHEEL:
		event_handled = scrollCallbackEvent(eve.wheel.x, eve.wheel.y);
	break;
	case SDL_MOUSEMOTION:
		event_handled = cursorPosCallbackEvent(eve.motion.x, eve.motion.y);
	break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		event_handled = mouseButtonCallbackEvent(eve.button.button, eve.button.type, SDL_GetModState());
	break;
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		event_handled = keyCallbackEvent(eve.key.keysym.sym, eve.key.keysym.scancode, eve.key.state, SDL_GetModState());
	break;
	case SDL_TEXTINPUT:
		event_handled = charCallbackEvent(eve.text.text[0]);
	break;
	}

	if (!event_handled)
	{
		// Attempt to match to keybinds
		for (int i = menu_stack.size() - 1; i >= 0 && result_event.type == PB_EVENT_NULL; --i)
		{
			result_event = menu_stack[i]->Get_Event_From_SDL_Event(eve);
		}
		event_handled = result_event.type != PB_EVENT_NULL;
	}

	if (!event_handled && (eve.type == SDL_MOUSEBUTTONDOWN || eve.type == SDL_MOUSEBUTTONUP))
	{
		if (eve.button.button == SDL_BUTTON_LEFT)
		{
			if (eve.type == SDL_MOUSEBUTTONUP)
			{
				result_event = pbEvent(PB_EVENT_FINISH_DESIGNATION, pbMenuEvent(Coordinate(eve.button.x, eve.button.y)));
			}
			if (eve.type == SDL_MOUSEBUTTONDOWN)
			{
				result_event = pbEvent(PB_EVENT_START_DESIGNATION, pbMenuEvent(Coordinate(eve.button.x, eve.button.y)));
			}
		}
	}

	if (result_event.type != PB_EVENT_NULL)
	{
		parent->Receive_Signal(result_event);
	}
}

void MenuBase::Initialize(UserInterfaceCore * inui, pbController * inparent)
{
	ui = inui;
	parent = inparent;
	id_counter = 0;
}

void Gui_Issue_Command(UserInterfaceCore * ui, pbEvent command)
{

}

void Gui_Issue_Commands(UserInterfaceCore * ui, std::vector<pbEvent> command_vector)
{
	for (pbEvent command : command_vector)
	{
		Gui_Issue_Command(ui, command);
	}
}

void MenuBase::Test()
{
	all_menues.emplace_back(this, (sdlgui::Widget*)NULL);
	Menu* new_menu = &(all_menues.back());
	new_menu->Generate_Default_Main_Menu(ui, parent);
	addChild((Widget*)new_menu);
	menu_stack.push_back(new_menu);
	performLayout();
}

Menu::Menu(MenuBase* parent, sdlgui::Widget* sdlgui_screen) : sdlgui::Widget(sdlgui_screen)
{
	menu_base = parent;
	incRef();
}

Menu::~Menu()
{

}

pbEvent Menu::Get_Event_From_SDL_Event(SDL_Event& eve)
{
	if (keybinds.empty() || eve.type != SDL_KEYUP || keybinds.find(eve.key.keysym.sym) == keybinds.end())
		return pbEvent(PB_EVENT_NULL);
	else
		return keybinds.at(eve.key.keysym.sym);
}

void Menu::Generate_Default_Main_Menu(UserInterfaceCore *&ui, pbController *&controller)
{
	withLayout<BoxLayout>(Orientation::Vertical);
	//auto& nwindow = window("Button demo", Vector2i{ 15, 15 }).withLayout<GroupLayout>();
	button("Start_Game", ENTYPO_ICON_ROCKET, [&] { std::cout << "Start"; controller->Receive_Signal(pbEvent(PB_EVENT_START_GAME)); });
	button("Exit", ENTYPO_ICON_AIRPLANE, [&] { std::cout << "Exit"; controller->Receive_Signal(pbEvent(PB_EVENT_CLOSE_GAME)); });
	button("Save Game", ENTYPO_ICON_FOLDER, [&] { std::cout << "Save Game"; controller->Receive_Signal(pbEvent(PB_EVENT_SAVE_GAME)); });
	button("Load Game", ENTYPO_ICON_FOLDER, [&] { std::cout << "Load Game"; controller->Receive_Signal(pbEvent(PB_EVENT_LOAD_GAME)); });
	//setParent(this);
	Generate_Default_KeyBinds();
}

void Menu::Generate_Default_KeyBinds()
{
	keybinds = {
		{SDLK_UP , pbEvent(PB_EVENT_MOVE_VIEWPORT)},
		{SDLK_DOWN , pbEvent(PB_EVENT_MOVE_VIEWPORT)},
		{SDLK_LEFT , pbEvent(PB_EVENT_MOVE_VIEWPORT)},
		{SDLK_RIGHT , pbEvent(PB_EVENT_MOVE_VIEWPORT)},
		{SDLK_SPACE, pbEvent(PB_EVENT_PAUSE_GAME)},
		{SDLK_e, pbEvent(PB_EVENT_NEXT_CURSOR_DESIGNATION)},
	};
}

Rectangle Menu::Get_Extents()
{
	return Rectangle(Coordinate(getAbsoluteLeft(),getAbsoluteTop()), Coordinate(getAbsoluteLeft() + width(), getAbsoluteTop() + height()));
}
