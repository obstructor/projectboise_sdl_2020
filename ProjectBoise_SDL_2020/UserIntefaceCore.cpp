#include "UserIntefaceCore.h"
#include "GameController.h"
#include "common.h"


using namespace pb;

UserInterfaceCore::UserInterfaceCore(pbController* inParent)
{
	parent = inParent;
}

std::pair<pbEvent, Rectangle> pb::UserInterfaceCore::Get_Designation()
{
	return std::pair<pbEvent, Rectangle>(designation, designation_rect);
}

textInformation pb::UserInterfaceCore::Get_EID_Text(EID id)
{
	auto text_info = proto_textinfo.find(id);
	if (text_info != proto_textinfo.end())
	{
		return text_info->second;
	}
	else {
		return textInformation();
	}
}

void UserInterfaceCore::Initialize_UI(Parser* parser)
{
	event_flag bitmask;
	bitmask.set(PB_EVENT_SET_CURSOR_DESIGNATION);
	bitmask.set(PB_EVENT_NEXT_CURSOR_DESIGNATION);
	bitmask.set(PB_EVENT_START_DESIGNATION);
	bitmask.set(PB_EVENT_FINISH_DESIGNATION);
	bitmask.set(PB_EVENT_CANCEL_DESIGNATION);

	parent->Register_Listener_Bitmask(bitmask, PB_SUBSYSTEM_USER_INTERFACE);

	proto_textinfo = parser->get_all_proto_text();

	designation = pbEvent(PB_EVENT_NULL, pbDesignation());
}

void UserInterfaceCore::Attach_MenuBase(MenuBase* inMenuBase)
{
	menu_base = inMenuBase;
}

void UserInterfaceCore::Receive_Signal(pbEvent inEvent)
{
	event_queue.push_back(inEvent);
}


pbEvent UserInterfaceCore::Designation_From_EID(EID id)
{
	pbEvent eve;
	eve.payload = pbDesignation(id);
	switch (id.type)
	{
		case PROTOTYPE_BLOCK:
			eve.type = PB_EVENT_PLACE_BLOCK;
			break;
		case PROTOTYPE_FLUID:
			eve.type = PB_EVENT_SPAWN_GAS;
			break;
		case PROTOTYPE_ITEM:
			eve.type = PB_EVENT_SPAWN_ITEM;
			break;
		case PROTOTYPE_CONSTRUCTOR:
			eve.type = PB_EVENT_SPAWN_ITEM;
			break;
		case PROTOTYPE_PLANT:
			eve.type = PB_EVENT_SPAWN_ITEM;
			break;
		case PROTOTYPE_RECIPE:
			eve.type = PB_EVENT_CHANGE_RECIPE;
			break;
		case PROTOTYPE_PAWN:
			eve.type = PB_EVENT_SPAWN_PAWN;
			break;
	}
	return eve;
}

void pb::UserInterfaceCore::Next_Designation()
{
	if (proto_textinfo.size() == 0)
	{
		designation = pbEvent();
		return;
	}

	auto first = proto_textinfo.begin();

	pbDesignation& desig = std::get<pbDesignation>(designation.payload);
	auto current = proto_textinfo.find(desig.eid);
	if (current == proto_textinfo.end())
		current = proto_textinfo.begin();

	auto next = current;
	next++;
	if (next == proto_textinfo.end())
		next = proto_textinfo.begin();

	if (first == proto_textinfo.end() || next == proto_textinfo.end() || current == proto_textinfo.end())
	{
		throw std::runtime_error("Next_Designation iterator invalid");
	}
	//HACK: should only do this if its a fluid, but honestly this needs to be refactored into the UI buttons and whatnot so its not a big deal
	desig.setQuantity(100);

	switch (designation.type)
	{
	case PB_EVENT_NULL:
		designation = Designation_From_EID(first->first);
		break;
	//Destroys move onto the next EID, creates must move to the equivalent destroy
	case PB_EVENT_CHANGE_RECIPE:
	case PB_EVENT_SPAWN_PAWN:
	case PB_EVENT_DELETE_ITEM:
	case PB_EVENT_DELETE_GAS:
	case PB_EVENT_DESTROY_BLOCK:
		designation = Designation_From_EID(next->first);
		break;
	case PB_EVENT_PLACE_BLOCK:
		designation.type = PB_EVENT_DESTROY_BLOCK;
		break; 
	case PB_EVENT_SPAWN_ITEM:
		designation.type = PB_EVENT_DELETE_ITEM;
		break;
	case PB_EVENT_SPAWN_GAS:
		designation.type = PB_EVENT_DELETE_GAS;
		break;
	}
}

void UserInterfaceCore::Process_Command(pbEvent eve)
{
	pbDesignation& desig = std::get<pbDesignation>(designation.payload);
	switch (eve.type)
	{
	case PB_EVENT_SET_CURSOR_DESIGNATION:
		//TODO: actually set designation
		parent->Receive_Signal(pbEvent(PB_EVENT_CURSOR_DESIGNATION_CHANGED));
		break;
	case PB_EVENT_NEXT_CURSOR_DESIGNATION:
		Next_Designation();
		parent->Receive_Signal(pbEvent(PB_EVENT_CURSOR_DESIGNATION_CHANGED));
		break;
	case PB_EVENT_START_DESIGNATION:
		//HACK: magic numbers should use viewport information
		desig.startCoord(WorldCoordinate(std::get<pbMenuEvent>(eve.payload).location / 8));
		break;
	case PB_EVENT_FINISH_DESIGNATION:
		//HACK: magic numbers
		desig.endCoord(WorldCoordinate(std::get<pbMenuEvent>(eve.payload).location / 8));
		parent->Receive_Signal(designation);
		break;
	case PB_EVENT_CANCEL_DESIGNATION:
		//TODO: cancel designation
		break;
	}
}

void UserInterfaceCore::Update()
{
	while (event_queue.size() > 0)
	{
		Process_Command(event_queue.pop_front());
	}
}

bj::object UserInterfaceCore::Save_UI_JSON()
{
	bj::object ui_obj;
	bj::array proto;
	for (auto key_pair : proto_textinfo)
	{
		bj::object pobj;
		pobj.insert_or_assign("name", key_pair.second.name);
		pobj.insert_or_assign("eid", key_pair.first.toJSON());
		proto.push_back(pobj);
	}
	ui_obj.insert_or_assign("prototypes", proto);
	ui_obj.insert_or_assign("event_queue", event_queue.Save_EventQueue_JSON());

	return ui_obj;
}

std::map<EID, EID> UserInterfaceCore::Load_UI_JSON(bj::value val)
{
	using std::make_pair;
	bj::object& ui_obj = val.as_object();
	std::map<EID, EID> proto_map;
	std::map<std::string, EID> flipped_map;
	for (auto key_pair : proto_textinfo)
	{
		flipped_map.insert(make_pair(key_pair.second.name, key_pair.first));
	}

	for (auto json_obj : ui_obj.at("prototypes").as_array())
	{
		EID eid, engine_eid;
		eid.fromJSON(json_obj.at("eid"));
		engine_eid = flipped_map.at(json_obj.at("name").as_string().c_str());
		proto_map.insert(make_pair(eid, engine_eid));
	}
	event_queue.Load_EventQueue_JSON(ui_obj.at("event_queue"));
	for (auto& eve : event_queue)
		eve.convertEIDs(proto_map);
	return proto_map;
}