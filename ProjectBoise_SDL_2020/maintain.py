## maintains specific parts of the c++ code
import re



common_header = {"file": "common.h",
            "data": [],
            "out": []
            }

common_cpp = {"file": "common.cpp",
            "data": [],
            "out": []
            }

prototypes_header = {"file": "Prototypes.h",
            "data": [],
            "out": []
            }

entities = {"file": "Entities.h",
            "data": [],
            "out": []
            }

entitiescpp = {"file": "Entities.cpp",
               "data": [],
               "out": []
               }

parsercpp = {"file": "Parser.cpp",
             "data": [],
             "out": []
             }

parser = {"file": "Parser.h",
          "data": [],
          "out": []
          }

parserprotocpp = {"file": "ParserPrototypes.cpp",
                  "data": [],
                  "out": []
                  }

parserproto = {"file": "ParserPrototypes.h",
               "data": [],
               "out": []
               }

prototoypes = []
pbEvents = []
line_counter = 0
c_types = ["int", "short", "char", "unsigned int", "bool", "float", "double", 'string', 'std::string', 'EID']
c_types_or = ""
for type in c_types:
    c_types_or += type + '|'
c_types_or = c_types_or[0:-1]


class Variable:
    def __init__(self, in_name="", in_type=""):
        self.name = in_name
        self.type = in_type.replace("std::", "")
        self.parse_name = "string_" + in_name if in_type == 'EID' else in_name


class Prototype:
    def __init__(self, in_enum="", in_name=""):
        self.enum = in_enum
        self.name = in_name
        self.declaration = in_name[0].upper() + in_name[1:].lower() + "Prototype"
        self.proper_name = in_name[0].upper() + in_name[1:].lower()
        self.variables = []


class pbEvent:
    def __init__(self, enum_text=""):
        self.enum_text = enum_text


def get_type_function(type):
    type_normal = type.replace(' ', '_')
    return type + " " + "String_To_" + type_normal


def parse_types(data_set):
    global line_counter
    while line_counter < len(data_set["data"]):
        line = data_set["data"][line_counter]
        data_set["out"].append(line)
        line_counter += 1

        if "//PYTHON STOP HERE" in line:
            return
        elif "PROTOTYPE_" in line:
            match = re.search("(PROTOTYPE_([a-zA-Z]+)),?", line)
            print(match.group(1))
            prototoypes.append(Prototype(match.group(1), match.group(2)))


def read_prototype(data_set):
    global line_counter
    unhandled_prototypes = [i.declaration for i in prototoypes]
    line = data_set["data"][line_counter]
    tabs = '\t' * line.count('\t')

    while line_counter < len(data_set["data"]):
        line = data_set["data"][line_counter]

        if "struct" in line or "class" in line:
            match = re.search("(class|struct) ([a-zA-Z_]+)", line)
            name = match.group(2)
            if name not in unhandled_prototypes:
                raise Exception("Unhandled prototype, do not put unhandled prototypes in python maintained code" + name)
            else:
                unhandled_prototypes.remove(name)

            while "};" not in line:
                line = data_set["data"][line_counter]
                line_counter += 1

                var_match = re.search("(" + c_types_or + ") ([a-zA-Z_\\[\\]0-9]+)", line)
                if var_match:
                    matching_proto = None
                    for pro in prototoypes:
                        if pro.declaration == name:
                            matching_proto = pro
                            break
                    if matching_proto is None:
                        raise Exception("Failed to match prototype declaration" + name)
                    else:
                        matching_proto.variables += [Variable(var_match.group(2), var_match.group(1))]
                        print(var_match.group(1))
                        print(var_match.group(2))

                data_set["out"].append(line)

        elif "//PYTHON STOP HERE" in line:
            # dump unhandled classes
            for unhandled in unhandled_prototypes:
                #unhandled = unhandled[0].upper() + unhandled[1:].lower()
                #unhandled[0] = unhandled[0].upper()
                data_set["out"].append(tabs +"struct " + unhandled + " : Prototype")
                data_set["out"].append(tabs + "{")
                data_set["out"].append(tabs + "};")
                print("struct " + unhandled + "Prototype : Prototype {};")
            return
        else:
            data_set["out"].append(line)
            line_counter += 1

def declare_union(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        data_set["out"].append(tabs + "Parser" + proto.declaration + " " + proto.name.lower() + ";")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + proto.declaration + " final_" + proto.name.lower() + ";")
        print(data_set["out"][-1])


def constructor_declaration(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "AllPrototypeUnion(" + proto.declaration + " " + proto.name[0].lower() +") { " + proto.name.lower() + " = " + proto.name[0].lower() + "; type = " + proto.enum + ";};")
        print(tabs + "AllPrototypeUnion(" + proto.declaration + " " + proto.name[0].lower() +") { " + proto.name.lower() + " = " + proto.name[0].lower() + "; type = " + proto.enum + ";};")


def destructor_switch(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "case " + proto.enum + ":")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + '\t is_final ? ' + proto.name.lower() + ".~Parser" + proto.declaration + "() : final_" + proto.name.lower() + ".~" + proto.declaration + "();")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + "\tbreak;")
        print(data_set["out"][-1])


def operator_equal_switch(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "case " + proto.enum + ":")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + '\t' + "in_all_type.is_final ? new (&final_" + proto.name.lower() + ") " + proto.declaration + " : " + "new (&" + proto.name.lower() + ") Parser" + proto.declaration + ";")
        data_set["out"].append(tabs + '\t' + "in_all_type.is_final ? final_" + proto.name.lower() + " = in_all_type.final_" + proto.name.lower() + " : " + proto.name.lower() + " = in_all_type." + proto.name.lower() + ";" )
        print(data_set["out"][-1])
        data_set["out"].append(tabs + "\tbreak;")
        print(data_set["out"][-1])


def parse_row_switch(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "case " + proto.enum + ":")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + '\t' + "Parse_" + proto.proper_name + "(row, any_prototype);")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + "\tbreak;")
        print(data_set["out"][-1])


def string_to_type(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        data_set["out"].append(tabs + "{\""+ proto.name + "\", " + proto.enum+ "},")
        print(data_set["out"][-1])

    data_set["out"][-1] = data_set["out"][-1][0:-1]


def parse_definition(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    tabs1 = tabs + '\t'
    tabs2 = tabs1 + '\t'

    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "AllPrototypeUnion& Parser::Parse_" + proto.proper_name + "(const std::vector<std::string>& row, AllPrototypeUnion& a)")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + "{")
        print(data_set["out"][-1])
        data_set["out"].append(tabs1 + "int i = 0;")
        print(data_set["out"][-1])

        for variable in proto.variables:
            if "[" in variable.name:
                index = re.search("\[([0-9a-zA-Z_]+)]", variable.name).group(1)

                data_set["out"].append(tabs1 + "for(int iter = 0; iter < " + index + ";++iter)")
                print(data_set["out"][-1])
                array = variable.parse_name.replace("[" + str(index) + "]", '')

                data_set["out"].append(tabs2 + "a." + proto.name.lower() + "." + array + "[iter] = String_To_" + variable.type.replace(' ', '_') + "(row[i++]);\n")
                #data_set["out"].append(tabs1 + "a." + proto.name.lower() + "." + array + "[" + "0" + "] = String_To_" + variable.type.replace(' ', '_') + "(row[i++]);")
                print(data_set["out"][-1])

                #for i_index in range(index):
                #
                #    print(data_set["out"][-1])
                #    i += 1
                #data_set["out"].append("")
            else:
                data_set["out"].append(tabs1 + "a." + proto.name.lower() + "." + variable.parse_name + " = String_To_" + variable.type.replace(' ', '_') + "(row[i++]);")
                print(data_set["out"][-1])

        data_set["out"].append("")
        data_set["out"].append(tabs1 + "return a;")
        data_set["out"].append(tabs + "};")
        data_set["out"].append("")
        print(data_set["out"][-1])


def parse_declaration(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "AllPrototypeUnion& Parse_" + proto.proper_name + "(const std::vector<std::string>&, AllPrototypeUnion&);")
        print(data_set["out"][-1])


def string_to_declaration(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for type in c_types:
        if type in ['EID', 'string', 'std::string']:
            continue
        data_set["out"].append(tabs + type + " String_To_" + type.replace(' ', '_') + "(std::string);")
        print(data_set["out"][-1])


def parser_prototypes_classes(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    tabs1 = tabs + '\t'
    tabs2 = tabs1 + '\t'

    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        data_set["out"].append(tabs + "class Parser" + proto.declaration + " : public " + proto.declaration)
        data_set["out"].append(tabs + "{")
        data_set["out"].append(tabs + "public:")
        for var in proto.variables:
            if "EID" in var.type:
                data_set["out"].append(tabs1 + "std::string " + var.parse_name + ";")

        data_set["out"].append(tabs1 + "void Convert(Parser* parser);")
        data_set["out"].append(tabs + "};")
        data_set["out"].append("")


def parser_prototype_classes_convert_functions(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    tabs1 = tabs + '\t'
    tabs2 = tabs1 + '\t'

    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        data_set["out"].append(tabs1 + "void Parser"+ proto.declaration +"::Convert(Parser* parser)")
        data_set["out"].append(tabs1 + "{")

        for var in proto.variables:
            if "EID" in var.type and '[' in var.name:
                index = re.search("\[([0-9a-zA-Z_]+)]", var.name).group(1)
                array = var.name.replace("[" + str(index) + "]", '')
                parse_array = var.parse_name.replace("[" + str(index) + "]", '')

                data_set["out"].append(tabs2 + "for(int iter = 0; iter < " + index + ";++iter)")
                print(data_set["out"][-1])

                data_set["out"].append(tabs2 + '\t' + array + "[iter] = parser->Lookup_EID(" + parse_array + "[iter]);\n")

                #array = var.name.replace("[" + str(index) + "]", '')
                #parse_array = var.parse_name.replace("[" + str(index) + "]", '')
                #for i_index in range(index):
                #    data_set["out"].append(tabs2 + array + "[" + str(i_index) + "] = parser->Lookup_EID(" + parse_array + "[" + str(i_index) + ']);')
                #    print(data_set["out"][-1])
                #data_set["out"].append("")
            elif "EID" in var.type:
                data_set["out"].append(tabs2 + var.name + ' = ' + "parser->Lookup_EID(" + var.parse_name + ");")

        data_set["out"].append(tabs1 + "};")
        data_set["out"].append(tabs1 + "")


def constructor_definition_switch(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "case " + proto.enum + ":")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + '\t' + "new (&" + proto.name.lower() + ") Parser" + proto.declaration + ";")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + "\tbreak;")
        print(data_set["out"][-1])


def finalize_allprototype_switch(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        #AllPrototypeUnion(MapgenPrototype m) { mapgen = m; type = PROTOTYPE_MAPGEN;};
        data_set["out"].append(tabs + "case " + proto.enum + ":")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + '\ttemp.' + proto.name.lower() + ".Convert(parser);")
        data_set["out"].append(tabs + '\t' + "new (&final_" + proto.name.lower() + ") " + proto.declaration + ";")
        data_set["out"].append(tabs + '\t' + "final_" + proto.name.lower() + " = temp." + proto.name.lower() + ";")
        data_set["out"].append(tabs + '\t' + "final_" + proto.name.lower() + ".self_eid = EID(proto_id, " + proto.enum + ");")
        print(data_set["out"][-1])
        data_set["out"].append(tabs + "\tbreak;")
        print(data_set["out"][-1])


def COPY_CONSTRUCTOR_ALLPROTOTYPE_SWITCH(data_set):
    pass


def TEMPLATE_FINAL_ENTITY_CASTS(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        data_set["out"].append(tabs + "operator " + proto.proper_name + "Prototype() const { return final_" + proto.name.lower() + "; };")
        print(data_set["out"][-1])


def DROP_ENTITY_TYPES(data_set):
    global line_counter
    line = data_set["data"][line_counter]

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for proto in prototoypes:
        data_set["out"].append(tabs + "ENTITY_" + proto.name + ",")
        print(data_set["out"][-1])


def READ_PBEVENT_ENUM(data_set):
    global line_counter
    line = ""
    # Append PYTHON START line
    while "//PYTHON STOP HERE" not in line:
        line = data_set["data"][line_counter]
        data_set["out"].append(line)
        if "PB" in line:
            match = re.search("(PB_([a-zA-Z_]+)),?", line)
            if match is not None:
                print(match.group(1))
                pbEvents.append(pbEvent(match.group(1)))
        line_counter += 1


def ENUM_TO_TEXT_MAP(data_set):
    global line_counter
    line = data_set["data"][line_counter]
    max_length = 0

    for pbE in pbEvents:
        max_length = len(pbE.enum_text) if len(pbE.enum_text) > max_length else max_length

    # Append PYTHON START line
    data_set["out"].append(line)
    tabs = '\t' * line.count('\t')
    while "//PYTHON STOP HERE" not in line:
        line_counter += 1
        line = data_set["data"][line_counter]

    for pbE in pbEvents:
        data_set["out"].append(tabs + '\t' + '{' + pbE.enum_text + ',' + ' ' * (max_length - len(pbE.enum_text)) + ' "' + (pbE.enum_text.replace("PB_EVENT_", "")) + '"},')
        print(data_set["out"][-1])


def maintain(data_set):
    global line_counter
    line_counter = 0
    while line_counter < len(data_set["data"]):
        line = data_set["data"][line_counter]
        if "//PYTHON START HERE" in line:
            if "PARSE_TYPES" in line:
                parse_types(data_set)
            elif "READ_PROTOTYPES" in line:
                read_prototype(data_set)
            elif "DECLARE_UNION" in line:
                declare_union(data_set)
            #elif "CONSTRUCTOR_DECLARATION" in line:
            #    constructor_declaration(data_set)
            elif "OPERATOR=_SWITCH" in line:
                operator_equal_switch(data_set)
            elif "DESTRUCTOR_SWITCH" in line:
                destructor_switch(data_set)
            elif "PARSE_DEFINITION" in line:
                parse_definition(data_set)
            elif "STRING_TO_TYPE_MAP" in line:
                string_to_type(data_set)
            elif "STRING_TO_DECLARATION" in line:
                string_to_declaration(data_set) # not done
            elif "PARSE_DECLARATION" in line:
                parse_declaration(data_set) # not done
            elif "PARSE_ROW_SWITCH" in line:
                parse_row_switch(data_set) # not done
            elif "PARSER_PROTOTYPES_CLASSES" in line:
                parser_prototypes_classes(data_set)
            elif "PARSER_PROTOTYPE_CLASSES_CONVERT_FUNCTIONS" in line:
                parser_prototype_classes_convert_functions(data_set)
            elif "CONSTRUCTOR_DEFINITION_SWITCH" in line:
                constructor_definition_switch(data_set)
            elif "FINALIZE_ALLPROTOTYPE_SWITCH" in line:
                finalize_allprototype_switch(data_set)
            elif "COPY_CONSTRUCTOR_ALLPROTOTYPE_SWITCH" in line:
                COPY_CONSTRUCTOR_ALLPROTOTYPE_SWITCH(data_set)
            elif "TEMPLATE_FINAL_ENTITY_CASTS" in line:
                TEMPLATE_FINAL_ENTITY_CASTS(data_set)
            elif "DROP_ENTITY_TYPES" in line:
                DROP_ENTITY_TYPES(data_set)
            elif "READ_PBEVENT_ENUM" in line:
                READ_PBEVENT_ENUM(data_set)
            elif "ENUM_TO_TEXT_MAP" in line:
                ENUM_TO_TEXT_MAP(data_set)
            else:
                data_set["out"].append(line)
                line_counter += 1
        else:
            # If we aren't parsing using rules the code should be unchanged.
            data_set["out"].append(line)
            line_counter += 1


if __name__ == '__main__':
    # entities must be read first as it has the enum in it
    file_dict = [common_header, common_cpp, prototypes_header,entities, parsercpp, parser, parserproto, parserprotocpp]

    for data_set in file_dict:
        f = open(data_set["file"], 'r', newline='')
        for line in f:
            data_set["data"].append(line)
        f.close()

    for data_set in file_dict:
        maintain(data_set)


    for data_set in file_dict:
        f = open(data_set["file"], 'w', newline='')
        for out in data_set["out"]:
            if out.endswith('\r\n'):
                f.write(out)
            else:
                f.write(out + '\r\n')
        f.close()
