#include "Game_Map.h"
#include "Entities.h"
#include "common.h"

using namespace pb;

EntityType EntityBase::get_type()
{
	//If parent is a prototype we are an entity of that prototype
	if (parent_eid.type == ENTITY_TYPE_NULL)
		return ENTITY_TYPE_NULL;
	if (parent_eid.type < PROTOTYPE_END_ENUM)
	{
		//0 is NULL
		//1 is FIRST_PROTOTYPE
		//PROTOTOTYPE_END_ENUM = last_proto + 1
		//First Entity = PROTOTOTYPE_END_ENUM + 1 
		//FIRST_PROTOTYPE + PROTOTYPE_END_ENUM = 1 + PROTOTYPE_END_ENUM = FIRST_ENTITY
		return (EntityType)(parent_eid.type + PROTOTYPE_END_ENUM);
	}
	else if (parent_eid.type == ENTITY_FLUID_BODY)
	{
		//If parent is a fluid body we are a fluid block, we have no prototype in this situation
		return ENTITY_FLUID_BLOCK;
	}
	return ENTITY_TYPE_NULL;
}

EntityBase::EntityBase(const Prototype* parent)
{
	parent_eid = parent->self_eid;
}

EntityBase::EntityBase(EID in_parent_eid)
{
	parent_eid = in_parent_eid;
}

EntityBase::EntityBase(bj::value val, const std::map<EID, EID>& eid_map)
{
	Load_EntityBase_FromJSON(val, eid_map);
}

bj::object EntityBase::toJSON() const
{
	bj::object obj;
	obj.insert_or_assign("peid", parent_eid.toJSON());
	return obj;
}

void EntityBase::Load_EntityBase_FromJSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	parent_eid.fromJSON(val.at("peid"));
	if(parent_eid.type < PROTOTYPE_END_ENUM && parent_eid.type > ENTITY_TYPE_NULL) //Only remap prototypes
		parent_eid = eid_map.at(parent_eid);
}

bool Map_Block::isVacuum()
{
	switch (get_type())
	{
	case ENTITY_TYPE_NULL:
		return true;
	default:
		return false;
	}
}

bool Map_Block::isFluid()
{
	switch (get_type())
	{
	case ENTITY_TYPE_NULL:
	case ENTITY_FLUID:
	case ENTITY_FLUID_BLOCK:
	case ENTITY_FLUID_BODY:
		return true;
	default:
		return false;
	}
}

Fluid::Fluid(const FluidPrototype* parent) : EntityBase(parent)
{
	quantity = 0;
}

Fluid::Fluid(const EID& id) : EntityBase(id)
{
	quantity = 0;
}

Fluid::Fluid(bj::value val, const std::map<EID, EID>& eid_map)
{
	Load_Fluid_FromJSON(val, eid_map);
}

bool Fluid::operator==(const Fluid& other) const
{
	return parent_eid.id ==  other.parent_eid.id && parent_eid.type == other.parent_eid.type;
}

bool Fluid::operator!=(const Fluid& other) const
{
	return !(operator==(other));
}

Map_Block::Map_Block(const BlockPrototype* parent) : EntityBase(parent)
{
	block_internals.hp = 100;
}

Map_Block::Map_Block(const FluidBody* parent) : EntityBase(parent->self_eid)
{
	fluid_internals.placed_entity = EID();
}

Map_Block::Map_Block(bj::value val, const std::map<EID, EID>& eid_map)
{
	Load_Block_FromJSON(val, eid_map);
}

Map_Block::Map_Block() : EntityBase(EID())
{
	fluid_internals.placed_entity = EID();
}

bj::object Map_Block::toJSON()
{
	bj::object obj = EntityBase::toJSON();
	if(isFluid())
		obj.insert_or_assign("feid", fluid_internals.placed_entity.toJSON());
	else
		obj.insert_or_assign("hp", block_internals.hp);
	return obj;
}

void Map_Block::Load_Block_FromJSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	EntityBase::Load_EntityBase_FromJSON(val, eid_map);
	if (isFluid())
		fluid_internals.placed_entity.fromJSON(val.at("feid")); // Only change "prototype eid", entity ids are map specific
	else
		block_internals.hp = val.at("hp").as_int64();

}

void FluidBody::clear()
{
	total = 0;
	self_eid = EID(-1, ENTITY_TYPE_NULL);
	constituent_fluids.clear();
}

void FluidBody::Add_Fluid(Fluid in_fluid)
{
	auto success = constituent_fluids.insert(in_fluid);
	if (!success.second)
	{
		success.first->quantity += in_fluid.quantity;
	}
	total += in_fluid.quantity;
}

Fluid FluidBody::Get_Fluid(EID eid)
{
	auto iter = constituent_fluids.find(Fluid(eid));
	if (iter != constituent_fluids.end())
		return *iter;
	else
		return Fluid(EID());
}

std::vector<Fluid> FluidBody::Get_Fluids()
{
	std::vector<Fluid> result;
	for (auto& fluid : constituent_fluids)
		result.push_back(fluid);
	return result;
}

int FluidBody::Remove_Fluid_Quantity(EID fluid_id, int quantity)
{
	auto x = constituent_fluids.find(Fluid(fluid_id));
	if (fluid_id.type != PROTOTYPE_FLUID || x == constituent_fluids.end())
		return 0;
	
	if (x->quantity < quantity)
	{
		quantity = x->quantity;
	}
	x->quantity -= quantity;
	total -= quantity;
	return quantity;	
}

FluidBody FluidBody::operator+(const FluidBody& other) const
{
	FluidBody merged_body;
	merged_body.total = total + other.total;

	for (auto& fluid : constituent_fluids)
	{
		merged_body.Add_Fluid(fluid);
	}

	for (auto& fluid : other.constituent_fluids)
	{
		merged_body.Add_Fluid(fluid);
	}

	return merged_body;
}

FluidBody::FluidBody(bj::value val, const std::map<EID, EID>& eid_map)
{
	Load_FluidBody_FromJSON(val, eid_map);
}

bj::object FluidBody::toJSON() const
{
	bj::object obj;
	obj.insert_or_assign("total", total);
	obj.insert_or_assign("eid", self_eid.toJSON());
	bj::array arr;
	for (const Fluid& fluid : constituent_fluids)
	{
		arr.push_back(fluid.toJSON());
	}
	obj.insert_or_assign("fluids", arr);
	return obj;
}

void FluidBody::Load_FluidBody_FromJSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	constituent_fluids.clear();
	total = val.at("total").as_int64();
	self_eid.fromJSON(val.at("eid"));
	Fluid reader(self_eid);
	for (auto& fluid : val.at("fluids").as_array())
	{
		reader.Load_Fluid_FromJSON(fluid, eid_map);
		constituent_fluids.insert(reader);
	}
}

void Plant::Breath(EntityHolder* parent)
{
	FluidBody* containing_fluid = parent->Get_Fluid(location);
	for (int i = 0; i < RECIPE_BUFFER_SIZE; ++i)
	{
		int quantity = containing_fluid->Remove_Fluid_Quantity(recipe->ingredient_id[i], recipe->ingredient_quantity[i] - input_buffer[i]);
		input_buffer[i] += quantity;
	}
}

Plant::Plant(const PlantPrototype* parent, Coordinate coordinate, RecipePrototype* recipe) : Constructor(parent, coordinate, recipe)
{

}

Plant::Plant(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent)
{
	Load_Plant_FromJSON(val, eid_map, parent);
}

void Plant::Update(EntityHolder* parent)
{
	working = true;
	Breath(parent);
	Constructor::Update();
}

bj::object Plant::toJSON() const
{
	return Constructor::toJSON();
}

void Plant::Load_Plant_FromJSON(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent)
{
	return Load_Constructor_FromJSON(val, eid_map, parent);
}


Constructor::ConstructorState Constructor::getState()
{
	if (progress > 0)
	{
		return ConstructorState::IN_PROGRESS;
	}
	if (progress == 0)
	{
		return ConstructorState::FINISHED;
	}
	if (progress == -1)
	{
		return ConstructorState::INPUT;
	}
	throw std::logic_error("Constructor in Invalid State");
	return ConstructorState();
}

void Constructor::ProgressState()
{
	switch (getState())
	{
	case ConstructorState::IN_PROGRESS:
		if (working)
			progress--;
		break;
	case ConstructorState::FINISHED:
		progress--;
		break;
	case ConstructorState::INPUT:
		if(recipe)
			progress = recipe->energy_cost;
		break;
	}
}

Constructor::Constructor(const ConstructorPrototype* parent, Coordinate coordinate, RecipePrototype* in_recipe=nullptr) : PhysicalEntity(parent, coordinate)
{
	recipe = in_recipe;
	for (int i = 0; i < RECIPE_BUFFER_SIZE; ++i)
	{
		input_buffer[i] = 0;
	}
	progress = -1; //INPUT
}

Constructor::Constructor(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent)
{
	Load_Constructor_FromJSON(val, eid_map, parent);
}

bool Constructor::Create_Output()
{
	if (recipe)
	{
		for (int i = 0; i < RECIPE_BUFFER_SIZE; ++i)
		{
			output_buffer[i] += recipe->output_quantity[i];
		}
	}
	else {
		throw std::runtime_error("Attempted to create output without a recipe!");
	}
	return false;
}

bool Constructor::Consume_Input()
{
	if (recipe && !isInputBlocked())
	{
		for (int i = 0; i < RECIPE_BUFFER_SIZE; ++i)
		{
			input_buffer[i] -= recipe->ingredient_quantity[i];
		}
	}
	else {
		throw std::runtime_error("Attempted to Consume Input without a recipe!");
	}
	return false;
}

bool Constructor::isFinished()
{
	return getState() == ConstructorState::FINISHED;
}

bool Constructor::isInputBlocked()
{
	if (recipe)
	{
		for (int i = 0; i < RECIPE_BUFFER_SIZE; ++i)
		{
			if (recipe->ingredient_quantity[i] > input_buffer[i] || 
				output_buffer[i] > 2 * recipe->output_quantity[i])
			{
				return true;
			}
		}
		return false;
	}
	//Some sort of math to figure out an acceptable amount of stuff to buffer
	return true;
}

bool Constructor::isWaiting()
{
	return getState() == ConstructorState::INPUT;
}

bool Constructor::inProgress()
{
	return getState() == ConstructorState::IN_PROGRESS;
}

void Constructor::Update()
{
	if (working)
	{
		switch (getState())
		{
		case ConstructorState::IN_PROGRESS:
			ProgressState();
			break;
		case ConstructorState::FINISHED:
			Create_Output();
			ProgressState();
			//Continue to input
		case ConstructorState::INPUT:
			if (!isInputBlocked())
			{
				Consume_Input();
				ProgressState();
			}
			break;
		}
	}
}

bj::object Constructor::toJSON() const
{
	bj::object obj = PhysicalEntity::toJSON();
	obj.insert_or_assign("progress", progress);
	obj.insert_or_assign("working", working);
	obj.insert_or_assign("recipe", recipe->get_EID().toJSON());
	obj.insert_or_assign("input_buffer", base64_encoder(input_buffer, RECIPE_BUFFER_SIZE));
	obj.insert_or_assign("output_buffer", base64_encoder(output_buffer, RECIPE_BUFFER_SIZE));
	return obj;
}

void Constructor::Load_Constructor_FromJSON(bj::value val, const std::map<EID, EID>& eid_map, EntityHolder* parent)
{
	PhysicalEntity::Load_PhysicalEntity_FromJSON(val, eid_map);
	progress = val.at("progress").as_int64();
	working = val.at("working").as_bool();
	recipe = (RecipePrototype*)parent->Get_Prototype(eid_map.at(EID().fromJSON(val.at("recipe"))));
	base64_decoder(input_buffer, std::string(val.at("input_buffer").as_string().c_str()));
	base64_decoder(output_buffer, std::string(val.at("output_buffer").as_string().c_str()));
}

PhysicalEntity::PhysicalEntity(const Prototype* proto, Coordinate loc) : EntityBase(proto)
{
	location = loc;
}

PhysicalEntity::PhysicalEntity(bj::value val, const std::map<EID, EID>& eid_map)
{
	Load_PhysicalEntity_FromJSON(val, eid_map);
}

bj::object PhysicalEntity::toJSON() const
{
	bj::object obj = EntityBase::toJSON();
	obj.insert_or_assign("location", location.toJSON());
	return obj;
}

void PhysicalEntity::Load_PhysicalEntity_FromJSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	EntityBase::Load_EntityBase_FromJSON(val, eid_map);
	location.fromJSON(val.at("location"));
}
Item::Item(const Prototype* prot, Coordinate coord) : PhysicalEntity(prot, coord)
{
}

Item::Item(bj::value val, const std::map<EID, EID>& eid_map)
{
	Load_Item_FromJSON(val, eid_map);
}

bj::object Item::toJSON() const
{
	bj::object obj = PhysicalEntity::toJSON();
	return obj;
}

void Item::Load_Item_FromJSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	PhysicalEntity::Load_PhysicalEntity_FromJSON(val, eid_map);
}

bj::object Fluid::toJSON() const
{
	bj::object obj = EntityBase::toJSON();
	obj.insert_or_assign("quantity", quantity);
	return obj;
}

void Fluid::Load_Fluid_FromJSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	EntityBase::Load_EntityBase_FromJSON(val, eid_map);
	quantity = val.at("quantity").as_int64();
}