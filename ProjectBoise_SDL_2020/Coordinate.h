#pragma once
#include <string>
#include <random>
#include <math.h>
#include <iostream>
#include <boost/json.hpp>

namespace pb
{
	namespace  bj = boost::json;
	//TODO: Migrate to a random engine system that is saveable
	extern std::default_random_engine rand_eng;

	template <class T> class CoordinateBaseNeighborIterator;

	template <class T>
	class CoordinateBase
	{
	private:
	public:
		T x, y;

		CoordinateBase() {
			x = y = T();
		}
		CoordinateBase(T ix, T iy) {
			x = ix;
			y = iy;
		}
		std::string toString() const {
			std::string message = "";
			message += "X:";
			message += std::to_string(x);
			message += "Y:";
			message += std::to_string(y);
			return message;
		};

		bj::object toJSON() const{
			bj::object obj;
			obj.insert_or_assign("x", x);
			obj.insert_or_assign("y", y);
			return obj;
		}

		void fromJSON(bj::value val) {
			x = val.at("x").is_double() ? (T)val.at("x").as_double() : (T)val.at("x").as_int64();
			y = val.at("y").is_double() ? (T)val.at("y").as_double() : (T)val.at("y").as_int64();
		};

		CoordinateBaseNeighborIterator<T> Get_Neighbors() {
			return CoordinateBaseNeighborIterator<T>(*this);
		}

		static inline bool Is_Between(CoordinateBase a, CoordinateBase middle, CoordinateBase c)
		{
			bool a_c_x = (a.x <= middle.x && c.x >= middle.x);
			bool c_a_x = (a.x >= middle.x && c.x <= middle.x);

			bool a_c_y = (a.y <= middle.y && c.y >= middle.y);
			bool c_a_y = (a.y >= middle.y && c.y <= middle.y);

			return (a_c_x || c_a_x) && (a_c_y || c_a_y);
		}

		static inline CoordinateBase<T> Get_Random_Coordinate_Between(CoordinateBase a, CoordinateBase b) 
		{
			auto x_negative = (a.x < b.x) ? 1 : -1;
			auto y_negative = (a.y < b.y) ? 1 : -1;
			auto x_plus = (a.x == b.x) ? 0 : rand_eng() % abs(a.x - b.x);
			auto y_plus = (a.y == b.y) ? 0 : rand_eng() % abs(a.y - b.y);
			return CoordinateBase<T>(x_negative * x_plus + a.x, y_negative * y_plus + a.y);
		}

		double Approx_Distance(CoordinateBase other) const {
			return Exact_Distance(other); // Optmize if needed
		}

		double Exact_Distance(CoordinateBase other) const {
			return sqrt(pow(other.x - x, 2) + pow(other.y - y, 2));
		}
		int IDistance(CoordinateBase other) const {
			return abs(other.x - x) + abs(other.y - y); // Could approximate diagonals, but pawns have to move in lines anyway.
		}


		CoordinateBase<T> operator+(CoordinateBase<T> other) const {
			return CoordinateBase<T>(x + other.x, y + other.y);
		}
		CoordinateBase<T> operator-(CoordinateBase<T> other) const {
			return CoordinateBase(x - other.x, y - other.y);
		}
		bool operator==(CoordinateBase<T> other) const {
			return x == other.x && y == other.y;
		}
		bool operator<(CoordinateBase<T> other) const {
			if (y < other.y)
				return true;
			else if (y == other.y && x < other.x)
				return true;
			return false;
		}
		bool operator>(CoordinateBase<T> other) const {
			if (y > other.y)
				return true;
			else if (y == other.y && x > other.x)
				return true;
			return false;
		}
		bool operator!=(CoordinateBase<T> other) const {
			return (x != other.x) || (y != other.y);
		}
		CoordinateBase<T> operator*(T i) const {
			return CoordinateBase(x * i, y * i);
		}
		CoordinateBase<T> operator/(T i) const {
			return CoordinateBase(x / i, y / i);
		}
		CoordinateBase<T> operator%(T i) const {
			return CoordinateBase(x % i, y % i);
		}
		operator CoordinateBase<double>() const {
			return CoordinateBase<double>(x, y);
		}
		operator CoordinateBase<int>() const {
			return CoordinateBase<int>(x, y);
		}
		friend std::istream& operator>>(std::istream& os, CoordinateBase<T>& dt) {
			os >> dt.x >> dt.y;
			return os;
		}
		friend std::ostream& operator<<(std::ostream& os, const CoordinateBase<T>& dt) {
			os << dt.x << " " << dt.y;
			return os;
		}
	};
	
	template <class T>
	class RectangleBase
	{
	private:
		void Maintain(){
			if (top_l.x > bot_r.x){
				CoordinateBase<T> temp = top_l;
				top_l.x = bot_r.x;
				bot_r.x = temp.x;
			}
			if (top_l.y > bot_r.y){
				CoordinateBase<T> temp = top_l;
				top_l.y = bot_r.y;
				bot_r.y = temp.y;
			}
		}

		CoordinateBase<T> top_l, bot_r;
	public:
		RectangleBase(CoordinateBase<T> a, CoordinateBase<T> b) {
			top_l = a;
			bot_r = b;
			Maintain();
		}
		
		RectangleBase() {
			top_l = CoordinateBase<T>();
			bot_r = CoordinateBase<T>();
			Maintain();
		}

		bj::object toJSON() const{
			bj::object obj;
			obj.insert_or_assign("top_l", top_l.toJSON());
			obj.insert_or_assign("bot_r", bot_r.toJSON());
			return obj;
		}

		void fromJSON(bj::value val) {
			top_l.fromJSON(val.at("top_l"));
			bot_r.fromJSON(val.at("bot_r"));
		};

		T Width() const {
			return bot_r.x - top_l.x;
		}
		T Height() const {
			return bot_r.x - top_l.x;
		}
		T Left() const {
			return top_l.x;
		}
		T Right() const {
			return bot_r.x;
		}
		T Bottom() const {
			return bot_r.y;
		}
		T Top() const {
			return top_l.y;
		}
		CoordinateBase<T> Top_Left() const{
			return top_l;
		}
		CoordinateBase<T> Bot_Right() const {
			return bot_r;
		}
		CoordinateBase<T> Top_Right() const {
			return CoordinateBase<T>(bot_r.x, top_l.y);
		}
		CoordinateBase<T> Bot_Left() const {
			return CoordinateBase<T>(top_l.x, bot_r.y);
		}
		CoordinateBase<T> Center() const {
			return top_l + (bot_r - top_l) / 2;
		}

		void Move_TopLeft(CoordinateBase<T> a){
			bot_r = CoordinateBase<T>(a.x + Width(), a.y + Height());
			top_l = a;
		}
		void Move_Center(CoordinateBase<T> a){
			CoordinateBase<T> offset = a - Center();
			top_l = top_l + offset;
			bot_r = bot_r + offset;
		}
		friend std::istream& operator>>(std::istream& os, RectangleBase<T>& dt) {
			os >> dt.top_l >> dt.bot_r;
			return os;
		}
		friend std::ostream& operator<<(std::ostream& os, const RectangleBase<T>& dt) {
			os << dt.top_l << " " << dt.bot_r;
			return os;
		}
	};

	typedef CoordinateBase<int> Coordinate;
	typedef CoordinateBase<double> FCoordinate;

	typedef RectangleBase<int> Rectangle;
	typedef RectangleBase<double> FRectangle;

	template <class T>
	class CoordinateBaseNeighborIterator
	{
	private:
		int iteration = 0;
		CoordinateBase<T> root;
	public:
		CoordinateBaseNeighborIterator(CoordinateBase<T> inroot) {
			root = inroot;
		};
		CoordinateBaseNeighborIterator<T>& next() {
			iteration < 8 ? iteration++ : iteration;
			return *this;
		};
		CoordinateBaseNeighborIterator<T>& prev() {
			iteration > 0 ? iteration-- : iteration;
			return *this;
		};
		operator CoordinateBase<T>() {
			return operator[](iteration);
		};
		CoordinateBase<T> operator*() {
			return operator[](iteration);
		};
		bool operator!=(const CoordinateBaseNeighborIterator<T>& other) {
			return root != other.root || iteration != other.iteration;
		};
		CoordinateBase<T> operator[](int x) {
			switch (iteration)
			{
			case 0:
				return root - CoordinateBase<T>(-1, -1);
			case 1:
				return root - CoordinateBase<T>(0, -1);
			case 2:
				return root - CoordinateBase<T>(1, -1);
			case 3:
				return root - CoordinateBase<T>(-1, 0);
			case 4:
				return root - CoordinateBase<T>(1, 0);
			case 5:
				return root - CoordinateBase<T>(-1, 1);
			case 6:
				return root - CoordinateBase<T>(0, 1);
			case 7:
				return root - CoordinateBase<T>(1, 1);
			case 8:
				return root;
			default:
				return root;
			}
		};
		CoordinateBaseNeighborIterator<T> end() {
			CoordinateBaseNeighborIterator<T> end(root);
			end.iteration = 8;
			return end;
		};
		CoordinateBaseNeighborIterator<T> begin() {
			CoordinateBaseNeighborIterator<T> begin(root);
			begin.iteration = 0;
			return begin;
		};
		bool isLast() {
			return iteration == 8;
		};
		CoordinateBaseNeighborIterator<T>& operator++() {
			next();
			return *this;
		};
		CoordinateBaseNeighborIterator<T>& operator--() {
			prev();
			return *this;
		};
	};

	template<class T>
	struct WorldCoordinateBase  : public CoordinateBase<T>
	{
		int world_id;
		WorldCoordinateBase() : CoordinateBase<T>() {
			world_id = 0;
		};
		
		WorldCoordinateBase(T a, T b, int id = 0) : CoordinateBase<T>(a, b) {
			world_id = id;
		};
		WorldCoordinateBase(CoordinateBase<T> coord, int id = 0) : CoordinateBase<T>(coord) {
			world_id = id;
		};

		bj::object toJSON() const{
			bj::object obj = CoordinateBase<T>::toJSON();
			obj.insert_or_assign("world_id", world_id);
			return obj;
		}

		void fromJSON(bj::value val) {
			CoordinateBase<T>::fromJSON(val);
			world_id = val.at("world_id").as_int64();
		};

		friend std::istream& operator>>(std::istream& os, WorldCoordinateBase<T>& dt) {
			operator>>(os, (CoordinateBase<T>&)dt);
			os >> dt.world_id;
			return os;
		}
		friend std::ostream& operator<<(std::ostream& os, const WorldCoordinateBase<T>& dt) {
			os << (CoordinateBase<T>&)dt;
			os << " " << dt.world_id;
			return os;
		}
	};

	template<class T>
	struct WorldRectangleBase : public RectangleBase<T>
	{
		int world_id;
		WorldRectangleBase() : RectangleBase<T>() {
			world_id = 0;
		};
		WorldRectangleBase(CoordinateBase<T> a, CoordinateBase<T> b, int id=0) : RectangleBase<T>(a,b){
			world_id = id;
		};
		WorldRectangleBase(RectangleBase<T> rect, int id=0) : RectangleBase<T>(rect) {
			world_id = id;
		};
		bj::object toJSON() const{
			bj::object obj = RectangleBase<T>::toJSON();
			obj.insert_or_assign("world_id", world_id);
			return obj;
		}

		void fromJSON(bj::value val) {
			RectangleBase<T>::fromJSON(val);
			world_id = val.at("world_id").as_int64();
		};
		friend std::istream& operator>>(std::istream& os, WorldRectangleBase<T>& dt) {
			operator>>(os, (RectangleBase<T>&)dt);
			os >> dt.world_id;
			return os;
		}
		friend std::ostream& operator<<(std::ostream& os, const WorldRectangleBase<T>& dt){
			operator<<(os, (RectangleBase<T>&)dt);
			os << " " << dt.world_id;
			return os;
		}
	};

	typedef WorldCoordinateBase<int> WorldCoordinate;
	typedef WorldCoordinateBase<double> FWorldCoordinate;

	typedef WorldRectangleBase<int> WorldRectangle;
	typedef WorldRectangleBase<double> FWorldRectangle;
}