#include "ParserPrototypes.h"
#include "Parser.h"
#include "common.h"
#include  <iostream>

using namespace pb;

//PYTHON START HERE PARSER_PROTOTYPE_CLASSES_CONVERT_FUNCTIONS
	void ParserFluidPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserBlockPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserItemPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserParticlePrototype::Convert(Parser* parser)
	{
	};
	
	void ParserConstructorPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserFurniturePrototype::Convert(Parser* parser)
	{
	};
	
	void ParserConstructionPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserRecipePrototype::Convert(Parser* parser)
	{
		for(int iter = 0; iter < RECIPE_BUFFER_SIZE;++iter)
			ingredient_id[iter] = parser->Lookup_EID(string_ingredient_id[iter]);

		for(int iter = 0; iter < RECIPE_BUFFER_SIZE;++iter)
			output_id[iter] = parser->Lookup_EID(string_output_id[iter]);

	};
	
	void ParserMapgenPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserPawnPrototype::Convert(Parser* parser)
	{
	};
	
	void ParserPlantPrototype::Convert(Parser* parser)
	{
		recipe = parser->Lookup_EID(string_recipe);
	};
	
//PYTHON STOP HERE PARSER_PROTOTYPE_CLASSES_CONVERT_FUNCTIONS

void AllPrototypeUnion::ClearData()
{
	switch (type)
	{
	case ENTITY_TYPE_NULL:
		break;
		//PYTHON START HERE DESTRUCTOR_SWITCH
		case PROTOTYPE_FLUID:
			 is_final ? fluid.~ParserFluidPrototype() : final_fluid.~FluidPrototype();
			break;
		case PROTOTYPE_BLOCK:
			 is_final ? block.~ParserBlockPrototype() : final_block.~BlockPrototype();
			break;
		case PROTOTYPE_ITEM:
			 is_final ? item.~ParserItemPrototype() : final_item.~ItemPrototype();
			break;
		case PROTOTYPE_PARTICLE:
			 is_final ? particle.~ParserParticlePrototype() : final_particle.~ParticlePrototype();
			break;
		case PROTOTYPE_CONSTRUCTOR:
			 is_final ? constructor.~ParserConstructorPrototype() : final_constructor.~ConstructorPrototype();
			break;
		case PROTOTYPE_FURNITURE:
			 is_final ? furniture.~ParserFurniturePrototype() : final_furniture.~FurniturePrototype();
			break;
		case PROTOTYPE_CONSTRUCTION:
			 is_final ? construction.~ParserConstructionPrototype() : final_construction.~ConstructionPrototype();
			break;
		case PROTOTYPE_RECIPE:
			 is_final ? recipe.~ParserRecipePrototype() : final_recipe.~RecipePrototype();
			break;
		case PROTOTYPE_MAPGEN:
			 is_final ? mapgen.~ParserMapgenPrototype() : final_mapgen.~MapgenPrototype();
			break;
		case PROTOTYPE_PAWN:
			 is_final ? pawn.~ParserPawnPrototype() : final_pawn.~PawnPrototype();
			break;
		case PROTOTYPE_PLANT:
			 is_final ? plant.~ParserPlantPrototype() : final_plant.~PlantPrototype();
			break;
		//PYTHON STOP HERE DESTRUCTOR_SWITCH
	default:
		throw std::runtime_error(std::string("Failed to destruct AllPrototypeUnion: ") + std::to_string(type));
	}
	text = textInformation();

	proto_id = 0;
	sprite_id = 0;
	is_final = false;
	type = ENTITY_TYPE_NULL;
}

void pb::AllPrototypeUnion::ClearProtoData()
{
}

AllPrototypeUnion::AllPrototypeUnion(EntityType t)
{
	type = t;
	switch (t)
	{
		case ENTITY_TYPE_NULL:
			new (&base) Prototype;
			break;
		//PYTHON START HERE CONSTRUCTOR_DEFINITION_SWITCH
		case PROTOTYPE_FLUID:
			new (&fluid) ParserFluidPrototype;
			break;
		case PROTOTYPE_BLOCK:
			new (&block) ParserBlockPrototype;
			break;
		case PROTOTYPE_ITEM:
			new (&item) ParserItemPrototype;
			break;
		case PROTOTYPE_PARTICLE:
			new (&particle) ParserParticlePrototype;
			break;
		case PROTOTYPE_CONSTRUCTOR:
			new (&constructor) ParserConstructorPrototype;
			break;
		case PROTOTYPE_FURNITURE:
			new (&furniture) ParserFurniturePrototype;
			break;
		case PROTOTYPE_CONSTRUCTION:
			new (&construction) ParserConstructionPrototype;
			break;
		case PROTOTYPE_RECIPE:
			new (&recipe) ParserRecipePrototype;
			break;
		case PROTOTYPE_MAPGEN:
			new (&mapgen) ParserMapgenPrototype;
			break;
		case PROTOTYPE_PAWN:
			new (&pawn) ParserPawnPrototype;
			break;
		case PROTOTYPE_PLANT:
			new (&plant) ParserPlantPrototype;
			break;
		//PYTHON STOP HERE CONSTRUCTOR_DEFINITION_SWITCH
	default:
		throw std::runtime_error("Failed to construct AllPrototypeUnion type: " + std::to_string(type));
	}
}

AllPrototypeUnion::AllPrototypeUnion(const AllPrototypeUnion& in_all_type) : AllPrototypeUnion(in_all_type.type)
{
	try {
		*this = in_all_type;
	}
	catch (std::runtime_error err) {
		throw std::runtime_error(std::string("Failed to copy construct AllPrototypeUnion type: ") + std::to_string(in_all_type.type) + "\n" + err.what());
	}
}

AllPrototypeUnion& AllPrototypeUnion::operator=(const AllPrototypeUnion& in_all_type)
{
	if (this == &in_all_type)
		return *this;

	ClearData();
	switch (in_all_type.type)
	{
		case ENTITY_TYPE_NULL:
			new (&base) Prototype;
			base = in_all_type.base;
			break;
		//PYTHON START HERE OPERATOR=_SWITCH
		case PROTOTYPE_FLUID:
			in_all_type.is_final ? new (&final_fluid) FluidPrototype : new (&fluid) ParserFluidPrototype;
			in_all_type.is_final ? final_fluid = in_all_type.final_fluid : fluid = in_all_type.fluid;
			break;
		case PROTOTYPE_BLOCK:
			in_all_type.is_final ? new (&final_block) BlockPrototype : new (&block) ParserBlockPrototype;
			in_all_type.is_final ? final_block = in_all_type.final_block : block = in_all_type.block;
			break;
		case PROTOTYPE_ITEM:
			in_all_type.is_final ? new (&final_item) ItemPrototype : new (&item) ParserItemPrototype;
			in_all_type.is_final ? final_item = in_all_type.final_item : item = in_all_type.item;
			break;
		case PROTOTYPE_PARTICLE:
			in_all_type.is_final ? new (&final_particle) ParticlePrototype : new (&particle) ParserParticlePrototype;
			in_all_type.is_final ? final_particle = in_all_type.final_particle : particle = in_all_type.particle;
			break;
		case PROTOTYPE_CONSTRUCTOR:
			in_all_type.is_final ? new (&final_constructor) ConstructorPrototype : new (&constructor) ParserConstructorPrototype;
			in_all_type.is_final ? final_constructor = in_all_type.final_constructor : constructor = in_all_type.constructor;
			break;
		case PROTOTYPE_FURNITURE:
			in_all_type.is_final ? new (&final_furniture) FurniturePrototype : new (&furniture) ParserFurniturePrototype;
			in_all_type.is_final ? final_furniture = in_all_type.final_furniture : furniture = in_all_type.furniture;
			break;
		case PROTOTYPE_CONSTRUCTION:
			in_all_type.is_final ? new (&final_construction) ConstructionPrototype : new (&construction) ParserConstructionPrototype;
			in_all_type.is_final ? final_construction = in_all_type.final_construction : construction = in_all_type.construction;
			break;
		case PROTOTYPE_RECIPE:
			in_all_type.is_final ? new (&final_recipe) RecipePrototype : new (&recipe) ParserRecipePrototype;
			in_all_type.is_final ? final_recipe = in_all_type.final_recipe : recipe = in_all_type.recipe;
			break;
		case PROTOTYPE_MAPGEN:
			in_all_type.is_final ? new (&final_mapgen) MapgenPrototype : new (&mapgen) ParserMapgenPrototype;
			in_all_type.is_final ? final_mapgen = in_all_type.final_mapgen : mapgen = in_all_type.mapgen;
			break;
		case PROTOTYPE_PAWN:
			in_all_type.is_final ? new (&final_pawn) PawnPrototype : new (&pawn) ParserPawnPrototype;
			in_all_type.is_final ? final_pawn = in_all_type.final_pawn : pawn = in_all_type.pawn;
			break;
		case PROTOTYPE_PLANT:
			in_all_type.is_final ? new (&final_plant) PlantPrototype : new (&plant) ParserPlantPrototype;
			in_all_type.is_final ? final_plant = in_all_type.final_plant : plant = in_all_type.plant;
			break;
		//PYTHON STOP HERE OPERATOR=_SWITCH
	default:
		throw std::runtime_error("Failed to assign AllPrototypeUnion type: " + std::to_string(in_all_type.type));
	}
	
	text = in_all_type.text;

	is_final = in_all_type.is_final;
	proto_id = in_all_type.proto_id;
	sprite_id = in_all_type.sprite_id;
	type = in_all_type.type;

	return *this;
}

AllPrototypeUnion::~AllPrototypeUnion()
{
	try {
		ClearData();
	}
	catch (...)
	{
		std::clog << "failed to destruct AllUnionPrototype\n";
	}

}

void AllPrototypeUnion::Finalize(Parser* parser)
{
	AllPrototypeUnion temp = *this; // Store data to convert
	ClearData();
	//Reconstruct using final prototype
	switch (temp.type)
	{
		//PYTHON START HERE FINALIZE_ALLPROTOTYPE_SWITCH
		case PROTOTYPE_FLUID:
			temp.fluid.Convert(parser);
			new (&final_fluid) FluidPrototype;
			final_fluid = temp.fluid;
			final_fluid.self_eid = EID(proto_id, PROTOTYPE_FLUID);
			break;
		case PROTOTYPE_BLOCK:
			temp.block.Convert(parser);
			new (&final_block) BlockPrototype;
			final_block = temp.block;
			final_block.self_eid = EID(proto_id, PROTOTYPE_BLOCK);
			break;
		case PROTOTYPE_ITEM:
			temp.item.Convert(parser);
			new (&final_item) ItemPrototype;
			final_item = temp.item;
			final_item.self_eid = EID(proto_id, PROTOTYPE_ITEM);
			break;
		case PROTOTYPE_PARTICLE:
			temp.particle.Convert(parser);
			new (&final_particle) ParticlePrototype;
			final_particle = temp.particle;
			final_particle.self_eid = EID(proto_id, PROTOTYPE_PARTICLE);
			break;
		case PROTOTYPE_CONSTRUCTOR:
			temp.constructor.Convert(parser);
			new (&final_constructor) ConstructorPrototype;
			final_constructor = temp.constructor;
			final_constructor.self_eid = EID(proto_id, PROTOTYPE_CONSTRUCTOR);
			break;
		case PROTOTYPE_FURNITURE:
			temp.furniture.Convert(parser);
			new (&final_furniture) FurniturePrototype;
			final_furniture = temp.furniture;
			final_furniture.self_eid = EID(proto_id, PROTOTYPE_FURNITURE);
			break;
		case PROTOTYPE_CONSTRUCTION:
			temp.construction.Convert(parser);
			new (&final_construction) ConstructionPrototype;
			final_construction = temp.construction;
			final_construction.self_eid = EID(proto_id, PROTOTYPE_CONSTRUCTION);
			break;
		case PROTOTYPE_RECIPE:
			temp.recipe.Convert(parser);
			new (&final_recipe) RecipePrototype;
			final_recipe = temp.recipe;
			final_recipe.self_eid = EID(proto_id, PROTOTYPE_RECIPE);
			break;
		case PROTOTYPE_MAPGEN:
			temp.mapgen.Convert(parser);
			new (&final_mapgen) MapgenPrototype;
			final_mapgen = temp.mapgen;
			final_mapgen.self_eid = EID(proto_id, PROTOTYPE_MAPGEN);
			break;
		case PROTOTYPE_PAWN:
			temp.pawn.Convert(parser);
			new (&final_pawn) PawnPrototype;
			final_pawn = temp.pawn;
			final_pawn.self_eid = EID(proto_id, PROTOTYPE_PAWN);
			break;
		case PROTOTYPE_PLANT:
			temp.plant.Convert(parser);
			new (&final_plant) PlantPrototype;
			final_plant = temp.plant;
			final_plant.self_eid = EID(proto_id, PROTOTYPE_PLANT);
			break;
		//PYTHON STOP HERE FINALIZE_ALLPROTOTYPE_SWITCH
	default:
		throw std::logic_error(std::string("Failed to finalize type: ") + std::to_string(type));
	}
	is_final = true;

	text = temp.text;

	proto_id = temp.proto_id;
	sprite_id = temp.sprite_id;
	type = temp.type;
}
