#include "GameController.h"
#include "EventHandler.h"
#include "common.h"
#include "sdlheaders.h"
#include <fstream>
#include <iostream>

#include <chrono>
#include "Parser.h"
//Keybinds

using namespace pb;

void pbController::Process_Own_Signal(pbEvent curEvent)
{
	switch (curEvent.type)
	{
	case PB_EVENT_PAUSE_GAME:
		game_running = !game_running;
		break;
	case PB_EVENT_CLOSE_GAME:
		quit = true;
		break;
	case PB_EVENT_START_GAME:
		game_running = true;
		break;
	case PB_EVENT_SAVE_GAME:
		//Probably should have more information but for now just saving at all is desireable
		Save_Game();
		break;
	case PB_EVENT_LOAD_GAME:
		Load_Game();
		break;
	}

}

bj::object pbController::Save_Controller_JSON()
{
	bj::object obj;
	obj.insert_or_assign("event_queue", event_queue.Save_EventQueue_JSON());
	obj.insert_or_assign("game_running", game_running);
	return obj;
}

void pbController::Load_Controller_JSON(bj::value val, const std::map<EID, EID>& eid_map)
{
	event_queue.Load_EventQueue_JSON(val.at("event_queue"));
	for (auto& eve : event_queue)
		eve.convertEIDs(eid_map);
	game_running = val.at("game_running").as_bool();
}

void pbController::Save_Game(std::string save_name)
{
	using namespace std::filesystem;
	//Create game/SaveData/SaveData.json
	path cur_path = current_path();
	cur_path.append("SaveData");
	if (!exists(cur_path))
	{
		create_directory("SaveData");
	}
	cur_path.append(save_name);
	if (!exists(cur_path))
	{
		create_directory(cur_path);
	}
	cur_path.append("SaveData.json");

	bj::value output;
	output.emplace_object();
	output.as_object().insert_or_assign("Controller", Save_Controller_JSON());
	output.as_object().insert_or_assign("UI", ui.Save_UI_JSON());
	output.as_object().insert_or_assign("Game_State", game_engine.Save_Game_Engine_JSON());

	std::ofstream x(cur_path);
	x << output;
}

void pbController::Load_Game(std::string save_name)
{
	using namespace std::filesystem;
	//Create game/SaveData/SaveName/SaveData.json
	path cur_path = current_path();
	cur_path.append("SaveData");
	if (!exists(cur_path))
	{
		create_directory("SaveData");
	}
	cur_path.append(save_name);
	if (!exists(cur_path))
	{
		create_directory(cur_path);
	}
	cur_path.append("SaveData.json");
	if (exists(cur_path))
	{
		std::ifstream stream(cur_path);
		bj::value input = read_json(stream);
		stream.close();

		std::map<EID, EID> eid_map = ui.Load_UI_JSON(input.at("UI"));
		Load_Controller_JSON(input.at("Controller"), eid_map);
		game_engine.Load_Game_Engine_JSON(input.at("Game_State"), eid_map);
	}
}

pbController::pbController() : ui(this), game_engine(this)
{
	event_flag bitmask;

	bitmask.set(PB_EVENT_START_GAME);
	bitmask.set(PB_EVENT_CLOSE_GAME);
	bitmask.set(PB_EVENT_SAVE_GAME);
	bitmask.set(PB_EVENT_LOAD_GAME);
	bitmask.set(PB_EVENT_PAUSE_GAME);

	Register_Listener_Bitmask(bitmask, PB_SUBSYSTEM_CONTROLLER);
}

pbController::~pbController()
{
	SDL_Quit();
}

bool pbController::Quit_Game()
{
	return quit;
}

void pbController::Receive_Input()
{
	quit = true;
}

void pb::pbController::Receive_Input(SDL_Event &sdl_event)
{
	if(!headless)
		menu_base->Receive_SDL_Event(sdl_event);
}

void pbController::Receive_Signal(pb::pbEvent eve)
{
	event_queue.push_back(eve);
}

void pb::pbController::Process_Signals()
{
	while (event_queue.size() > 0)
	{
		pbEvent curEvent = event_queue.pop_front();
		if (listener_bitmask[PB_SUBSYSTEM_CONTROLLER].test(curEvent.type))
			Process_Own_Signal(curEvent);
		if (listener_bitmask[PB_SUBSYSTEM_USER_INTERFACE].test(curEvent.type))
			ui.Receive_Signal(curEvent);
		if (listener_bitmask[PB_SUBSYSTEM_GAME_ENGINE].test(curEvent.type))
			game_engine.Receive_Signal(curEvent);
		if (listener_bitmask[PB_SUBSYSTEM_SDLRENDER_ENGINE].test(curEvent.type))
			render_engine.Receive_Signal(curEvent);
	}
}

void pb::pbController::Register_Listener_Bitmask(event_flag set, PB_SubSystem system)
{
	listener_bitmask[system] = set;
}


void pbController::Initalize_Everything()
{
	render_engine.Initialize_Render_Core(&game_engine, &ui, this);
	parser.Run_Parser();
	game_engine.Initialize_Game_Engine(&parser);
	//HACK: should generate using user actions/save load/ new game
	game_engine.Generate_Gallery_Map();
	ui.Initialize_UI(&parser);
	menu_base = render_engine.Get_Menu_Base();
	menu_base->Initialize(&ui, this);
	ui.Attach_MenuBase(menu_base);
	menu_base->Test();
	pbsdl::Initialize_Event_Handler(this, menu_base);
}

void pbController::Initialize_Headless()
{
	parser.Run_Parser();
	game_engine.Initialize_Game_Engine(&parser);
	ui.Initialize_UI(&parser);
	headless = true;
}

void pbController::Initialize_SDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		throw std::runtime_error(SDL_GetError());
	}
}

void pbController::Initalize_Controller()
{

}

void pbController::Game_Loop()
{
	while (!pbController::Quit_Game())
	{
		if (!headless)
			pbsdl::Gather_Input();
		Process_Signals();
		ui.Update();
		if(game_running)
			game_engine.Game_Update();
		if(!headless)
			render_engine.Render();
		Print_FPS();
	}
}

void pbController::Print_FPS()
{
	static std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	static std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
	static int i = 0;
	++i;

	end = std::chrono::high_resolution_clock::now();
	if (std::chrono::duration_cast<std::chrono::seconds>(end - start) > std::chrono::duration<int>(1))
	{
		std::cout << "FPS:" << i << '\n';
		i = 0;
		start = end;
	}
}

GameEngineCore* pb::pbController::Get_Game_Engine()
{
	return &game_engine;
}
