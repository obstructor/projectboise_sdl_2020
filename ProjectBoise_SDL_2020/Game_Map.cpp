#include "GameEngineCore.h"
#include "Game_Map.h"
#include "common.h"
#include <queue>

using namespace pb;
FluidBody EntityHolder::vacuum = FluidBody();


bool Game_Map::Fill_Vacuum(Coordinate loc, Fluid fluid)
{
	if (!Bound_Check(loc) || !At(loc).isVacuum())
		return false; // Either out of bounds or isn't a vacuum so failed to fill vacuum

	FluidBody& result = CreateFluidBody();
	result.Add_Fluid(fluid);

	std::queue<Coordinate> breadth_coord_queue;
	breadth_coord_queue.push(loc);
	At(loc).parent_eid = result.self_eid;

	while (!breadth_coord_queue.empty())
	{
		Coordinate cur = breadth_coord_queue.front();
		breadth_coord_queue.pop();

		for (auto cur_neighbor = cur.Get_Neighbors(); !cur_neighbor.isLast(); ++cur_neighbor)
		{
			if (Bound_Check(cur_neighbor) && At(cur_neighbor).isVacuum())
			{
				breadth_coord_queue.push(cur_neighbor);
				At(cur_neighbor).parent_eid = result.self_eid;
			}
		}
	}
	return true;
}

std::set<EID> Game_Map::Find_Adjoining_Fluids(Coordinate starting_location)
{
	std::queue<Coordinate> breadth_queue;
	breadth_queue.push(starting_location);
	std::set<EID> adjoining_fluids;
	std::set<Coordinate> visited;

	if (!At(starting_location).isFluid())
		return adjoining_fluids;
	EID starting_fluid_eid = At(starting_location).parent_eid;

	while (breadth_queue.size() > 0)
	{
		Coordinate current_loc = breadth_queue.front();
		breadth_queue.pop();

		auto success = visited.insert(current_loc);

		if (success.second && Bound_Check(current_loc))	//Coordinate has not been visited and is in bounds		
		{
			if (At(current_loc).parent_eid == starting_fluid_eid) //Keep searching if we are still in the starting fluid
			{
				for (auto neighbor : current_loc.Get_Neighbors())
					breadth_queue.push(neighbor);
			}
			else if (At(current_loc).isFluid())										//Coordinate is a fluid
			{
				adjoining_fluids.insert(At(current_loc).parent_eid);
			}
		}
	}
	return adjoining_fluids;
}

Prototype* Game_Map::Get_Prototype(EID id)
{
	if (parent)
	{
		return parent->Get_Prototype(id);
	}
	else {
		throw std::runtime_error("Game Map cannot get Prototype as parent is nullptr");
	}
}

EntityHolder* pb::Game_Map::Get_Entities()
{
	return &entities;
}

void Game_Map::Place_Blocks(Rectangle rect, EID eid)
{
	if (Bound_Check(rect.Top_Left()) && Bound_Check(rect.Bot_Right()))
	{
		auto tl = rect.Top_Left();
		for (int y = tl.y; y <= rect.Bot_Right().y; ++y)
		{
			for (int x = tl.x; x <= rect.Bot_Right().x; ++x)
			{
				operator[](Coordinate(x, y)) = Map_Block((BlockPrototype*)Get_Prototype(eid));
			}
		}
	}
}

void Game_Map::Remove_Blocks(Rectangle rect)
{
	if (Bound_Check(rect.Top_Left()) && Bound_Check(rect.Bot_Right()))
	{
		auto tl = rect.Top_Left();
		for (int y = tl.y; y <= rect.Bot_Right().y; ++y)
		{
			for (int x = tl.x; x <= rect.Bot_Right().x; ++x)
			{
				operator[](Coordinate(x, y)) = Map_Block();
			}
		}
	}
	auto vec = Find_Adjoining_Fluids(rect.Top_Left());
	if (vec.size() > 0)
	{
		Merge_Fluids(vec, rect.Top_Left());
		entities.Merge_Fluids(vec, *vec.begin());
	}
}

unsigned int Game_Map::size()
{
	return internal_map_storage.size();
}

void Game_Map::resize(const Coordinate& extents)
{
	internal_map_storage.clear();
	map_chunk_xwidth = extents.x / CHUNK_SIZE + 1;
	map_chunk_ywidth = extents.y / CHUNK_SIZE + 1;
	internal_map_storage.resize(map_chunk_xwidth * map_chunk_ywidth);
}


bool Game_Map::Bound_Check(const Coordinate& coord) const
{
	if (Coord_to_Chunk(coord) >= 0 && Coord_to_Chunk(coord) < internal_map_storage.size())
	{
		if (Coord_to_Block(coord) < CHUNK_SIZE * CHUNK_SIZE && Coord_to_Block(coord) >= 0)
		{
			if (coord.x >= 0 && coord.y >= 0)
				return true;
		}
	}
	return false;
}

int Game_Map::Coord_to_Chunk(const Coordinate& coord) const
{
	return (coord.x / CHUNK_SIZE) * map_chunk_ywidth + (coord.y / CHUNK_SIZE);
}


int Game_Map::Coord_to_Block(const Coordinate& coord) const
{
	return (coord.x % CHUNK_SIZE) * CHUNK_SIZE + (coord.y % CHUNK_SIZE);
}

Coordinate Game_Map::Get_Ground(Coordinate position)
{
	Coordinate ground = position;
	while (Bound_Check(ground) && At(ground).isFluid())
	{
		ground.y += 1;
	}
	ground.y -= 1; //Maybe not true, but maybe is true.  Is the ground the block or the space above the block.
	return ground;
}

std::tuple<Coordinate, Coordinate> Game_Map::Get_Floor_Extents(Coordinate location)
{
	// Check left
	Coordinate leftExtent = location;
	Coordinate testExtent = leftExtent;
	while (Bound_Check(testExtent) && At(testExtent).get_type() == ENTITY_FLUID_BLOCK)
	{
		leftExtent = testExtent;
		testExtent.x -= 1;
	}
	// Check right
	Coordinate rightExtent = location;
	testExtent = rightExtent;
	while (Bound_Check(testExtent) && At(testExtent).get_type() == ENTITY_FLUID_BLOCK)
	{
		rightExtent = testExtent;
		testExtent.x += 1;
	}

	return std::tuple<Coordinate, Coordinate>(leftExtent, rightExtent);
}

bool Game_Map::CreateFluid(const FluidPrototype* fluid_proto, int quantity, Coordinate loc)
{
	Map_Block dest_block = At(loc);
	Fluid constituent_fluid = Fluid(fluid_proto);
	constituent_fluid.quantity = quantity;

	if (dest_block.isVacuum())
	{
		Fill_Vacuum(loc, constituent_fluid);
		return true;
	}
	else if (dest_block.isFluid())
	{
		EID fluid_body = dest_block.parent_eid;
		entities.fluid_bodies.at(fluid_body.id).Add_Fluid(constituent_fluid);
		return true;
	}
	return false;
}

void Game_Map::Delete_FluidBody(Coordinate loc)
{
	if (!At(loc).isFluid() || At(loc).isVacuum())
		return;
	auto fluid = At(loc).parent_eid;
	std::queue<Coordinate> breadth_queue;
	breadth_queue.push(loc);
	std::set<Coordinate> visited;

	//Remove from map
	while (breadth_queue.size() > 0)
	{
		Coordinate current_loc = breadth_queue.front();
		breadth_queue.pop();

		auto success = visited.insert(current_loc);
		if (success.second && Bound_Check(current_loc))	//Coordinate has not been visited and is in bounds		
		{
			Map_Block& block = At(current_loc);
			if (block.parent_eid == fluid){
				for (auto neighbor : current_loc.Get_Neighbors())
					breadth_queue.push(neighbor);
				block.parent_eid = EID();
			}
		}
	}
	//Remove from entities
	entities.fluid_bodies.at(fluid.id).clear();
}

FluidBody& Game_Map::CreateFluidBody()
{
	FluidBody& result = entities.fluid_bodies.emplace_back();
	result.self_eid = EID(entities.fluid_bodies.size() - 1, ENTITY_FLUID_BODY);
	return result;
}

bool Game_Map::CreateBlock(const BlockPrototype* block_proto, Coordinate loc)
{
	if (Bound_Check(loc))
	{
		DeleteBlock(loc);
		At(loc) = Map_Block(block_proto);
		return true;
	}
	return false;
}

bool Game_Map::DeleteBlock(Coordinate loc)
{
	if (Bound_Check(loc))
	{
		Map_Block& block = At(loc);
		if (block.isFluid()) {
			DeleteEntity(block.fluid_internals.placed_entity);
		}
		block = Map_Block();
		return true;
	}
	return false;
}

bool Game_Map::RemoveBlock(Coordinate loc)
{
	throw std::runtime_error("Remove block not supported, functionality to merge fluids after removal is not implemented");
	DeleteBlock(loc);
	return false;
}

bool Game_Map::CreateEntity(const Prototype* proto, int quantity, Coordinate loc)
{
	if (Bound_Check(loc) && At(loc).isFluid() && !At(loc).fluid_internals.placed_entity)
	{
		switch (proto->self_eid.type)
		{
		case PROTOTYPE_PLANT:
			PlantPrototype* plant_proto = (PlantPrototype*) proto;
			RecipePrototype* recipe = (RecipePrototype*)Get_Prototype(plant_proto->recipe);
			entities.plants.push_back(Plant((PlantPrototype*)proto, loc, recipe));
			At(loc).fluid_internals.placed_entity = EID(entities.plants.size() - 1, ENTITY_PLANT);
			return true;
		}
	}
	return false;
}

bool Game_Map::DeleteEntity(EID entity)
{
	switch (entity.type)
	{
		case ENTITY_TYPE_NULL:
		case ENTITY_FLUID:
		case ENTITY_BLOCK:
		case ENTITY_ITEM:
		case ENTITY_PARTICLE:
		case ENTITY_MAPGEN:
		case ENTITY_FLUID_BLOCK:
		default:
			return false;
		case ENTITY_CONSTRUCTOR:
		case ENTITY_FURNITURE:
		case ENTITY_CONSTRUCTION:
		case ENTITY_PAWN:
		case ENTITY_PLANT:
		case ENTITY_FLUID_BODY:
			return true;
	}
	return false;
}

EntityBase* pb::Game_Map::GetEntity(EID id)
{
	switch (id.type)
	{
	case ENTITY_FLUID_BODY:
		return (EntityBase*)(&entities.fluid_bodies.at(id.id));
		break;
	case ENTITY_PLANT:
		return (EntityBase*)(&entities.plants.at(id.id));
		break;
	}
	return nullptr;
}

int Game_Map::MaxX()
{
	return map_chunk_xwidth * CHUNK_SIZE;
}

int Game_Map::MaxY()
{
	return map_chunk_ywidth * CHUNK_SIZE;
}
void Game_Map::Update_Entities()
{
	for (auto& plant : entities.plants)
	{
		plant.Update(&entities);
	}
}

void Game_Map::Generate(const std::vector<BlockPrototype>& blocks, const std::vector<FluidPrototype>& fluids, const std::vector<PlantPrototype>& plants)
{
	Coordinate size;
	size.x = CHUNK_SIZE - 1;
	size.y = CHUNK_SIZE - 1;
	resize(size); // Fills with vacuum
	int max_block_id = blocks.size();
	int cur_block_id = 0;
	
	for (int x = 0; x < MaxX(); ++x)
	{
		//Lower half of map is made of equal distribution of each block type
		for (int y = MaxY() / 2; y < MaxY(); ++y)
		{
			CreateBlock(&blocks[cur_block_id++], Coordinate(x, y));
			if (cur_block_id >= max_block_id)
				cur_block_id = 0;
		}
	}

	for (auto fluid : fluids)
	{
		// Dump each fluid into atmosphere, which corresponds to upper half of map
		CreateFluid(&fluid, 100000, Coordinate(0, 0));
	}

	for (int i = 0; i < plants.size() && i < MaxX(); ++i)
	{
		CreateEntity((Prototype*)&plants[i], 1, Coordinate(i, MaxY() / 2 - 1));
		//(*this)[Coordinate(i, MaxY() / 2 - 1)].fluid_internals.placed_entity = EID(i, ENTITY_PLANT); // Really hacky, this needs to abstracted to a concept like "Create Entity at location"
	}
}

pb::Game_Map::Game_Map(GameEngineCore* in_parent, bj::value val, std::map<EID, EID> eid_map) : Game_Map(in_parent)
{
	Load_Game_Map_JSON(val, eid_map);
}

Game_Map::Game_Map(GameEngineCore* in_parent) : entities(this)
{
	parent = in_parent;
}

Game_Map::Game_Map() : entities(this)
{
	parent = nullptr;
}

Map_Block& Game_Map::operator[](const Coordinate& coord)
{
	if (Bound_Check(coord))
	{
		return internal_map_storage[Coord_to_Chunk(coord)].chunk[Coord_to_Block(coord)];
	}
	else {
		throw std::out_of_range(coord.toString());
	}
}

Map_Block& Game_Map::At(const Coordinate& coord)
{
	if (Bound_Check(coord))
	{
		return internal_map_storage[Coord_to_Chunk(coord)].chunk[Coord_to_Block(coord)];
	}
	else {
		throw std::out_of_range(coord.toString());
	}
}

Map_Block pb::Game_Map::Get_Map_Block(const Coordinate& coord) const
{
	if (Bound_Check(coord))
	{
		return internal_map_storage[Coord_to_Chunk(coord)].chunk[Coord_to_Block(coord)];
	}
	else {
		throw std::out_of_range(coord.toString());
	}
}

EntityHolder::EntityHolder(Game_Map* in_parent)
{
	parent = in_parent;
}

FluidBody* EntityHolder::Get_Fluid(Coordinate loc)
{
	if (parent->At(loc).isFluid())
	{
		EID fluid_id = parent->At(loc).parent_eid;
		return fluid_id == vacuum.self_eid ? &vacuum : &fluid_bodies.at(fluid_id.id);
	}
	else {
		throw std::runtime_error("EntityHolder Failed to get Fluid");
	}
	return nullptr;
}

void Game_Map::Merge_Fluids(std::set<EID> fluids, Coordinate starting_location)
{
	if (fluids.size() < 1 || !At(starting_location).isFluid())
		return;

	//Merge vacuum always
	EID self_eid = *fluids.begin();

	std::queue<Coordinate> breadth_queue;
	breadth_queue.push(starting_location);
	std::set<Coordinate> visited;

	while (breadth_queue.size() > 0)
	{
		Coordinate current_loc = breadth_queue.front();
		breadth_queue.pop();

		auto success = visited.insert(current_loc);

		if (success.second && Bound_Check(current_loc))	//Coordinate has not been visited and is in bounds		
		{
			Map_Block& block = At(current_loc);
			if (fluids.find(block.parent_eid) != fluids.end() || //Keep searching if we are still in the starting fluid
				block.isVacuum()) //Always merge vacuum
			{
				for (auto neighbor : current_loc.Get_Neighbors())
					breadth_queue.push(neighbor);
				block.parent_eid = self_eid;
			}
		}
	}
	entities.Merge_Fluids(fluids, self_eid);
}

void EntityHolder::Merge_Fluids(const std::set<EID>& fluids, EID merged_id)
{
	if (!merged_id || fluids.size() < 1)
		return;
	FluidBody merged_body;
	for(auto fluid : fluids)
	{
		if (fluid.type == ENTITY_FLUID_BODY)
		{
			merged_body = merged_body + fluid_bodies.at(fluid.id);
		}
	}

	merged_body.self_eid = merged_id;

	if(fluid_bodies.size() > merged_id.id)
		fluid_bodies.at(merged_id.id) = merged_body;

}

Prototype* pb::EntityHolder::Get_Prototype(EID e)
{
	return parent->Get_Prototype(e);
}

bj::object EntityHolder::toJSON()
{
	bj::object obj;
	bj::array json_plants;
	for (auto& plant : plants)
	{
		json_plants.push_back(plant.toJSON());
	}
	obj.insert_or_assign("plants", json_plants);

	bj::array json_constructors;
	for (auto& constructor : constructors)
	{
		json_constructors.push_back(constructor.toJSON());
	}
	obj.insert_or_assign("constructors", json_constructors);

	bj::array json_fluidbodies;
	for (auto& body : fluid_bodies)
	{
		json_fluidbodies.push_back(body.toJSON());
	}
	obj.insert_or_assign("fluid_bodies", json_fluidbodies);

	bj::array json_items;
	for (auto& item : items)
	{
		json_items.push_back(item.toJSON());
	}
	obj.insert_or_assign("items", json_items);
	return obj;
}

void EntityHolder::Load_Entities_JSON(bj::value val, std::map<EID, EID> eid_map)
{
	plants.clear();
	constructors.clear();
	fluid_bodies.clear();
	items.clear();
	for (auto& plant : val.at("plants").as_array())
		plants.push_back(Plant(plant, eid_map, this));

	for (auto& con : val.at("constructors").as_array())
		constructors.push_back(Constructor(con, eid_map, this));

	for (auto& body : val.at("fluid_bodies").as_array())
		fluid_bodies.push_back(FluidBody(body, eid_map));

	for (auto& item : val.at("items").as_array())
		items.push_back(Item(item, eid_map));
}

bj::object Game_Map::toJSON()
{
	bj::object obj;
	obj.insert_or_assign("entities", entities.toJSON());
	obj.insert_or_assign("MaxX", MaxX());
	obj.insert_or_assign("MaxY", MaxY());
	obj.insert_or_assign("chunk_x_width", map_chunk_xwidth);
	obj.insert_or_assign("chunk_y_width", map_chunk_ywidth);
	bj::array map_data;
	for (Coordinate loc(0, 0); loc.y < MaxY(); ++loc.y)
	{
		for (loc.x = 0; loc.x < MaxX(); ++loc.x)
		{
			map_data.push_back(Get_Map_Block(loc).toJSON());
		}
	}
	obj.insert_or_assign("map_data", map_data);
	return obj;
}

void Game_Map::Load_Game_Map_JSON(bj::value val, std::map<EID, EID> eid_map)
{
	entities.Load_Entities_JSON(val.at("entities"), eid_map);
	resize(Coordinate(val.at("MaxX").as_int64() - 1, val.at("MaxY").as_int64() - 1));
	bj::array map_data = val.at("map_data").as_array();
	int i = 0;
	for (Coordinate loc(0, 0); loc.y < MaxY(); ++loc.y)
	{
		for (loc.x = 0; loc.x < MaxX(); ++loc.x)
		{
			At(loc).Load_Block_FromJSON(map_data.at(i++), eid_map);
		}
	}
}
