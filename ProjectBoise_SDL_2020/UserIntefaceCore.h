#pragma once
#include "MenuBase.h"
#include "common.h"
#include "CircularBuffer.h"

namespace pb
{
	class MenuBase;
	class Parser;
	class pbController;

	class UserInterfaceCore
	{
	private:
		pbController* parent;
		CircularBuffer<pbEvent> event_queue;
		MenuBase* menu_base;
		std::map<EID, textInformation> proto_textinfo;
		pbEvent designation;
		WorldRectangle designation_rect;
		std::vector<WorldRectangle> viewports;

		pbEvent Designation_From_EID(EID id);
		void Next_Designation();
		void Process_Command(pbEvent eve);
	public:
		UserInterfaceCore(pbController* parent);
		std::pair<pbEvent, Rectangle> Get_Designation();
		textInformation Get_EID_Text(EID id);
		void Initialize_UI(Parser*);
		void Attach_MenuBase(MenuBase*);
		void Receive_Signal(pbEvent);
		void Update();

		void Save_UI(std::filesystem::path p);
		std::map<EID, EID> Load_UI(std::filesystem::path p);
		bj::object Save_UI_JSON();
		std::map<EID, EID> Load_UI_JSON(bj::value val);
	};
}
