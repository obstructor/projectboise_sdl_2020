#include "common.h"

const std::map<pb::pbEventType, std::string> pb::enum_text_map = {
	//PYTHON START HERE ENUM_TO_TEXT_MAP
		{PB_EVENT_CLOSE_MENU,                 "CLOSE_MENU"},
		{PB_EVENT_SWITCH_MENU,                "SWITCH_MENU"},
		{PB_EVENT_OPEN_MENU,                  "OPEN_MENU"},
		{PB_EVENT_START_GAME,                 "START_GAME"},
		{PB_EVENT_CLOSE_GAME,                 "CLOSE_GAME"},
		{PB_EVENT_SAVE_GAME,                  "SAVE_GAME"},
		{PB_EVENT_LOAD_GAME,                  "LOAD_GAME"},
		{PB_EVENT_PAUSE_GAME,                 "PAUSE_GAME"},
		{PB_EVENT_CHANGE_GAME_SPEED,          "CHANGE_GAME_SPEED"},
		{PB_EVENT_SET_CURSOR_DESIGNATION,     "SET_CURSOR_DESIGNATION"},
		{PB_EVENT_NEXT_CURSOR_DESIGNATION,    "NEXT_CURSOR_DESIGNATION"},
		{PB_EVENT_START_DESIGNATION,          "START_DESIGNATION"},
		{PB_EVENT_FINISH_DESIGNATION,         "FINISH_DESIGNATION"},
		{PB_EVENT_CANCEL_DESIGNATION,         "CANCEL_DESIGNATION"},
		{PB_EVENT_CHANGE_SETTING_GLOBAL,      "CHANGE_SETTING_GLOBAL"},
		{PB_EVENT_CHANGE_SETTING_PAWN,        "CHANGE_SETTING_PAWN"},
		{PB_EVENT_SPAWN_ITEM,                 "SPAWN_ITEM"},
		{PB_EVENT_DELETE_ITEM,                "DELETE_ITEM"},
		{PB_EVENT_DESTROY_BLOCK,              "DESTROY_BLOCK"},
		{PB_EVENT_PLACE_BLOCK,                "PLACE_BLOCK"},
		{PB_EVENT_SPAWN_GAS,                  "SPAWN_GAS"},
		{PB_EVENT_DELETE_GAS,                 "DELETE_GAS"},
		{PB_EVENT_SPAWN_PAWN,                 "SPAWN_PAWN"},
		{PB_EVENT_CHANGE_RECIPE,              "CHANGE_RECIPE"},
		{PB_EVENT_CURSOR_DESIGNATION_CHANGED, "CURSOR_DESIGNATION_CHANGED"},
		{PB_EVENT_MOVE_VIEWPORT,              "MOVE_VIEWPORT"},
		{PB_EVENT_MOD_EVENT,                  "MOD_EVENT"},
	//PYTHON STOP HERE ENUM_TO_TEXT_MAP
		{PB_EVENT_NULL, "PB_EVENT_NULL"}
};
using namespace pb;
namespace pb
{
	bj::value read_json(std::istream& is)
	{
		bj::stream_parser p;
		bj::error_code ec;
		std::string line;
		while (std::getline(is, line))
		{
			p.write(line, ec);
			if (ec)
				throw std::runtime_error(std::string("Failed to parse json: ") + ec.message());
		}
		p.finish(ec);
		if (ec)
			throw std::runtime_error(std::string("Failed to parse json: ") + ec.message());
		return p.release();
	}
}

bj::object pbEvent::toJSON() const
{
	bj::object obj;
	obj.insert_or_assign("type", (int)type);
	obj.insert_or_assign("variant_index", payload.index());
	bj::value destination;
	std::visit([&destination](const auto& arg) { destination = arg.toJSON(); }, payload);
	obj.insert_or_assign("payload", destination);
	return obj;
}

void pbEvent::fromJSON(bj::value val)
{
	type = (pbEventType)val.as_object().at("type").as_int64();
	size_t index = val.as_object().at("variant_index").as_int64();
	payload = variant_from_index<PayloadType>(index);
	std::visit([&val](auto& arg) { arg.fromJSON(val.as_object().at("payload")); }, payload);
}

void pbEvent::convertEIDs(const std::map<EID, EID>& eid_map)
{
	std::visit([&eid_map](auto& arg) { arg.convertEIDs(eid_map); }, payload);
}

bj::object pbDesignation::toJSON() const
{
	bj::object obj;
	obj.insert_or_assign("wrect", wrect.toJSON());
	obj.insert_or_assign("quantity", quantity);
	obj.insert_or_assign("eid", eid.toJSON());
	return obj;
}

void pbDesignation::fromJSON(bj::value val)
{
	bj::object& obj = val.as_object();
	wrect.fromJSON(obj.at("wrect"));
	quantity = obj.at("quantity").as_int64();
	eid.fromJSON(obj.at("eid"));
}

void pbDesignation::convertEIDs(const std::map<EID, EID>& eid_map)
{
	if (eid.type < PROTOTYPE_END_ENUM)
		eid = eid_map.at(eid);
}
