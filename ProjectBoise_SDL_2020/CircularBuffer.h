#pragma once
#include <boost/circular_buffer.hpp>
#include <boost/json.hpp>

namespace pb
{
	struct EID;
	template<class T>
	class CircularBuffer
	{
	private:
		boost::circular_buffer<T> internal_buffer;
	public:
		auto size() const;

		void push_back(const T & in);
		void push_front(const T& in);
		T pop_front();
		T pop_back();
		auto begin() { return internal_buffer.begin(); };
		auto end() { return	internal_buffer.end(); };

		boost::json::array Save_EventQueue_JSON();
		void Load_EventQueue_JSON(boost::json::value val);

		CircularBuffer<T>() { internal_buffer.set_capacity(10); };
	};

	template<class T>
	inline auto CircularBuffer<T>::size() const
	{
		return internal_buffer.size();
	}

	template<class T>
	inline void CircularBuffer<T>::push_back(const T& in)
	{
		if (internal_buffer.full())
		{
			internal_buffer.set_capacity((internal_buffer.size() + 1) * 2);
		}
		internal_buffer.push_back(in);
	}
	template<class T>
	inline void CircularBuffer<T>::push_front(const T& in)
	{
		if (internal_buffer.full())
		{
			internal_buffer.rset_capacity((internal_buffer.size() + 1) * 2);
		}
		internal_buffer.push_front(in);
	}
	template<class T>
	inline T CircularBuffer<T>::pop_front()
	{
		T v_front = internal_buffer.front();
		internal_buffer.pop_front();
		return v_front;
	}
	template<class T>
	inline T CircularBuffer<T>::pop_back()
	{
		T v_back = internal_buffer.back();
		internal_buffer.pop_back();
		return v_back;
	}

	template<class T>
	inline boost::json::array CircularBuffer<T>::Save_EventQueue_JSON()
	{
		boost::json::array arr;
		for (const auto& it : internal_buffer)
		{
			arr.push_back(it.toJSON());
		}
		return arr;
	}

	template<class T>
	inline void CircularBuffer<T>::Load_EventQueue_JSON(boost::json::value val)
	{
		internal_buffer.clear();
		for (const auto& it : val.as_array())
		{
			T pb;
			pb.fromJSON(it);
			internal_buffer.push_back(pb);
		}
	}
}