#pragma once
#include "Parser.h"
#include "GameEngineCore.h"
#include "UserIntefaceCore.h"
#include "RenderCore.h"
#include <chrono>
#include <bitset>

namespace pb 
{

	enum PB_SubSystem
	{
		PB_SUBSYSTEM_CONTROLLER,
		PB_SUBSYSTEM_USER_INTERFACE,
		PB_SUBSYSTEM_GAME_ENGINE,
		PB_SUBSYSTEM_AUDIO_ENGINE,
		PB_SUBSYSTEM_SDLRENDER_ENGINE,
		PB_SUBSYSTEM_SDLGUI_MENU,
		PB_SUBSYSTEM_MOD_ENGINE,

		PB_SUBSYSTEM_ENUM_END
	};

	typedef std::bitset<PB_EVENT_ENUM_END> event_flag;

	class pbController
	{
	private:
		GameEngineCore game_engine;
		UserInterfaceCore ui;
		RenderCore render_engine;
		MenuBase* menu_base = nullptr;
		Parser parser;
		event_flag listener_bitmask[PB_SUBSYSTEM_ENUM_END];

		CircularBuffer<pbEvent> event_queue;

		bool quit = false;
		bool game_running = false;
		bool headless = false;

		void Process_Own_Signal(pbEvent eve);
		void Save_Controller(std::filesystem::path p);
		bj::object Save_Controller_JSON();
		void Load_Controller_JSON(bj::value val, const std::map<EID, EID>&);

	public:
		pbController();
		~pbController();
		bool Quit_Game();
		void Receive_Input();
		void Receive_Input(SDL_Event &sdl_event);
		void Receive_Signal(pbEvent eve);
		void Process_Signals();

		void Register_Listener_Bitmask(event_flag set, PB_SubSystem system);

		void Initalize_Everything();
		void Initialize_Headless();
		void Initialize_SDL();
		void Initalize_Controller();
		void Game_Loop();
		void Print_FPS();

		void Save_Game(std::string save_name=std::string("Save1"));
		void Load_Game(std::string save_name = std::string("Save1"));

		GameEngineCore* Get_Game_Engine();
	};
}