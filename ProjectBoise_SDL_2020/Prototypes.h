#pragma once
#include "common.h"

namespace pb
{
	struct Prototype
	{
		EID self_eid;
		EID get_EID() { return self_eid; };
	};

	//PYTHON START HERE READ_PROTOTYPES


	struct FluidPrototype : Prototype
	{

	};

	struct RecipePrototype : Prototype
	{
		EID ingredient_id[RECIPE_BUFFER_SIZE];
		int ingredient_quantity[RECIPE_BUFFER_SIZE];
		EID output_id[RECIPE_BUFFER_SIZE];
		int output_quantity[RECIPE_BUFFER_SIZE];
		int energy_cost;
	};

	struct MapgenPrototype : Prototype
	{
		int gen_height_min;
		int gen_height_max;
	};

	struct ConstructorPrototype : Prototype
	{
		int stat_example;
	};

	struct PawnPrototype : Prototype
	{
		int fall_speed;
		int walk_speed;
	};
	struct BlockPrototype : Prototype
	{
	};
	struct ItemPrototype : Prototype
	{
	};
	struct ParticlePrototype : Prototype
	{
	};
	struct FurniturePrototype : Prototype
	{
	};
	struct ConstructionPrototype : Prototype
	{
	};
	struct PlantPrototype : ConstructorPrototype
	{
		EID recipe;
	};
	//PYTHON STOP HERE READ_PROTOTYPES

}
