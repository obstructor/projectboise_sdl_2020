#include "Parser.h"
#include "common.h"
#include <iostream>
#include <filesystem>

#include <sstream>
#include <boost/algorithm/string.hpp>
#include "RenderCore.h"

using namespace pb;
namespace fs = std::filesystem;

using std::map, std::vector, std::string, std::ifstream, std::stringstream;

// Initialize map to PrototypeType from parsed string
const map<string, EntityType> Parser::string_to_enumtype = {
			//PYTHON START HERE STRING_TO_TYPE_MAP
			{"FLUID", PROTOTYPE_FLUID},
			{"BLOCK", PROTOTYPE_BLOCK},
			{"ITEM", PROTOTYPE_ITEM},
			{"PARTICLE", PROTOTYPE_PARTICLE},
			{"CONSTRUCTOR", PROTOTYPE_CONSTRUCTOR},
			{"FURNITURE", PROTOTYPE_FURNITURE},
			{"CONSTRUCTION", PROTOTYPE_CONSTRUCTION},
			{"RECIPE", PROTOTYPE_RECIPE},
			{"MAPGEN", PROTOTYPE_MAPGEN},
			{"PAWN", PROTOTYPE_PAWN},
			{"PLANT", PROTOTYPE_PLANT}
		   //PYTHON STOP HERE STRING_TO_TYPE_MAP
};

void Parser::Parse_File(string file_path)
{
	ifstream csv_file(file_path);
	string line;
	AllPrototypeUnion prototype;
	bool read_file = false;

	while (csv_file.good() && csv_file.is_open() && !csv_file.eof() && getline(csv_file,line))
	{
		read_file = true;
		prototype = Parse_String(line);
		Insert_Prototype(prototype);
	}
}

AllPrototypeUnion Parser::Parse_String(string line)
{
	vector<string> row;
	boost::split(row, line, boost::is_any_of("|"));

	if (row.size() < 2 || row[0].length() < 2 || String_Contains(row[0], "HEADER") || String_Contains(row[0], "#"))
	{
		return AllPrototypeUnion();
	}
	else
	{
		boost::to_upper(row[0]);
		auto it = string_to_enumtype.find(row[0]);
		if (it == string_to_enumtype.end())
			throw std::invalid_argument(string("PrototypeType not valid") + row[0]);

		return Parse_Row(row, it->second);
	}
}

void Parser::Convert_ParserPrototypes_to_Prototypes()
{
	for (auto& proto : prototypes)
	{
		proto.second.Finalize(this);
	}
}

std::map<EID, textInformation> Parser::get_all_proto_text()
{
	std::map<EID, textInformation> text_data;
	for (auto& pair : prototypes)
	{
		text_data.insert(std::pair<EID,textInformation>(pair.second.base.get_EID(), pair.second.text));
	}
	return text_data;
}

std::vector<RenderData> pb::Parser::get_render_data()
{
	std::vector<RenderData> result(prototypes.size());

	int proto_offset[PROTOTYPE_END_ENUM];
	int proto_index[PROTOTYPE_END_ENUM];

	int prior = 0;
	for (int i = 0; i < PROTOTYPE_END_ENUM; ++i)
	{
		proto_offset[i] = prior;
		proto_index[i] = prior;
		prior += prototype_id[i];
	}

	for (auto& proto : prototypes)
	{
		RenderData data;
		unsigned int raw_color = String_To_unsigned_int(proto.second.text.sprite_file);
		data.color.r = raw_color & 0xFF000000;
		data.color.g = raw_color & 0x00FF0000;
		data.color.b = raw_color & 0x0000FF00;
		data.color.a = raw_color & 0x000000FF;

		int offset = proto_offset[proto.second.type] + proto.second.proto_id;
		result[offset] = data;
	}
	return result;
}

Parser::Parser()
{
	Clear_Parser();
}

void Parser::Clear_Parser()
{
	for (int i = 0; i < PROTOTYPE_END_ENUM; ++i)
	{
		prototype_id[i] = 0;
		prototypes.clear();
	}
}

void Parser::Insert_Prototype(AllPrototypeUnion& prototype)
{
	if (prototype)
	{
		prototype.proto_id = prototype_id[prototype.type]++;
		auto [it, success] = prototypes.insert({ prototype.text.name, prototype });
		if (!success) {
			throw std::logic_error(string("Duplicated prototype name:" + prototype.text.name));
		}
	}
}



//PYTHON START HERE PARSE_DEFINITION
AllPrototypeUnion& Parser::Parse_Fluid(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;

	return a;
};

AllPrototypeUnion& Parser::Parse_Block(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;

	return a;
};

AllPrototypeUnion& Parser::Parse_Item(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;

	return a;
};

AllPrototypeUnion& Parser::Parse_Particle(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;

	return a;
};

AllPrototypeUnion& Parser::Parse_Constructor(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;
	a.constructor.stat_example = String_To_int(row[i++]);

	return a;
};

AllPrototypeUnion& Parser::Parse_Furniture(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;

	return a;
};

AllPrototypeUnion& Parser::Parse_Construction(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;

	return a;
};

AllPrototypeUnion& Parser::Parse_Recipe(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;
	for(int iter = 0; iter < RECIPE_BUFFER_SIZE;++iter)
		a.recipe.string_ingredient_id[iter] = String_To_EID(row[i++]);

	for(int iter = 0; iter < RECIPE_BUFFER_SIZE;++iter)
		a.recipe.ingredient_quantity[iter] = String_To_int(row[i++]);

	for(int iter = 0; iter < RECIPE_BUFFER_SIZE;++iter)
		a.recipe.string_output_id[iter] = String_To_EID(row[i++]);

	for(int iter = 0; iter < RECIPE_BUFFER_SIZE;++iter)
		a.recipe.output_quantity[iter] = String_To_int(row[i++]);

	a.recipe.energy_cost = String_To_int(row[i++]);

	return a;
};

AllPrototypeUnion& Parser::Parse_Mapgen(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;
	a.mapgen.gen_height_min = String_To_int(row[i++]);
	a.mapgen.gen_height_max = String_To_int(row[i++]);

	return a;
};

AllPrototypeUnion& Parser::Parse_Pawn(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;
	a.pawn.fall_speed = String_To_int(row[i++]);
	a.pawn.walk_speed = String_To_int(row[i++]);

	return a;
};

AllPrototypeUnion& Parser::Parse_Plant(const std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 0;
	a.plant.string_recipe = String_To_EID(row[i++]);

	return a;
};

//PYTHON STOP HERE PARSE_DEFINITION

AllPrototypeUnion& Parser::PopParse_General_Entity_Parts(std::vector<std::string>& row, AllPrototypeUnion& a)
{
	int i = 1;
	a.text.name = row[i++];
	a.text.sprite_file = row[i++];
	a.text.icon = row[i++];

	row.erase(row.begin(), row.begin() + i);
	return a;
}

AllPrototypeUnion Parser::Parse_Row(vector<string>& row, EntityType type)
{
	AllPrototypeUnion any_prototype(type);
	PopParse_General_Entity_Parts(row, any_prototype);
	switch (type)
	{
	//PYTHON START HERE PARSE_ROW_SWITCH
	case PROTOTYPE_FLUID:
		Parse_Fluid(row, any_prototype);
		break;
	case PROTOTYPE_BLOCK:
		Parse_Block(row, any_prototype);
		break;
	case PROTOTYPE_ITEM:
		Parse_Item(row, any_prototype);
		break;
	case PROTOTYPE_PARTICLE:
		Parse_Particle(row, any_prototype);
		break;
	case PROTOTYPE_CONSTRUCTOR:
		Parse_Constructor(row, any_prototype);
		break;
	case PROTOTYPE_FURNITURE:
		Parse_Furniture(row, any_prototype);
		break;
	case PROTOTYPE_CONSTRUCTION:
		Parse_Construction(row, any_prototype);
		break;
	case PROTOTYPE_RECIPE:
		Parse_Recipe(row, any_prototype);
		break;
	case PROTOTYPE_MAPGEN:
		Parse_Mapgen(row, any_prototype);
		break;
	case PROTOTYPE_PAWN:
		Parse_Pawn(row, any_prototype);
		break;
	case PROTOTYPE_PLANT:
		Parse_Plant(row, any_prototype);
		break;
	//PYTHON STOP HERE PARSE_ROW_SWITCH
	default:
		throw std::runtime_error(string("Failed to Parse Row type: ") + std::to_string(type));
	}
	return any_prototype;
}

int Parser::String_To_int(string value)
{
	stringstream converter;
	converter << value;
	int result;
	
	if (value.find("0x") != string::npos)
		converter >> std::hex >> result;
	else
		converter >> std::dec >> result;

	if (converter.fail())
		throw std::logic_error(string("Failed to parse number: ") + value);

	return result;
}

unsigned int Parser::String_To_unsigned_int(string value)
{
	stringstream converter;
	converter << value;
	unsigned int result;

	if (value.find("0x") != string::npos)
		converter >> std::hex >> result;
	else
		converter >> std::dec >> result;

	if (converter.fail() || value.length() > 0 && value[0] == '-')
		throw std::logic_error(string("Failed to parse number: ") + value);

	return result;
}

EID Parser::Lookup_EID(std::string name)
{
	if (name == "")
		return EID();
	auto p = prototypes.at(name);
	return EID(p.proto_id, p.type);
}

void Parser::Run_Parser()
{
	Clear_Parser();
	Parse_File("Resources/entities.csv");
	Convert_ParserPrototypes_to_Prototypes();
	/*
	for (auto& p : fs::recursive_directory_iterator("Mods"))
	{
		if (p.is_directory())
		{
			Logln("Loading Directory: " + p.path().string());
		}
		else
		{
			Parse_file(p.path().string()); // I am sure there is probably some better way to do this but this works just fine.
		}
	}*/
}
