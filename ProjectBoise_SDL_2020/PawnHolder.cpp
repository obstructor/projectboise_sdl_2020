#include "PawnHolder.h"
#include "GameEngineCore.h"
#include "common.h"
using namespace pb;

pb::PawnHolder::PawnHolder(GameEngineCore* inGameEngine)
{
	game_engine = inGameEngine;
}

Pawn* pb::PawnHolder::Get_Pawn_By_Id(unsigned int id)
{ // Really pawns should probably be in a map or have a map index, but this is fine for now
	if (size() > id)
	{
		return &at(id);
	}
	else
	{
		return nullptr;
	}
}

unsigned int pb::PawnHolder::Get_Pawn_Count()
{
	return size();
}

int pb::PawnHolder::Add_Pawn(Coordinate coord)
{
	int id = size();
	Pawn new_pawn;
	new_pawn.Set_Location(coord);
	push_back(new_pawn);
	return id;
}

int pb::PawnHolder::Add_Pawn(Pawn new_pawn)
{
	int id = size();
	push_back(new_pawn);
	return id;
}

void pb::PawnHolder::Update_All_Pawns()
{
	for (int i = 0; i < size(); ++i)
	{
		at(i).Update(game_engine);
	}
}

bj::array PawnHolder::toJSON()
{
	bj::array arr;
	for (Pawn& p : *this)
	{
		arr.push_back(p.toJSON());
	}
	return arr;
}

void PawnHolder::fromJSON(bj::value val)
{
	clear();
	for (auto& pobj : val.as_array())
	{
		push_back(Pawn(pobj));
	}
}