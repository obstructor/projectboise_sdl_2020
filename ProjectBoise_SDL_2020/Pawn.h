#pragma once
#include "common.h"

namespace pb 
{
	class GameEngineCore;
	const int FALL_SPEED = 30;
	const int WALK_SPEED = 100;

	enum PawnState
	{
		PAWN_STATE_NULL,
		PAWN_STATE_FALLING,
		PAWN_STATE_WORKING,
		PAWN_STATE_WALKING,
		PAWN_STATE_IDLE,
		PAWN_STATE_DEAD,
		PAWN_STATE_END
	};
	class Pawn
	{
	private:
		PawnState state;
		WorldCoordinate location;
		WorldCoordinate destination;
		int hunger;
		int tired;
		int start_time;
		int time_to_finish_task;
	public:
		Pawn();
		Pawn(bj::value val);
		void Set_Location(WorldCoordinate);
		WorldCoordinate Get_Location() const;
		FWorldCoordinate Get_Exact_Location() const;

		PawnState Get_State() const;
		int Get_Time_To_Finish_Task() const;

		void Update(GameEngineCore* game_engine);
		void Check_Environment(GameEngineCore* game_engine);
		void Change_State(GameEngineCore* game_engine,PawnState);
		void Move(GameEngineCore* game_engine);
		void Think(GameEngineCore* game_engine);

		bool isAlive() const { return state != PAWN_STATE_DEAD; };

		bj::object toJSON();
		void fromJSON(bj::value val);
	};
}