#include "GameEngineCore.h"
#include "Parser.h"
#include "common.h"
#include "GameController.h"
using namespace pb;

void GameEngineCore::Process_Signals()
{
	Prototype* proto;
	while (event_queue.size() > 0)
	{
		auto eve = event_queue.pop_front();
		switch (eve.type)
		{
			//TODO: Cleanup overly long lines
		case PB_EVENT_PLACE_BLOCK:
			game_maps.at(std::get<pbDesignation>(eve.payload).getWorld()).Place_Blocks(std::get<pbDesignation>(eve.payload).getWrect(), std::get<pbDesignation>(eve.payload).eid);
			break;
		case PB_EVENT_DESTROY_BLOCK:
			game_maps.at(std::get<pbDesignation>(eve.payload).getWorld()).Remove_Blocks(std::get<pbDesignation>(eve.payload).getWrect());
			break;
		case PB_EVENT_SPAWN_GAS:
			proto = Get_Prototype(std::get<pbDesignation>(eve.payload).eid);
			game_maps.at(std::get<pbDesignation>(eve.payload).getWorld()).CreateFluid((FluidPrototype*)proto, std::get<pbDesignation>(eve.payload).getQuantity(), std::get<pbDesignation>(eve.payload).getLoc());
			break;
		case PB_EVENT_DELETE_GAS:
			game_maps.at(std::get<pbDesignation>(eve.payload).getWorld()).Delete_FluidBody(std::get<pbDesignation>(eve.payload).getLoc());
			break;
		case PB_EVENT_CHANGE_RECIPE:
			Change_Recipe(eve);
			break;
		}
	}
}

void GameEngineCore::Change_Recipe(pbEvent eve)
{
	//TODO: not implemented since only plants exist and they don't change recipe
	auto block = game_maps.at(std::get<pbDesignation>(eve.payload).getWorld()).At(std::get<pbDesignation>(eve.payload).getLoc());
	if (block.isFluid() && block.fluid_internals.placed_entity)
	{
		auto placed_eid = block.fluid_internals.placed_entity;
		Plant* plant = (Plant*)Get_Entity(placed_eid);
	}
}

GameEngineCore::GameEngineCore(pbController* in_parent) : pawn_holder(this)
{
	parent = in_parent;
	game_maps.emplace_back(this);
}

void GameEngineCore::Initialize_Game_Engine()
{
	//game_map.Generate();
	pawn_holder.Add_Pawn(Coordinate(0,0));
}

void GameEngineCore::Initialize_Game_Engine(Parser* parser)
{
	event_flag bitmask;

	bitmask.set(PB_EVENT_START_GAME);
	bitmask.set(PB_EVENT_CLOSE_GAME);
	bitmask.set(PB_EVENT_SAVE_GAME);
	bitmask.set(PB_EVENT_LOAD_GAME);
	bitmask.set(PB_EVENT_PAUSE_GAME);

	bitmask.set(PB_EVENT_SPAWN_ITEM);
	bitmask.set(PB_EVENT_DELETE_ITEM);
	bitmask.set(PB_EVENT_DESTROY_BLOCK);
	bitmask.set(PB_EVENT_PLACE_BLOCK);
	bitmask.set(PB_EVENT_SPAWN_GAS);
	bitmask.set(PB_EVENT_DELETE_GAS);
	bitmask.set(PB_EVENT_SPAWN_PAWN);
	bitmask.set(PB_EVENT_CHANGE_RECIPE);

	bitmask.set(PB_EVENT_MOD_EVENT);

	parent->Register_Listener_Bitmask(bitmask, PB_SUBSYSTEM_GAME_ENGINE);

	parser->get_prototypes(block_prototypes, PROTOTYPE_BLOCK);
	parser->get_prototypes(fluid_prototypes, PROTOTYPE_FLUID);
	parser->get_prototypes(pawn_prototypes, PROTOTYPE_PAWN);
	parser->get_prototypes(plant_prototypes, PROTOTYPE_PLANT);
	parser->get_prototypes(recipes, PROTOTYPE_RECIPE);

	pawn_holder.Add_Pawn(Coordinate(0, 0));
}

void GameEngineCore::Sync_PrototypeEID(std::map<EID, EID> proto_map)
{
	BlockPrototype block;
	RecipePrototype recipe;
	PawnPrototype pawn;
	FluidPrototype fluid;
	PlantPrototype plant;
	ItemPrototype item;
	for (auto pro : proto_map)
	{
		if (pro.first != pro.second)
		{
			switch (pro.first.type)
			{
			case PROTOTYPE_BLOCK:
				block = block_prototypes.at(pro.first.id);
				block_prototypes.at(pro.first.id) = block_prototypes.at(pro.second.id);
				block_prototypes.at(pro.second.id) = block;
				break;

			case PROTOTYPE_RECIPE:
				recipe = recipes.at(pro.first.id);
				recipes.at(pro.first.id) = recipes.at(pro.second.id);
				recipes.at(pro.second.id) = recipe;
				break;

			case PROTOTYPE_PAWN:
				pawn = pawn_prototypes.at(pro.first.id);
				pawn_prototypes.at(pro.first.id) = pawn_prototypes.at(pro.second.id);
				pawn_prototypes.at(pro.second.id) = pawn;
				break;

			case PROTOTYPE_FLUID:
				fluid = fluid_prototypes.at(pro.first.id);
				fluid_prototypes.at(pro.first.id) = fluid_prototypes.at(pro.second.id);
				fluid_prototypes.at(pro.second.id) = fluid;
				break;

			case PROTOTYPE_PLANT:
				plant = plant_prototypes.at(pro.first.id);
				plant_prototypes.at(pro.first.id) = plant_prototypes.at(pro.second.id);
				plant_prototypes.at(pro.second.id) = plant;
				break;

			case PROTOTYPE_ITEM:
				item = item_prototypes.at(pro.first.id);
				item_prototypes.at(pro.first.id) = item_prototypes.at(pro.second.id);
				item_prototypes.at(pro.second.id) = item;
				break;
			}
		}
	}

}

bj::object pb::GameEngineCore::Save_Game_Engine_JSON()
{
	bj::object obj;
	obj.insert_or_assign("event_queue", event_queue.Save_EventQueue_JSON());
	obj.insert_or_assign("pawns", pawn_holder.toJSON());
	bj::array maps;
	for (int i = 0; i < game_maps.size(); ++i)
	{
		maps.push_back(game_maps[i].toJSON());
	}
	obj.insert_or_assign("maps", maps);
	return obj;
}

void pb::GameEngineCore::Load_Game_Engine_JSON(bj::value val, std::map<EID, EID> proto_map)
{
	event_queue.Load_EventQueue_JSON(val.at("event_queue"));
	for (auto& eve : event_queue)
		eve.convertEIDs(proto_map);

	pawn_holder.clear();
	for (auto& json_pawn : val.at("pawns").as_array())
	{
		Pawn new_pawn = Pawn();
		new_pawn.fromJSON(json_pawn);
		pawn_holder.Add_Pawn(new_pawn);
	}

	game_maps.clear();
	for (auto& json_map : val.at("maps").as_array())
	{
		game_maps.emplace_back(this, json_map, proto_map);
	}
}

void GameEngineCore::Generate_Gallery_Map()
{
	game_maps[0].Generate(block_prototypes, fluid_prototypes, plant_prototypes);
}

void GameEngineCore::Receive_Signal(pbEvent inSignal)
{
	event_queue.push_back(inSignal);
}

Game_Map* GameEngineCore::Get_Game_Map(int world_id)
{
	return world_id < game_maps.size() ? &game_maps[world_id] : nullptr;
}

Game_Map* pb::GameEngineCore::Get_Game_Map(WorldCoordinate wcoord)
{
	return Get_Game_Map(wcoord.world_id);
}

PawnHolder* GameEngineCore::Get_PawnHolder()
{
	return &pawn_holder;
}

EntityHolder* pb::GameEngineCore::Get_EntityHolder(int world_id)
{
	if (game_maps.size() > world_id)
	{
		return game_maps.at(world_id).Get_Entities();
	}
	return nullptr;
}

Pawn* GameEngineCore::Get_Pawn_By_Id(int id)
{
	return pawn_holder.Get_Pawn_By_Id(id);
}

Prototype* GameEngineCore::Get_Prototype(EID id)
{
	switch (id.type)
	{
	case PROTOTYPE_RECIPE:
		return &recipes.at(id.id);
	case PROTOTYPE_BLOCK:
		return &block_prototypes.at(id.id);
	case PROTOTYPE_FLUID:
		return &fluid_prototypes.at(id.id);
	case PROTOTYPE_PLANT:
		return &plant_prototypes.at(id.id);
	case PROTOTYPE_ITEM:
		return &item_prototypes.at(id.id);
	case PROTOTYPE_PAWN:
		return &pawn_prototypes.at(id.id);
	}
	return nullptr;
}

EntityBase* GameEngineCore::Get_Entity(EID id)
{
	switch (id.type)
	{
	case ENTITY_PLANT:
	case ENTITY_FLUID_BODY:
		return game_maps[id.world_id].GetEntity(id);
		break;
	}
	return nullptr;
}

void GameEngineCore::Game_Update()
{
	Process_Signals();
	for(auto& map : game_maps)
		map.Update_Entities();
	pawn_holder.Update_All_Pawns();

}
