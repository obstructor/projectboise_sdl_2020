#pragma once
#include "GameController.h"


namespace pbsdl
{
	void Initialize_Event_Handler(pb::pbController* inGameController, pb::MenuBase*);
	void Gather_Input();
}