#pragma once
#include "Coordinate.h"
#include "UserIntefaceCore.h"
#include <sdlgui/sdlguii.h>
#include "common.h"

namespace pb {
	class MenuBase;
	class UserInterfaceCore;
	class pbController;
	struct pbEvent;

	class Menu : private sdlgui::Widget
	{
	public:
		enum { MENU_NULL,
				MENU_SDLGUI,
				MENU_SCREEN,
				MENU_VIEWPORT,
				MENU_CUSTOM
		} type;
		MenuBase* menu_base;
		std::map<SDL_Keycode, pbEvent> keybinds;

		Menu(MenuBase* parent,sdlgui::Widget* screen);
		~Menu();

		pbEvent Get_Event_From_SDL_Event(SDL_Event& eve);

		void Generate_Default_Main_Menu(UserInterfaceCore*&, pbController *& controller);
		void Generate_Default_KeyBinds();
		Rectangle Get_Extents();
	};

	class MenuBase : private sdlgui::Screen
	{
	private:
		int id_counter = 0;
		bool event_handled = false;
		UserInterfaceCore* ui = nullptr;
		pbController* parent = nullptr;
		std::map<int, pbEvent> button_event_map;

		std::vector<Menu> all_menues;
		std::vector<Menu*> menu_stack;
		
		void Generate_Default_Event_Map();
		void Execute_Event(int id);
		auto Generate_Event_Lambda();
		auto Generate_Event_Lambda(int id);
		auto Generate_Event_Lambda(int id, pbEvent e);
		auto Generate_Event_Lambda(pbEvent e);
	public:
		MenuBase(SDL_Window*, int, int);
		void Draw_All();
		void Receive_SDL_Event(SDL_Event &eve);
		void Initialize(UserInterfaceCore*, pbController*);
		void Test();
	};
}
