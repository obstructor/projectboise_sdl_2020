#pragma once
#include "Entities.h"
#include "Game_Map.h"
#include "PawnHolder.h"
#include "common.h"

namespace pb
{
	class Parser;
	class pbController;
	class GameEngineCore
	{
	private:
		PawnHolder pawn_holder;
		std::vector <Game_Map> game_maps;
		pbController* parent;

		std::vector<RecipePrototype> recipes;
		std::vector<BlockPrototype> block_prototypes;
		std::vector<PawnPrototype> pawn_prototypes;
		std::vector<FluidPrototype> fluid_prototypes;
		std::vector<PlantPrototype> plant_prototypes;
		std::vector<ItemPrototype> item_prototypes;

		pb::CircularBuffer<pbEvent> event_queue;

		void Sync_PrototypeEID(std::map<EID, EID> proto_map);

		void Process_Signals();

		void Change_Recipe(pbEvent eve);


	public:
		GameEngineCore(pbController*);

		void Initialize_Game_Engine();
		void Initialize_Game_Engine(Parser *);
		bj::object Save_Game_Engine_JSON();
		void Load_Game_Engine_JSON(bj::value val, std::map<EID, EID> proto_map);
		void Generate_Gallery_Map();
		void Receive_Signal(pbEvent);
		Game_Map* Get_Game_Map(int world_id);
		Game_Map* Get_Game_Map(WorldCoordinate wcoord);
		PawnHolder* Get_PawnHolder();
		EntityHolder* Get_EntityHolder(int world_id);
		Pawn* Get_Pawn_By_Id(int id);

		Prototype* Get_Prototype(EID id);
		EntityBase* Get_Entity(EID id);

		void Game_Update();
	};
}