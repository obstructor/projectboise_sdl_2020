#pragma once
#include "ParserPrototypes.h"
#include "common.h"

struct RenderData;
namespace pb
{
	class Parser
	{
	private:
		static const std::map<std::string, EntityType> string_to_enumtype;
		int prototype_id[PROTOTYPE_END_ENUM];
		std::map<std::string, AllPrototypeUnion> prototypes;
		void Insert_Prototype(AllPrototypeUnion&);
		void Clear_Parser();

		//PYTHON START HERE PARSE_DECLARATION
		AllPrototypeUnion& Parse_Fluid(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Block(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Item(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Particle(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Constructor(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Furniture(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Construction(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Recipe(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Mapgen(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Pawn(const std::vector<std::string>&, AllPrototypeUnion&);
		AllPrototypeUnion& Parse_Plant(const std::vector<std::string>&, AllPrototypeUnion&);
		//PYTHON STOP HERE PARSE_DECLARATION
		AllPrototypeUnion& PopParse_General_Entity_Parts(std::vector<std::string>&, AllPrototypeUnion&);
	public:
		Parser();

		AllPrototypeUnion Parse_String(std::string);
		AllPrototypeUnion Parse_Row(std::vector<std::string>&, EntityType);

		//PYTHON START HERE STRING_TO_DECLARATION
		int String_To_int(std::string);
		short String_To_short(std::string);
		char String_To_char(std::string);
		unsigned int String_To_unsigned_int(std::string);
		bool String_To_bool(std::string);
		float String_To_float(std::string);
		double String_To_double(std::string);
		//PYTHON STOP HERE STRING_TO_DECLARATION
		std::string String_To_string(std::string a) { return a; };
		std::string String_To_EID(std::string a) { return a; };

		EID Lookup_EID(std::string);

		void Run_Parser();
		void Parse_File(std::string);
		void Convert_ParserPrototypes_to_Prototypes();

		auto get_all_prototypes() { return prototypes; };
		std::map<EID, textInformation> get_all_proto_text();
		template <class T> void get_prototypes(std::vector<T>& vector, EntityType type);

		std::vector<RenderData> get_render_data();
	};

	template<class T>
	inline void Parser::get_prototypes(std::vector<T>& vector, EntityType type)
	{
		vector.clear();
		vector.resize(prototype_id[type]);
		for (const auto& pair : prototypes)
		{
			if (pair.second.type == type)
			{
				vector[pair.second.proto_id] = (T)pair.second;
			}
		}
	}
}
