#pragma once
#include <string>
#include <vector>
#include <set>
#include <map>
#include <tuple>
#include <stdexcept>
#include <bitset>
#include <filesystem>
#include <fstream>
#include <variant>
#include <boost/json.hpp>
#include <boost/mp11.hpp>
#include <boost/beast/core/detail/base64.hpp>

#include "Coordinate.h"
#include "CircularBuffer.h"

namespace bj = boost::json;

namespace pb
{
	const int RECIPE_BUFFER_SIZE = 8;
	
	template<typename T>
	inline std::string base64_encoder(T* object)
	{
		size_t byte_size = sizeof(*object) * 4 / 3 + 1;
		char* char_buffer = new char[byte_size + 2];
		char_buffer[byte_size + 1] = '\0';
		boost::beast::detail::base64::encode(char_buffer, object, byte_size);
		std::string output(char_buffer);
		delete[] char_buffer;
		return output;
	}

	template<typename T>
	inline std::string base64_encoder(T object[], size_t length)
	{
		size_t input_size = sizeof(*object) * length;
		size_t output_size = sizeof(*object) * length * 4 / 3 + 1;
		char* char_buffer = new char[output_size + 2];
		char_buffer[output_size + 1] = '\0';
		boost::beast::detail::base64::encode(char_buffer, object, input_size);
		std::string output(char_buffer);
		delete[] char_buffer;
		return output;
	}

	template<typename T>
	inline T* base64_decoder(T object[], std::string input)
	{
		boost::beast::detail::base64::decode(object, input.c_str(), input.length());
		return object;
	}

	template <typename V>
	auto variant_from_index(size_t index) -> V
	{
		using namespace boost::mp11;
		return mp_with_index<mp_size<V>>(index,
			[](auto I) { return V(std::in_place_index<I>); });
	}

	bj::value read_json(std::istream& is);

	enum pbDirection {
		UP, DOWN, LEFT, RIGHT
	};

	enum EntityType : short {
		ENTITY_TYPE_NULL,
		//PYTHON START HERE PARSE_TYPES
		PROTOTYPE_FLUID,
		PROTOTYPE_BLOCK,
		PROTOTYPE_ITEM,
		PROTOTYPE_PARTICLE,
		PROTOTYPE_CONSTRUCTOR,
		PROTOTYPE_FURNITURE,
		PROTOTYPE_CONSTRUCTION,
		PROTOTYPE_RECIPE,
		PROTOTYPE_MAPGEN,
		PROTOTYPE_PAWN,
		PROTOTYPE_PLANT,
		//PYTHON STOP HERE PARSE_TYPES
		PROTOTYPE_END_ENUM,
		//PYTHON START HERE DROP_ENTITY_TYPES
		ENTITY_FLUID,
		ENTITY_BLOCK,
		ENTITY_ITEM,
		ENTITY_PARTICLE,
		ENTITY_CONSTRUCTOR,
		ENTITY_FURNITURE,
		ENTITY_CONSTRUCTION,
		ENTITY_RECIPE,
		ENTITY_MAPGEN,
		ENTITY_PAWN,
		ENTITY_PLANT,
		//PYTHON STOP HERE DROP_ENTITY_TYPES
		ENTITY_FLUID_BODY,
		ENTITY_FLUID_BLOCK,
		ENTITY_END_ENUM
	};

	struct EID
	{
		int id;
		short world_id = 0;
		EntityType type;
		operator int() { return id; };
		operator bool() { return id != -1 || type != ENTITY_TYPE_NULL; };
		bool operator<(const EID& other) const { return other.type == type? other.id < id : other.type < type; };
		bool operator==(const EID& other) const { return other.type == type && other.id == id; };
		bool operator!=(const EID& other) const { return other.type != type || other.id != id; };
		//EID& operator=(EID i) { id = i.id; type = i.type; return *this; };
		//EID(const EID& i) { *this = i; };
		EID(int i=-1, EntityType p=ENTITY_TYPE_NULL, int wi=0) { id = i; type = p; world_id = wi; };

		bj::object toJSON() const
		{
			bj::object obj;
			obj.insert_or_assign("id",id);
			obj.insert_or_assign("world_id", world_id);
			obj.insert_or_assign("entity_type", type);
			return obj;
		}
		EID& fromJSON(bj::value val)
		{
			bj::object& obj = val.as_object();
			id = obj.at("id").as_int64();
			world_id = obj.at("world_id").as_int64();
			type = (EntityType)obj.at("entity_type").as_int64();
			return *this;
		}

		EID& fromJSON(bj::value val, const std::map<EID, EID>& eid_map)
		{
			bj::object& obj = val.as_object();
			id = obj.at("id").as_int64();
			world_id = obj.at("world_id").as_int64();
			type = (EntityType)obj.at("entity_type").as_int64();
			return *this;
		}
	};

	inline bool String_Contains(const std::string& fullstring, const std::string& substring)
	{
		return fullstring.find(substring) != std::string::npos;
	}	
	
	struct textInformation {
		std::string name = "NULL";
		std::string sprite_file = "";
		std::string icon = "";
	};

	//Extra data or whatever for the event
	//Do not put any pointers or strings/vectors in pbEvent, use some sort of EID instead

	enum pbEventType
	{
		PB_EVENT_NULL,
		//PYTHON START HERE READ_PBEVENT_ENUM
		PB_EVENT_CLOSE_MENU,
		PB_EVENT_SWITCH_MENU,
		PB_EVENT_OPEN_MENU,
		PB_EVENT_START_GAME,
		PB_EVENT_CLOSE_GAME,
		PB_EVENT_SAVE_GAME,
		PB_EVENT_LOAD_GAME,
		PB_EVENT_PAUSE_GAME,

		PB_EVENT_CHANGE_GAME_SPEED,
		PB_EVENT_SET_CURSOR_DESIGNATION,
		PB_EVENT_NEXT_CURSOR_DESIGNATION,
		PB_EVENT_START_DESIGNATION,
		PB_EVENT_FINISH_DESIGNATION,
		PB_EVENT_CANCEL_DESIGNATION,
		PB_EVENT_CHANGE_SETTING_GLOBAL,
		PB_EVENT_CHANGE_SETTING_PAWN,

		PB_EVENT_SPAWN_ITEM,
		PB_EVENT_DELETE_ITEM,
		PB_EVENT_DESTROY_BLOCK,
		PB_EVENT_PLACE_BLOCK,
		PB_EVENT_SPAWN_GAS,
		PB_EVENT_DELETE_GAS,
		PB_EVENT_SPAWN_PAWN,
		PB_EVENT_CHANGE_RECIPE,

		PB_EVENT_CURSOR_DESIGNATION_CHANGED,

		PB_EVENT_MOVE_VIEWPORT,
		PB_EVENT_MOD_EVENT,
		//PYTHON STOP HERE READ_PBEVENT_ENUM
		PB_EVENT_ENUM_END
	};

	class BasePayload
	{
	public:
		virtual void convertEIDs(const std::map<EID, EID>& eid_map) {};
	};

	class pbDesignation : BasePayload
	{
	private:
		WorldRectangle wrect;
		int quantity;

	public:
		EID eid;

		pbDesignation() { eid = EID(); quantity = 0; };
		pbDesignation(EID in) { eid = in;  quantity = 0; };
		//Probably good to do type checking but this is fine for now.
		WorldCoordinate getLoc() const { return wrect.Top_Left(); };
		WorldRectangle getWrect() const { return wrect; };
		int getWorld() const { return wrect.world_id; };
		int getQuantity() const { return quantity; };
		void setQuantity(int x) { quantity = x; };
		void startCoord(WorldCoordinate x) { wrect = WorldRectangle(x, x, x.world_id); };
		void endCoord(WorldCoordinate x) { wrect = WorldRectangle(wrect.Top_Left(), x, x.world_id); };

		bj::object toJSON() const;
		void fromJSON(bj::value val);
		virtual void convertEIDs(const std::map<EID, EID>& eid_map);
	};

	class pbMenuEvent : public BasePayload
	{
	public:
		Coordinate location;
		pbMenuEvent() { location = Coordinate(); };
		pbMenuEvent(Coordinate c) { location = c; };
		bj::object toJSON() const{return bj::object();};
		void fromJSON(bj::value val){};
	};

	class pbViewportEvent : public BasePayload
	{

	};

	typedef std::variant<
		pbMenuEvent,
		pbDesignation
	> PayloadType;
	const int SIZE_PB_EVENT_CONTAINER = 64;
	struct pbEvent : public BasePayload
	{
		pbEventType type;
		PayloadType payload;

		pbEvent() { type = PB_EVENT_NULL; };
		pbEvent(pbEventType in_type) { type = in_type; };
		template<typename T>
		pbEvent(pbEventType in_type, T t) { type = in_type; payload = t; };

		bj::object toJSON() const;
		void fromJSON(bj::value val);
		virtual void convertEIDs(const std::map<EID, EID>& eid_map);
	};
	extern const std::map<pbEventType, std::string> enum_text_map;
}