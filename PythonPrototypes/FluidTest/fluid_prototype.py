import pygame
from pygame.locals import *
import csv

class Coordinate:
    def __init__(self):
        self.x = 0
        self.y = 0


class FluidSurface:
    def __init__(self):
        self.left = Coordinate()
        self.right = Coordinate()
        self.pressure = 0


class Fluid:
    def __init__(self, density=1, iid=1, in_type=-1, quantity=100):
        self.density = density
        self.id = iid
        self.type = in_type
        self.quantity = quantity

    def fillArea(self, top_left, bot_right):
        self.quantity = self.density * (bot_right.x - top_left.x) * (bot_right.y - top_left.y)


class FluidPrototype:
    id_counter = 0

    def __init__(self, name, density, color="0xFFFFFF"):
        self.name = name
        self.density = density
        self.color = Color(int(color, 16))

    def makeFluid(self):
        FluidPrototype.id_counter += 1
        return Fluid(self.density, FluidPrototype.id_counter)

    def __str__(self):
        return self.name + " " + str(self.density) + " " + str(self.color) + ";"

    def __repr__(self):
        return self.__str__()


class Block:
    def __init__(self, fluid=False, iid=-1, in_type=-1):
        self.fluid = fluid
        self.id = iid
        self.type = in_type


gameMap = [Block()] * 64 * 64
fluidProto = []

with open('fluids.csv','r') as f:
    reader = csv.reader(f)
    for row in reader:
        if '#' in row[0]:
            continue
        else:
            fluidProto.append(FluidPrototype(row[0], row[1], row[2]))

print(fluidProto)

with open('default_map.dat', 'w', newline='') as f:
    writer = csv.writer(f)
    for y in range(64):
        row = []
        for x in range(64):
            row.append('B0')
        writer.writerow(row)


SCREENRECT = Rect(0, 0, 64 * 8, 64 * 8)
pygame.init()
pygame.display.set_mode(SCREENRECT.size)
print('hello')


def Draw_Screen():
    for y in range(64):
        for x in range(64):
            pygame.draw.rect(pygame.display.get_surface(), fluidProto[gameMap[y * 64 + x].id].color)